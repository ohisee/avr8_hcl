/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "hal/build_defs.h"
#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <util/delay.h>

#include "hal/hal.h"
//#include "os/kernel/kernel_preproc_defs.h"
#include "os/kernel/kernel_memory.h"
#include "os/kernel/kernel_process.h"
#include "os/kernel/kernel_mutex.h"
#include "os/kernel/kernel_isr.h"

#include "processes/vt/virtual_terminal.h"

#include "processes/tabita/tabita.h"


extern uint8_t __heap_start;
extern uint8_t __heap_end;

k_pid idle_pid = K_NO_PID;

void idle(void * arg)
{

	while (1)
	{
		for (uint8_t i=0; i < 100; i++)
		{
			asm volatile ("nop");
		}
	}
}

//#include "hal/drivers/serial/usart.h"

int main(void)
{
    /*#define BAUD_END(UART_BAUD_RATE0, F_OSC0) (((F_OSC0 / (UART_BAUD_RATE0 * 16UL)))-1)
        UCSR0B = (1 << RXEN0) | (1 << TXEN0);
	//SET FRAME FORMAT TO 8DATA 1 STOP
	UCSR0C = (1 << UCSZ00) | (1 << UCSZ01);

	UBRR0L = BAUD_END(1000000UL, F_OSC);
	UBRR0H = (BAUD_END(1000000UL, F_OSC)>>8);

	UDR0 = 'A';*/
hal_run_debug_serial();
#if DEBUG_MESSAGES == YES
	hal_run_debug_serial();
    printf_P(PSTR("Debug\r\n"));
#endif // DEBUG_MESSAGES

	//initialize OS i.e heap, idle
	kernel_memory_init((uint8_t *)&__heap_start, (uint8_t *)(((uint16_t)&__heap_start)+(uint16_t)HEAP_ENDS_AT));

	idle_pid = kernel_create_process(PSTR("cpu_idle"), KPROC_TYPE_SYSTEM, 15, idle, NULL);

	kernel_isr_init();


	//initialize HAL and hal process
    hal_init();
    hal_load_config();
    hal_postinit();

	//initialize other (if required) processes, mutexes, semaphores etc
#if DEBUG_MESSAGES == YES
    printf_P(PSTR("Initialization completed!"));
#endif // DEBUG_MESSAGES

	vt_main();
	tabita();

	//run
	kernel_processes_start();

    while(1)
    ;

    return 0;
}
