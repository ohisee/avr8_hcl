/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/pgmspace.h>
#include "vt_text.h"

const char PR_NULL[] PROGMEM = "\0";

#if BUILD_WITH_TOP == YES
const char main_1[] PROGMEM = "Processes\0";
#endif // BUILD_WITH_TOP
const char main_2[] PROGMEM = "Manage\0";
const char main_3[] PROGMEM = "Streams\0";
const char main_4[] PROGMEM = "I/O Pins allocs\0";
const char main_5[] PROGMEM = "Save configuration\0";
const char main_6[] PROGMEM = "Clear EEPROM\0";
const char main_7[] PROGMEM = "Error list\0";
const char main_8[] PROGMEM = "Reboot\0";
const char main_9[] PROGMEM = "Powerdown\0";
const char main_10[] PROGMEM = "View EEPROM data\0";
#if BUILD_WITH_SRAM_VIEWER == YES
const char main_11[] PROGMEM = "SRAM viewer\0";
#endif // BUILD_WITH_SRAM_VIEWER
const char main_12[] PROGMEM = "Netatat\0";
const char main_13[] PROGMEM = "\0";

const char * const main_menu[] PROGMEM  =
{
#if BUILD_WITH_TOP == YES
	main_1,
#endif // BUILD_WITH_TOP
	main_2,
	main_3,
	main_4,
	main_5,
	main_6,
	main_7,
	main_8,
	main_9,
#if BUILD_WITH_SRAM_VIEWER == YES
	main_10,
#endif // BUILD_WITH_SRAM_VIEWER
    main_11,
	main_12,
	main_13,
	PR_NULL
};

const char eepr_1[] PROGMEM = "Reset config\0";
const char eepr_2[] PROGMEM = "Clean all EE\0";
const char eepr_3[] PROGMEM = "<- Back\0";
const char eepr_4[] PROGMEM = "\0";

const char * const eeprom_menu[] PROGMEM =
{
	eepr_1,
	eepr_2,
	eepr_3,
	PR_NULL
};

const char label_ram[] PROGMEM = "Free RAM (kB):";
const char label_exit_conf[] PROGMEM = "Exit config. Changes will remain\0";
const char label_eepr_d[] PROGMEM = "Erasing EEPROM\0";
const char label_eepr_d2[] PROGMEM = "Config was reset, Press any key\0";
const char label_finished_e[] PROGMEM = "Done, press Enter to continue\0";

const char label_devices[] PROGMEM = "Device list:\0";
const char label_dev_add[] PROGMEM = "C - Attach new device.\0";
const char label_dev_add_h[] PROGMEM = "V - Attach new handler.\0";
const char label_dev_edit[] PROGMEM = "E - Edit selected device.\0";
const char label_dev_rem[] PROGMEM = "D - Remove selected device.\0";
const char label_dev_sel[] PROGMEM = "<Enter> - Select device, get info.\0";
const char label_dev_opts[] PROGMEM = "S -Device options.\0";
const char label_dev_exit[] PROGMEM = "<F5> - Leave config page.\0";
const char label_dev_left[] PROGMEM = "<LEFT> - Previous page\0";
const char label_dev_right[] PROGMEM = "<RIGHT> - Next page\0";
const char label_devs[] PROGMEM = "DEVICES: %i/%i\0";
const char label_pages[] PROGMEM = "Pages: %i/%i\0";
const char label_tb0_no[] PROGMEM = "Num\0";
const char label_tb0_did[] PROGMEM = "ID(DID)\0";
const char label_tb0_drv[] PROGMEM = "DRIVER\0";
const char label_tb0_hnd[] PROGMEM = "HANDLER\0";
const char label_tb0_mem[] PROGMEM = "ADR\0";

const char label_tb1_title[] PROGMEM = "OPENED STREAMS TO DEVICES LIST\0";
const char label_tb1_num[] PROGMEM = "SID\0";
const char label_tb1_addr[] PROGMEM = "ADDRESS\0";
const char label_tb1_dest[] PROGMEM = "DESTINATION\0";
const char label_tb1_ext[] PROGMEM = "EXTENSION\0";
const char label_tb1_flags1[] PROGMEM = "FLAGS\0";
const char label_tb1_flags2[] PROGMEM = "FLAGS\0";

const char label_tb2_title[] PROGMEM = "ALLOCATED INPUT OUTPUT PINS\0";
const char label_tb2_recno[] PROGMEM = "RecNo\0";
const char label_tb2_owner[] PROGMEM = "Ownder (DID)\0";
const char label_tb2_port[] PROGMEM = "PORT\0";
const char label_tb2_pins[] PROGMEM = "PINS (MASK)\0";

const char label_tb3_title[] PROGMEM = "EEPROM RECORDS\0";

const char label_press_any_key[] PROGMEM = "Press ESC key\0";

const char label_query_fail[] PROGMEM = "QUERY FAILED\0";
const char label_query_devf[] PROGMEM = "hal_dev_node_query returned -1\0";
const char label_query_ddf[] PROGMEM = "hal returned dd = -1\0";

const char title_areyoushure[] PROGMEM = "Confirm\0";
const char label_yes[] PROGMEM = "Yes\0";
const char label_no[] PROGMEM = "No\0";
const char * const yesno_menu[] PROGMEM =
{
	label_yes,
	label_no,
	PR_NULL
};

const char save_changes_to_eeprom[] PROGMEM = "Save to eeprom?\0";
const char press_any_key_to_cont[] PROGMEM = "Press any key to cont...\0";
const char do_not_power_down[] PROGMEM = "Do not unplug PSU!\0";
const char do_work_in_prog[] PROGMEM = "Working! please wait!\0";

//errors descr
const char error_capt[] PROGMEM = "ERROR\0";
const char no_fd_created[] PROGMEM = "Unable to create new dev\0";
const char unable_to_remove[] PROGMEM = "Can not remove FD\0";
const char query_failed[] PROGMEM = "Operation failed (see error log)\0";
const char dd_dev_memcpy_failed[] PROGMEM = "Item not found by offset\0";
const char dd_ret_null[] PROGMEM = "hal_dd_get returned NULL\0";

const char info_capt[] PROGMEM = "Information\0";
const char measurments_capt[] PROGMEM = "Measurments\0";
const char measure_sensors[] PROGMEM = "Measure ALL\0";
const char measure_all[] PROGMEM = "Measure ALL ASYNC\0";
const char value_capt[] PROGMEM = "Values changing\0";
const char send_data_capt[] PROGMEM = "Send data\0";
const char receive_monitor_capt[] PROGMEM = "Receive monitor\0";
const char add_slave[] PROGMEM = "Add slave\0";
const char remove_slave[] PROGMEM = "Remove slave\0";
const char rescan_bus[] PROGMEM = "Resacan BUS\0";

const char not_supported[] PROGMEM = "Not supported\0";

const char measuring_capt[] PROGMEM = "Measuring available ADC\0";
const char measuring_sync_failed[] PROGMEM = "Measuring all ADC SYNC failed!\0";

const char ocra8bit[] PROGMEM = "OCRA value max:255\0";
const char ocrb8bit[] PROGMEM = "OCRB value max:255\0";
const char ocrc8bit[] PROGMEM = "OCRC value max:255\0";
const char ocra16bit[] PROGMEM = "OCRA value max:65535\0";
const char ocrb16bit[] PROGMEM = "OCRB value max:65535\0";
const char ocrc16bit[] PROGMEM = "OCRC value max:65535\0";
const char icr8bit[] PROGMEM = "ICR value max:255\0";
const char icr16bit[] PROGMEM = "ICR value max:65535\0";

const char system_timer_busy_capt[] PROGMEM = "Timer %d is used by system!\0";

const char input_mac_address[] PROGMEM = "MAC address (without : or .) AABB\0";

const char io_mask_input_capt[] PROGMEM = "Input mask in dec: i.e 128 or 3\0";
const char usart_buf_rx_size_capt[] PROGMEM = "Input RX buf size (0-not needed)\0";
const char usart_buf_tx_size_capt[] PROGMEM = "Input TX buf size (0-not needed)\0";
const char usart_buf_cts_rts_pin[] PROGMEM = "Input pin number 0..7 or -1 if N/A\0";


const char usart_router_scr_capt[] PROGMEM = "From (src)[input fid]:\0";
const char usart_router_rcv_capt[] PROGMEM = "To (rcv)[input fid]:\0";
const char usart_router_send_capt[] PROGMEM = "Data byte\0";

const char spi_sel_id[] PROGMEM = "Input DID of SPI driver to attach to\0";
const char io_sel_id[] PROGMEM = "Input DID of the IO driver to attach to\0";
const char io_sel_id_mon[] PROGMEM = "Input DID of the IO driver for monitoring\0";

const char proc_sel[] PROGMEM = "Input process ID\0";
const char proc_mux_nf[] PROGMEM = "Mutex not found\0";

const char anon_pan_sel[] PROGMEM = "Input cell id\0";
const char netdev_pan_netid[] PROGMEM = "Input network device ID\0";
const char netdev_pan_notfound[] PROGMEM = "Network device not found\0";
const char length_assert[] PROGMEM = "Length assertion\0";
const char dhclient_failed[] PROGMEM = "dhclient failed\0";
const char dhclient_not_sup[] PROGMEM = "dhclient not supported\0";

const char inp_ip_addr[] PROGMEM = "Input IP addr x.y.z.c\0";
const char inp_mask_addr[] PROGMEM = "Input IP MASK x.y.z.c\0";
const char inp_gateway_addr[] PROGMEM = "Input gateway x.y.z.c\0";

const char label_netstat_title[] PROGMEM = "NETSTAT\0";
const char label_netstat_if[] PROGMEM = "NET INTRFS\0";
const char label_netstat_if_id[] PROGMEM = "IFD\0";
const char label_netstat_if_did[] PROGMEM = "DID\0";
const char label_netstat_if_mac[] PROGMEM = "MAC\0";
const char label_netstat_if_ip[] PROGMEM = "IP\0";
const char label_netstat_if_mask[] PROGMEM = "IPMASK\0";
const char label_netstat_if_gateway[] PROGMEM = "Gateway\0";
const char label_netstat_listen[] PROGMEM = "LISTEN\0";
const char label_netstat_lis_lid[] PROGMEM = "LID\0";
const char label_netstat_lis_proto[] PROGMEM = "PROTO\0";
const char label_netstat_lis_lport[] PROGMEM = "PORT\0";
const char label_netstat_states[] PROGMEM = "STATES\0";
const char label_netstat_sta_id[] PROGMEM = "SID\0";
const char label_netstat_if_rip[] PROGMEM = "Rem IP\0";

const char label_netstat_arp[] PROGMEM = "ARP\0";
const char label_netstat_arp_bottom[] PROGMEM = "F5 - EXIT\0";
const char label_netstat_bottom[] PROGMEM = "F1 - DHCP | F2 - IP MANUAL | F3 - ARP | F5 - EXIT\0";

const char label_u2eth_did[] PROGMEM = "Input DID of USART\0";
