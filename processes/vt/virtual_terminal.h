/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef VIRTUAL_TERMINAL_H_
#define VIRTUAL_TERMINAL_H_

#define VT_VERSION PSTR("1.0")

#define ASCII_SYN 0x16

extern void vt_main();
extern void vt_main_telnet(t_dev_id did);

#endif /* VIRTUAL_TERMINAL_H_ */
