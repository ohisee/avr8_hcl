/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <ctype.h>
#include "../../hal/shared.h"
#include "../../hal/extlib/e_enum.h"
#include "../../hal/device/dev.h"
#include "../../lib/conio/conio.h"
#include "../../lib/conio/kbd.h"
#include "../../lib/conio/ui.h"
#include "fd_attach/vt_attach.h"

FOREACH_IO_ADDRESSES(GENERATE_VARS_VAL);
const struct menudialog_entry vt_io_ports_captions_struct[] PROGMEM = {
	FOREACH_IO_ADDRESSES(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};


void
__convert_ip(char * str, char * ip)
{
    size_t index = 0;

    while (*str)
    {
        if (isdigit((unsigned char)*str)) {
            ip[index] *= 10;
            ip[index] += *str - '0';
        } else {
            index++;
        }
        str++;
    }

    return;
}
