/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../../build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "../../../shared.h"
#include "../../../error.h"
#include "../../../device/dev.h"
#include "../../../kernel.h"
#include "../../../driver/usart/usart.h"
#include "../../handlers.h"
#include "../virtual_terminal.h"
#include "../../../lib/conio/conio.h"
#include "../../../lib/conio/kbd.h"
#include "../../../lib/conio/ui.h"
#include "../vt_text.h"
#include "vt_attach.h"
#include "../../../handler/MAX6675/max6675.h"

int8_t vt_attach_max6675_s(struct fd_dev_node * fd)
{

}

int8_t vt_modify_max6675_s(struct fd_dev_node * fd)
{

}

int8_t vt_info_max6675_s(struct fd_dev_node * fd)
{

}
