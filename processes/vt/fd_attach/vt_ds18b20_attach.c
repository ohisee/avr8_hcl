/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT
 */

#include "../../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "../../../hal/hal.h"
#include "../../../hal/drivers/ds18x20/ds18.h"
#include "../../../hal/handlers.h"
#include "../virtual_terminal.h"
#include "../../../lib/conio/conio.h"
#include "../../../lib/conio/kbd.h"
#include "../../../lib/conio/ui.h"
#include "../vt_text.h"
#include "vt_attach.h"


#define FOREACH_VT_MASKS(DV)\
	DV(PIN_0, (uint8_t) 1)\
	DV(PIN_1, (uint8_t) 2)\
	DV(PIN_2, (uint8_t) 4)\
	DV(PIN_3, (uint8_t) 8)\
	DV(PIN_4, (uint8_t) 16)\
	DV(PIN_5, (uint8_t) 32)\
	DV(PIN_6, (uint8_t) 64)\
	DV(PIN_7, (uint8_t) 128)

enum e_pin_masks
{
	FOREACH_VT_MASKS(GENERATE_ENUM_VAL)
};

FOREACH_VT_MASKS(GENERATE_VARS_VAL);
const struct menudialog_entry vt_ds18_masks_struct[] PROGMEM = {
	FOREACH_VT_MASKS(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

const struct menudialog_entry ds18x20_io_options_captions_struct[] PROGMEM = {
	{
	    measure_sensors, VT_INFO_OPTS_MEAS
	},
	{
        rescan_bus, VT_INFO_OPTS_RESCAN
	},
	{
		P_NULL, NULL
	}
};

static int8_t __ui_query(FILE * file, struct ds18_setup * dss);
static int8_t __ui_query_info(FILE * file, uint8_t * opt);

static int8_t __ui_query(FILE * file, struct ds18_setup * dss)
{
	int ret = 0;
	int res = 0;
	ret = runmenu0_s(file, 10, 10, 0, 10, vt_io_ports_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	dss->e_port = (uint8_t) res;

	ret = runmenu0_s(file, 12, 10, 0, 10, vt_ds18_masks_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	dss->pin_mask = (uint8_t) res;

	return F_OK;
}

static int8_t
__ui_query_info(FILE * file, uint8_t * opt)
{
    int ret = 0;
	int res = 0;
	ret = runmenu0_s(file, 10, 10, 0, 10, ds18x20_io_options_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	*opt = (uint8_t) res;

	return F_OK;
}

int8_t vt_attach_ds18b20(FILE * file, struct ds18_setup * dss)
{
	return __ui_query(file, dss);
}

int8_t vt_modify_ds18b20(FILE * file, struct ds18_setup * dss)
{



	return F_FAIL;
}

int8_t vt_info_ds18b20(FILE * file, struct dev_descr_node * dd)
{
	uint8_t opt = 0;
    int8_t res = 0;

    res = __ui_query_info(file, &opt);
	ASSERT_DATA(res != F_OK, return F_FAIL);

    switch (opt)
    {

        case VT_INFO_OPTS_MEAS:
        {
            //we can use dd->instance but we need to make sure that record presents
            ds18_read_all_blocking((struct ds18_bus*) dd->instance);
        }
        break;
        case VT_INFO_OPTS_RESCAN:
            (void) ds18_rescan_bus( (struct ds18_bus*) dd->instance);
        break;
    }

	return F_OK;
}
