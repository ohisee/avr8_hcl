/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../../build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "../../../shared.h"
#include "../../../error.h"
#include "../../../device/dev.h"
#include "../../../kernel.h"
#include "../../../driver/usart/usart.h"
#include "../../handlers.h"
#include "../virtual_terminal.h"
#include "../../../lib/conio/conio.h"
#include "../../../lib/conio/kbd.h"
#include "../../../lib/conio/ui.h"
#include "../vt_text.h"
#include "vt_attach.h"
#include "../../../handler/usart_switch/usart_switch.h"

static int8_t __ui_query_link(fd_dev_id * src, fd_dev_id * rcv);
static int8_t __ui_query_info(uint8_t * opt);
static int8_t __ui_query_send(fd_dev_id * to, uint8_t * data_byte);
static int8_t __ui_query_data();


const struct menudialog_entry vt_usart_route_options_captions_struct[] PROGMEM = {
	{
	    info_capt, VT_INFO_OPTS_INFO
	},
	{
	    send_data_capt, VT_INFO_OPTS_SEND
	},
	{
	    receive_monitor_capt, VT_INFO_OPTS_RECEIVE
	},
	{
		P_NULL, NULL
	}
};

static int8_t
__ui_query_link(fd_dev_id * src, fd_dev_id * rcv)
{
	int ret = 0;
    uint8_t data[5];
    memset(data, 0, 5);
	ret = draw_input_dialog(10, 10, usart_router_scr_capt, data, 5);
	if (ret < 1)
    {
        return F_FAIL;
    }

    *src = (uint16_t) atoi ( (const char *) data );

    memset(data, 0, 5);
	ret = draw_input_dialog(10, 10, usart_router_rcv_capt, data, 5);
	if (ret < 1)
    {
        return F_FAIL;
    }

    *rcv = (uint16_t) atoi ( (const char *) data );

	return F_OK;
}

static int8_t
__ui_query_info(uint8_t * opt)
{
    int ret = 0;
	int res = 0;
	ret = runmenu0_s(10, 10, 0, 10, vt_usart_route_options_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	*opt = (uint8_t) res;

	return F_OK;
}

static int8_t
__ui_query_send(fd_dev_id * to, uint8_t * data_byte)
{
    int ret = 0;
    uint8_t data[5];
    memset(data, 0, 5);
	ret = draw_input_dialog(10, 10, usart_router_rcv_capt, data, 5);
	if (ret < 1)
    {
        return F_FAIL;
    }

    *to = (uint16_t) atoi ( (const char *) data );

    memset(data, 0, 5);
	ret = draw_input_dialog(10, 10, usart_router_send_capt, data, 5);
	if (ret < 1)
    {
        return F_FAIL;
    }

    *data_byte = (uint16_t) atoi ( (const char *) data );

	return F_OK;
}

static int8_t
__ui_query_data()
{
    int ret = 0;
    drawframe(20, 5, 40, 7, WHITE | GREEN << 4);
    gotoxy(21, 6);

    while (1)
    {
        if (_kbhit())
		{
		    (void) _getch();
			return F_OK;
		}
    }

	return F_OK;
}

int8_t vt_attach_usart_router(struct fd_dev_node * fd)
{
	int8_t res = 0;

	res = usart_router_attach_to_fd(fd);
	ASSERT_DATA(res == K_NO_FD, return F_FAIL);

	return F_OK;
}

int8_t vt_modify_usart_router(struct fd_dev_node * fd)
{
    int8_t res = 0;
	fd_dev_id src = 0;
	fd_dev_id rcv = 0;

	res = __ui_query_link(&src, &rcv);
	ASSERT_DATA(res != F_OK, return F_FAIL);

	res = usart_router_link_ports(src, rcv);

	ASSERT_DATA(res == 1, return F_FAIL);

	return F_OK;
}

int8_t vt_info_usart_router(struct fd_dev_node * fd)
{
    uint8_t opt = 0;
    int8_t res = 0;

    res = __ui_query_info(&opt);
	ASSERT_DATA(res != F_OK, return F_FAIL);

    switch (opt)
    {
        case VT_INFO_OPTS_INFO:
            //print information summery
            setcolor(YELLOW | BLACK <<4);
            clrscr();
            gotoxy(1,1);

            kernel_fd_query(fd->dev_fd, NULL, NULL);

            printf_P(PSTR("%S\r\n"), label_finished_e);
            while (!_kbhit());
            (void) _getch();
        break;

        case VT_INFO_OPTS_SEND:
        {
            fd_dev_id fid_to = 0;
            uint8_t cmd = 0;
            res = __ui_query_send(&fid_to, &cmd);
            ASSERT_DATA(res != F_OK, return F_FAIL);

            res = usart_route_send_data(fid_to, cmd);
            ASSERT_DATA(res != 0, return F_FAIL);
            __ui_query_data();
        }
        break;

        case VT_INFO_OPTS_RECEIVE:

        break;
    }



	return F_OK;
}
