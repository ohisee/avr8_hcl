/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "../../../hal/hal.h"
#include "../../../hal/drivers/serial/usart.h"
#include "../../../hal/handlers.h"
#include "../virtual_terminal.h"
#include "../../../lib/conio/conio.h"
#include "../../../lib/conio/kbd.h"
#include "../../../lib/conio/ui.h"
#include "../vt_text.h"
#include "vt_attach.h"

FOREACH_USART_HW_CLOCK_POL(GENERATE_VARS_VAL);
const struct menudialog_entry vt_usart_hw_clock_struct[] PROGMEM =
{
	FOREACH_USART_HW_CLOCK_POL(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_USART_HW_CHAR_SIZE(GENERATE_VARS_VAL);
const struct menudialog_entry vt_usart_hw_char_size_struct[] PROGMEM =
{
	FOREACH_USART_HW_CHAR_SIZE(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_USART_HW_STOP_BIT(GENERATE_VARS_VAL);
const struct menudialog_entry vt_usart_hw_stop_bit_struct[] PROGMEM =
{
	FOREACH_USART_HW_STOP_BIT(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_USART_HW_PARITY(GENERATE_VARS_VAL);
const struct menudialog_entry vt_usart_hw_parity_struct[] PROGMEM =
{
	FOREACH_USART_HW_PARITY(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_USART_HW_MODE(GENERATE_VARS_VAL);
const struct menudialog_entry vt_usart_hw_mode_struct[] PROGMEM =
{
	FOREACH_USART_HW_MODE(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_USART_DEV(GENERATE_VARS_VAL);
const struct menudialog_entry vt_usart_dev_struct[] PROGMEM =
{
	FOREACH_USART_DEV(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};


FOREACH_USART_BUFFER_TYPE(GENERATE_VARS_VAL);
const struct menudialog_entry vt_usart_buffer_type[] PROGMEM =
{
	FOREACH_USART_BUFFER_TYPE(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_USART_SW_RTOPERMODE(GENERATE_VARS_VAL);
const struct menudialog_entry vt_usart_sw_rtopermode_struct[] PROGMEM =
{
	FOREACH_USART_SW_RTOPERMODE(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_USART_BAUD_RATE(GENERATE_VARS_VAL);
const struct menudialog_entry vt_usart_hw_baudrates_struct[] PROGMEM =
{
	FOREACH_USART_BAUD_RATE(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

static int8_t __ui_query(FILE * file,
                         struct usart_setup_struct * uss_setup,
                         struct usart_hw_setup * hw_setup);

static int8_t __ui_query(FILE * file,
                         struct usart_setup_struct * uss_setup,
                         struct usart_hw_setup * hw_setup)

{
	int ret = 0;
	int res = 0;

    enum usart_dev e_usart_hw;
    enum usart_sw_RTopermode tr_oper_mode;
    enum usart_sw_buftype sb_buf_type;
    enum ports e_port_rts;
    enum ports e_port_cts;
    uint16_t rx_size = 0;
    uint16_t tx_size = 0;

	ret = runmenu0_s(file, 10, 10, 0, 10, vt_usart_dev_struct, &res);
	ASSERT_NULL(ret, __ui_query_error);
	e_usart_hw  = (enum usart_dev) res;


	ret = runmenu0_s(file, 15, 10, 0, 10, vt_usart_hw_clock_struct, &res);
	ASSERT_NULL(ret, __ui_query_error);
	enum usart_hw_clock_pol e_hw_clock  = (enum usart_hw_clock_pol) res;

	ret = runmenu0_s(file, 20, 10, 0, 10, vt_usart_hw_char_size_struct, &res);
	ASSERT_NULL(ret, __ui_query_error);
	enum usart_hw_char_size e_char_size  = (enum usart_hw_char_size) res;

	ret = runmenu0_s(file, 25, 10, 0, 10, vt_usart_hw_stop_bit_struct, &res);
	ASSERT_NULL(ret, __ui_query_error);
	enum usart_hw_stop_bit e_stop_bit  = (enum usart_hw_stop_bit) res;

	ret = runmenu0_s(file, 30, 10, 0, 10, vt_usart_hw_parity_struct, &res);
	ASSERT_NULL(ret, __ui_query_error);
	enum usart_hw_parity e_parity = (enum usart_hw_parity) res;

	ret = runmenu0_s(file, 35, 10, 0, 10, vt_usart_hw_mode_struct, &res);
	ASSERT_NULL(ret, __ui_query_error);
	enum usart_hw_mode e_mode = (enum usart_hw_mode) res;

	ret = runmenu0_s(file, 40, 10, 0, 10, vt_usart_hw_baudrates_struct, &res);
	ASSERT_NULL(ret, __ui_query_error);
	enum usart_hw_baudrates e_baud = (enum usart_hw_baudrates) res;

	usart_hw_init_helper(hw_setup, e_hw_clock, e_char_size, e_stop_bit, e_parity, e_mode);
	usart_hw_init_helper2(hw_setup, USART_ENABLE_TXE|USART_ENABLE_RXE); //<-asynchroneous mode
	usart_hw_init_helper3(hw_setup, e_baud);//38400UL

    //filling usart_setup_struct

	ret = runmenu0_s(file, 45, 10, 0, 10, vt_usart_sw_rtopermode_struct, &res);
	ASSERT_NULL(ret, __ui_query_error);
	tr_oper_mode = (enum usart_sw_RTopermode) res;

    uint8_t mask[5];

    //buffer size
    if (tr_oper_mode != USART_NON_BUFFERED)
    {
        ret = runmenu0_s(file, 45, 10, 0, 10, vt_usart_buffer_type, &res);
        ASSERT_NULL(ret, __ui_query_error);
        sb_buf_type = (enum usart_sw_buftype) res;

        if ( (tr_oper_mode == USART_BUFFERED_RX) || (tr_oper_mode == USART_BUFFERED_BOTH) )
        {
            memset(mask, 0, 5);
            ret = draw_input_dialog(file, 10, 10, usart_buf_rx_size_capt, mask, 5);
            ASSERT_NULL(ret, __ui_query_error);
            rx_size = (uint16_t) atoi ( (const char *) mask );
        }

        if ( (tr_oper_mode == USART_BUFFERED_TX) || (tr_oper_mode == USART_BUFFERED_BOTH) )
        {
            memset(mask, 0, 5);
            ret = draw_input_dialog(file, 10, 10, usart_buf_tx_size_capt, mask, 5);
            ASSERT_NULL(ret, __ui_query_error);
            tx_size = (uint16_t) atoi ( (const char *) mask );
        }
    }
    else
    {
        sb_buf_type = USART_BUF_CIRCULAR;
    }
    //pins CTS RTS

     memset(mask, 0, 5);
    ret = draw_input_dialog(file, 10, 10, usart_buf_cts_rts_pin, mask, 5);
    ASSERT_NULL(ret, __ui_query_error);
    int8_t bit_mask_rts = (int8_t) atoi ( (const char *) mask );

    if (bit_mask_rts != -1)
    {
        ret = runmenu0_s(file, 10, 10, 0, 10, vt_io_ports_captions_struct, &res);
        //try to create fd for the device
        if (ret == UI_NOTHING)
        {
            return F_FAIL;
        }
        e_port_rts = (enum ports) res;
    }
    else
    {
        bit_mask_rts = -1;
        e_port_rts = 0;
    }


    memset(mask, 0, 5);
    ret = draw_input_dialog(file, 10, 10, usart_buf_cts_rts_pin, mask, 5);
    ASSERT_NULL(ret, __ui_query_error);
    int8_t bit_mask_cts = (int8_t) atoi ( (const char *) mask );

    if (bit_mask_cts != -1)
    {
        ret = runmenu0_s(file, 10, 10, 0, 10, vt_io_ports_captions_struct, &res);
        //try to create fd for the device
        if (ret == UI_NOTHING)
        {
            fprintf_P(file, PSTR("FAIL 2\r\n"));
            return F_FAIL;
        }
        e_port_cts = (enum ports) res;
    }
    else
    {
        bit_mask_cts = -1;
        e_port_cts = 0;
    }


    usart_hw_init_helperf(uss_setup, hw_setup, e_usart_hw, tr_oper_mode, sb_buf_type, rx_size, tx_size);
	usart_hw_init_helperio(uss_setup, e_port_rts, bit_mask_rts, e_port_cts, bit_mask_cts);

	return F_OK;

__ui_query_error:
	return F_FAIL;

}

int8_t vt_attach_usart(FILE * file, void * data)
{
	int8_t res = 0;

    struct usart_setup_struct * uss_setup = (struct usart_setup_struct *) data;
    struct usart_hw_setup * hw_setup = (struct usart_hw_setup *) (data+sizeof(struct usart_setup_struct));

	res = __ui_query(file, uss_setup, hw_setup);
    ASSERT_DATA(res == 1, return F_FAIL);

    return F_OK;
}

int8_t vt_modify_usart(FILE * file, struct dev_descr_node * dd)
{
	/*int8_t res = 0;
	size_t ln = sizeof(struct usart_setup_update);
	struct usart_setup_update setup_env;
	struct usart_hw_setup hw;
	setup_env.hw_set = &hw;
	res = __ui_query(setup_env.hw_set, &(setup_env.e_usart_hw), &(setup_env.frame_format), &(setup_env.tr_oper_mode));
	ASSERT_DATA(res != F_OK, return F_FAIL);

	res = kernel_fd_update(fd->dev_fd, &setup_env, &ln);

	ASSERT_DATA(res == 1, return F_FAIL);
*/
	return F_OK;
}

int8_t vt_info_usart(FILE * file, struct dev_descr_node * dd)
{
	//print information summery
	/*clrscr();
	gotoxy(1,1);

	kernel_fd_query(fd->dev_fd, NULL, NULL);

	printf_P(PSTR("%S\r\n"), label_finished_e);
	while (!_kbhit());
	(void) _getch();
*/
	return F_FAIL;
}
