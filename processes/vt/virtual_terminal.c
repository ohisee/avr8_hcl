/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include "../../hal/hal.h"
#include "../../hal/lib/stream/stream.h"
#include "../../hal/haldevs.h"
#include "./virtual_terminal.h"
#include "../../lib/conio/conio.h"
#include "../../lib/conio/kbd.h"
#include "../../lib/conio/ui.h"
#include "./fd_attach/vt_attach.h"
#include "./vt_text.h"

#include "../../hal/drivers/eeprom/eeprom.h"

//os
#include "../../os/kernel/kernel_process.h"

#include "../dhclient/libnetwork.h"

//#include "./fd_attach/vt_attach.h"

#define VT_PROC_STACK_SIZE 350
#define VT_TERMINAL_RAW_PORT 23

static k_pid vt100s_pid = K_NO_PID;
static k_pid vt100n_pid = K_NO_PID;
static FILE * net_file = NULL;
static struct kstream * ks = NULL;

#if BUILD_WITH_TOP == YES
static void ui_top(FILE * file);
#endif // BUILD_WITH_TOP
static void __vt_information_window(FILE * file);
static void __vt_show_eeprom(FILE * file);
static void __vt_eepr_conf(FILE * file);
static void __vt_eepr_writing(FILE * file, t_dev_count inp);
static void __vt_eepr_error(FILE * file, t_dev_count inp);
static void __vt_manage_dev(FILE * file);
static int8_t __man_pressed_c(FILE * file);
static int8_t __man_pressed_e(FILE * file, int16_t offset);
static int8_t __man_pressed_v(FILE * file);
static int8_t __man_pressed_d(FILE * file, int16_t offset);
static int8_t __man_pressed_s(FILE * file, int16_t offset);
static void __vt_error_list(FILE * file);
static void __vt_streams(FILE * file);
static void __vt_io_allocs(FILE * file);
static void drawmem(FILE * file, const unsigned char *mem);
static void __memviewer(FILE * file);
static void __netstat(FILE * file);
static void vt100_serial(void * arg);

static int8_t vt100_onopen(struct knetdevice * kndev, const struct knetstate * knetst);
static int8_t vt100_onclose(struct knetdevice * kndev, const struct knetstate * knetstate);
static int8_t vt100_onrcv(struct knetdevice * kndev,
                        struct knetstate * state,
                        uint8_t *frame,
                        uint16_t len);



void
vt_main()
{
    FILE * serial0_file = hal_dopen(hal_node_get(SERAIL_STDIO_RW_DEVICE_NUMBER), FOPEN_RW, NULL);
    stdout = serial0_file;
    stdin = serial0_file;
    printf_P(PSTR("Creating VT100\r\n"));
	//create process
	vt100s_pid = kernel_create_process(PSTR("vt100serial"), KPROC_TYPE_SYSTEM, VT_PROC_STACK_SIZE, vt100_serial, serial0_file);
	if (vt100s_pid == K_NO_PID)
	{
		printf_P(PSTR("Can not create process vt100serial\r\n"));
	}
    printf_P(PSTR("Created VT100\r\n"));
	return;
}

void
vt_main_telnet(t_dev_id did)
{
    struct knetlisten_handlers hndlrs;
    hndlrs.ONOPEN = vt100_onopen;
    hndlrs.CLOSED = vt100_onclose;
    hndlrs.ONRCV = vt100_onrcv;

    struct knetlisten * sock = hal_net_listen_socket(KNET_TCP, 0, VT_TERMINAL_RAW_PORT, &hndlrs);
    //t_listen_id lid = hal_open_server_socket(0, 23, KNET_TCP, &hndlrs);
    if (sock == NULL)
    {
        //error
    }

    return;
}

static int8_t
vt100_onopen(struct knetdevice * kndev, const struct knetstate * knetst)
{
    //check if process is active, if so, then terminate connection

    if (net_file == NULL)
    {
        net_file = hal_dopen(kndev->ddn, FOPEN_RW, knetst);
    }
    else
    {
        return 1; //cant accept connection
    }

    if (vt100n_pid == K_NO_PID)
    {
        vt100n_pid = kernel_create_process(PSTR("vt100network"), KPROC_TYPE_SYSTEM, VT_PROC_STACK_SIZE, vt100_serial, net_file);
        if (vt100n_pid == K_NO_PID)
        {
            printf_P(PSTR("Can not create process vt100serial\r\n"));
            return 1;
        }

    }
    else
    {
        if (kernel_process_restart(vt100n_pid, vt100_serial, net_file) != 0)
        {
            //error
        }
        //todo resume and restart
    }

    return 0;   //accept connection
}

static int8_t
vt100_onclose(struct knetdevice * kndev, const struct knetstate * knetstate)
{
    //suspend process
    kernel_process_suspend(vt100n_pid);
    //close file
    hal_dclose(net_file);
    net_file = NULL;

    return 0;
}

static int8_t
vt100_onrcv(struct knetdevice * kndev,
            struct knetstate * state,
            uint8_t *frame,
            uint16_t len)
{
    return 0;
}

static void
vt100_serial(void * arg)
{
    FILE * fileio = (FILE *) arg;
	//printf("%c[%dmHELLO!\n", 0x1B, 32);
    textbackground(fileio, BLACK);
    clrscr(fileio);
    cursoroff(fileio);

	while (1)
	{
	//	_putch(_getch());
		//main menu
		textbackground(fileio, BLACK);
		clrscr(fileio);
		textattr(fileio, BLACK<<4 | YELLOW);
		labelf_P(fileio, 1, 1, PSTR("Firmware V%i / UI %S mcurs:%i"), FIRMWARE_VER, VT_VERSION, ret_mcusr);
//		fileflush(fileio);
		int ret = runmenu0(fileio, 10, 10, 0, 10, main_menu);

		switch (ret)
		{
#if BUILD_WITH_TOP == YES
			case VT_MAIN_PROCESS:
				ui_top(fileio);
			break;
#endif // BUILD_WITH_TOP
			case VT_MAIN_MANAGE:
				__vt_manage_dev(fileio);
			break;

			case VT_MAIN_STREAMS:
                __vt_streams(fileio);
            break;

            case VT_MAIN_IO_PINS:
                __vt_io_allocs(fileio);
            break;

            case VT_MAIN_SAVECFG:
            {
                int ret = runmenu1(fileio, 10, 10, 0, save_changes_to_eeprom, yesno_menu);
                if (ret == VT_YES)
                {
                    runinfod0(fileio, 10, 10, false, do_work_in_prog, do_not_power_down);
                    hal_dump_config(fileio, __vt_eepr_writing, __vt_eepr_error);
                }
            }
            break;

			case VT_MAIN_CLEAR_EE:

				ret = runmenu0(fileio, 30, 10, 0, 5, eeprom_menu);

				switch (ret)
				{
					case VT_EEPR_CLEAN:
						hal_clean_eeprom(fileio);
						__vt_eepr_conf(fileio);
//						fileflush(fileio);
					break;

					case VT_EEPR_RESET:
						drawframe(fileio, 10, 10, 50, 10, WHITE | BLUE <<4);
						putlabel(fileio, 25, 12, YELLOW | BLUE <<4, label_eepr_d);
						__vt_eepr_writing(fileio, 0);
						eeprom_clear();
						labelf_P(fileio, 14, 15, PSTR("%S"), label_finished_e);
//                        fileflush(fileio);
						while (1)
						{
							if (_getch(fileio) == KB_ENTER)
							{
								break;
							}
						}

					break;

					case VT_EEPR_BACK:
					break;
				}

			break;

			case VT_MAIN_ERROR_LIST:
				__vt_error_list(fileio);
			break;

			case VT_MAIN_REBOOT:

			break;

			case VT_MAIN_SHUTDOWN:
				//sent MCU to deep sleep
			break;

			case VT_SHOW_EEPORM:
                __vt_show_eeprom(fileio);
            break;
#if BUILD_WITH_SRAM_VIEWER == YES
            case VT_SRAM_VIEWER:
                __memviewer(fileio);
            break;
#endif // BUILD_WITH_SRAM_VIEWER
            case VT_NETSTAT_VIEWER:
                __netstat(fileio);
            break;
			case UI_NOTHING:

			break;
		}

	}

	return;
}

static void
__vt_show_eeprom(FILE * file)
{
    textbackground(file, BLACK);
    clrscr(file);
    cursoroff(file);

    putlabel(file, 1, 1, YELLOW | BLACK << 4, label_tb3_title);
    putlabel(file, 1, 3, WHITE | BLACK << 4, label_tb0_no);
    putlabel(file, 6, 3, WHITE | BLACK << 4, label_tb0_did);
    putlabel(file, 15, 3, WHITE | BLACK << 4, label_tb0_drv);
    putlabel(file, 37, 3, WHITE | BLACK << 4, label_tb0_hnd);

    t_dev_count i = 0;
    struct dev_descr_node_envilope envlp;

    while (hal_read_eeprom_offset(i, &envlp) == 0)
    {
        PGM_P p;
        PGM_P p2;
        memcpy_P(&p, &dd_dev_captions[envlp.e_dev_descr], sizeof(PGM_P));
        memcpy_P(&p2, &d_handlers_captions[envlp.e_dh_code], sizeof(PGM_P));

        //labelf_P(1, 4+i, PSTR("%i\t%i\t%S\t%S\t%p"),
        //((page*page_sz)+i), fddl.dev_id, p, p2, &fddl);
        labelf_P(file, 1, 4+i, PSTR("%d"), i);
        labelf_P(file, 6, 4+i, PSTR("%d"), envlp.dev_id);
        labelf_P(file, 15, 4+i, PSTR("%S"), p);
        labelf_P(file, 37, 4+i, PSTR("%S"), p2);
        i++;
    }

    labelf_P(file, 1, 25, PSTR("TOTAL: %d EEMEM:magic:%x DEF:%x"), hal_read_eeprom_recs_am(), hal_eeprom_read_magic(), EEPROM_MAGIC);

//    fileflush(file);

    while (1)
    {
        _wait_for_kbhit(file);
        {
            switch(_getch(file))
            {
                case KB_ESC:
                     return;
                break;

                default:
                break;
            }
        }
    }

    return;
}

static void
__vt_manage_dev(FILE * file)
{
	uint8_t itm_sel = 0;
	int8_t page = 0;
	uint8_t i = 0;
	uint8_t itmsc = 0;
	t_dev_count pages = 0;
	int ret = 0;
	uint8_t updall = 1;
	const uint8_t page_sz = 10;
	while (1)
	{

        if (updall)
        {
            textbackground(file, BLACK);
            clrscr(file);
		    cursoroff(file);
            putlabel(file, 1, 1, YELLOW | BLACK << 4, label_devices);
            putlabel(file, 1, 20, WHITE | BLACK << 4, label_dev_add);
            putlabel(file, 38, 20, WHITE | BLACK << 4, label_dev_add_h);
            putlabel(file, 1, 21, WHITE | BLACK << 4, label_dev_edit);
            putlabel(file, 1, 22, WHITE | BLACK << 4, label_dev_rem);
            putlabel(file, 1, 23, WHITE | BLACK << 4, label_dev_sel);
            putlabel(file, 1, 24, WHITE | BLACK << 4, label_dev_opts);
            putlabel(file, 38, 23, WHITE | BLACK << 4, label_dev_exit);
            putlabel(file, 38, 22, WHITE | BLACK << 4, label_dev_right);
            putlabel(file, 38, 21, WHITE | BLACK << 4, label_dev_left);
            putlabel(file, 1, 3, WHITE | BLACK << 4, label_tb0_no);
            putlabel(file, 6, 3, WHITE | BLACK << 4, label_tb0_did);
            putlabel(file, 15, 3, WHITE | BLACK << 4, label_tb0_drv);
            putlabel(file, 37, 3, WHITE | BLACK << 4, label_tb0_hnd);
            putlabel(file, 55, 3, WHITE | BLACK << 4, label_tb0_mem);
            textattr(file, GREEN | BLACK << 4);
            labelf_P(file, 1, 25, label_devs, hal_fd_get_total(), hal_get_max_devs());
            //draw registered devices

            itm_sel = 0;
            page = 0;
            pages = (t_dev_count) hal_fd_get_total() / page_sz; //count max pages
            i = 0;
            ret = 0;
            updall = 0;
        }

		struct dev_descr_node fddl;
		int8_t ret = 0;
		while (updall == 0)
		{
			textattr(file, GREEN | BLACK << 4);
			labelf_P(file, 1, 18, label_pages, page, pages);

			textattr(file, WHITE | BLACK << 4);


			for (i=0;i<page_sz;i++)
			{
			    ret = hal_dd_mem_cpy(i + (page*page_sz), &fddl);
				uint8_t j = 0;
				if (i == itm_sel)
				{
					textattr(file, BLACK | WHITE << 4);
				}
				else
				{
					textattr(file, WHITE | BLACK << 4);
				}

				if (ret == K_NO_DD)
				{
					++itmsc;
					gotoxy(file, 1, 4+i);
					for (j = 0; j <=80; j++) _putch(file, ' ');
					//labelf_P(1, 3+i, PSTR("---<EOF>---"));
					//++i;
					//--i;
					//break;
					continue;
				}

				gotoxy(file, 1, 4+i);
				for (j = 0; j <=80; j++) _putch(file, ' ');
				PGM_P p;
				PGM_P p2;
				memcpy_P(&p, &dd_dev_captions[fddl.e_dev_descr], sizeof(PGM_P));
				memcpy_P(&p2, &d_handlers_captions[fddl.e_dh_code], sizeof(PGM_P));

				//labelf_P(1, 4+i, PSTR("%i\t%i\t%S\t%S\t%p"),
				//((page*page_sz)+i), fddl.dev_id, p, p2, &fddl);
				labelf_P(file, 1, 4+i, PSTR("%d"), ((page*page_sz)+i));
				labelf_P(file, 6, 4+i, PSTR("%d"), fddl.dev_id);
				labelf_P(file, 15, 4+i, PSTR("%S"), p);
				labelf_P(file, 37, 4+i, PSTR("%S"), p2);
				labelf_P(file, 55, 4+i, PSTR("%p"), fddl.instance);

			 }

//			 fileflush(file);

			  i = i - itmsc;
			  itmsc = 0;

				 // key = _getch();
				  switch(_getch(file))
				  {
					  case KB_UP:
					      if (itm_sel>0) itm_sel--;
					      else itm_sel = i-1;
                      break;
					  case KB_DOWN:
					      itm_sel++;
					      itm_sel %= i;
                      break;
					  case KB_LEFT:
							itm_sel = 0;
							if (page > 0) page--;
							else page = pages;

					  break;

					  case KB_RIGHT:
 							itm_sel = 0;
 							page++;
 							page %= pages;
					  break;

					  case 'C':
					  case 'c':
						ret = __man_pressed_c(file);
						updall = 1;
					  break;

					  case 'V':
					  case 'v':
						ret = __man_pressed_v(file);
						updall = 1;
					  break;

					  case 'S':
                      case 's':
                        ret = __man_pressed_s(file, itm_sel + (page*page_sz));
                        updall = 1;
                      break;

					  case KB_ENTER:
						{
                            ret = hal_dd_mem_cpy(itm_sel + (page*page_sz), &fddl);
                            if (ret != K_NO_DD)
                            {
                                struct hal_query_info hqi = {HQO_INFO_TO_STDOUT, (void*)file , NULL, NULL, NULL};
                                //clear screen
                                gotoxy(file, 1,1);
                                textbackground(file, BLACK);
                                clrscr(file);
                                //send request
                                t_dev_id res = hal_dev_node_query(&fddl, &hqi);
                                if (res == K_NO_DD)
                                {
                                    runerrord1(file, 10, 10, 40, label_query_fail, label_query_devf);
                                }
                            }
                            else
                            {
                                runerrord1(file, 10, 10, 40, label_query_fail, label_query_ddf);
                            }

							ret = 1;
							while (!_kbhit(file));
                            (void) fgetc(file);
						}
						updall = 1;
					  break;

					  case 'E':
					  case 'e':
					  {
                            __man_pressed_e(file, itm_sel + (page*page_sz));
						/*	struct fd_dev_node * fd_c = kernel_fd_get((int8_t)page*page_sz+itm_sel);
							if (fd_c == NULL)
							{
								runerrord0(3,3, error_capt, fd_ret_null);
							}

							uint8_t res = 0;

							if (fd_c->d_hndl == NULL_DEV)
							{
								res = vt_attach_driver(fd_c,  fd_c->e_dev, VT_ATT_MODIFY);
							}
							else
							{
								res = vt_attach_handler(fd_c,  fd_c->d_hndl, VT_ATT_MODIFY);
							}

							if (res != 0)
							{
								runerrord0(3,3, error_capt, query_failed);
							}*/
							ret = 1;
					  }
                        updall = 1;
					  break;

					  case 'D':
					  case 'd':
                      {
                          __man_pressed_d(file, itm_sel + (page*page_sz));
							//remove device
						/*	int8_t kfd = kernel_fd_destroy((int8_t)page*page_sz+itm_sel); //TODO FIX
							if (kfd == K_NO_FD)
							{
								runerrord1(6,5,21, error_capt, PSTR("%S ID:%i"), unable_to_remove, (page*page_sz+itm_sel));
							}
*/
							ret = 1;
					  }
                        updall = 1;
					  break;
					  case KB_F5:
				  			//ask to save changes in eeprom
				  			/*ret = runmenu1(file, 10, 10, 0, save_changes_to_eeprom, yesno_menu);
				  			if (ret == VT_YES)
				  			{
				  				runinfod0(file, 10, 10, false, do_work_in_prog, do_not_power_down);
				  				hal_dump_config(file, __vt_eepr_writing, __vt_eepr_error);
				  			}*/
				  			//cursoron(file);
				  			ret = runmenu1(file, 10, 10, 0, label_exit_conf, yesno_menu);
				  			if (ret == VT_YES)
				  			{
				  			    return;
				  			}
				  			ret = 1;
                            updall = 1;
					  break;
					  default:

					  break;
				  }
		}
	}
}

static int8_t __man_pressed_s(FILE * file, int16_t offset)
{
    struct dev_descr_node descr_data;
    int8_t ret = hal_dd_mem_cpy(offset, &descr_data);
    if (ret != 0)
    {
        runerrord0(file, 3,3, error_capt, dd_dev_memcpy_failed);
        return 1;
    }

    //retrieving node
    struct dev_descr_node * node = hal_node_get(descr_data.dev_id);

    if (node == NULL)
    {
        runerrord0(file, 3,3, error_capt, dd_ret_null);
        return 1;
    }

    ret = vt_extra_options(file, node);
    if (ret == F_OK)
    {
        return F_OK;
    }
    else
    {
        runerrord0(file, 20,3, error_capt, label_query_fail);
    }

    return F_FAIL;
}

static int8_t __man_pressed_c(FILE * file)
{
	int ret = runmenu0(file, 10, 10, 0, 10, dd_dev_captions);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return 1;
	}

	//call dialogs to collect data


    ret = vt_attach_driver(file, NULL, (enum dev_descr_code) (ret-1));
    if (ret == F_OK)
    {
        return 0;
    }
    else
    {/*while (!_kbhit(file));
    while (!_kbhit(file));*/
        runerrord0(file, 3,10, error_capt, no_fd_created);
    }
/*	fd_dev_id fid = kernel_fd_create();
	if (fid == K_NO_FD)
	{
		//show error dialog
		runerrord0(3,3, error_capt, no_fd_created);
		return 1;
	}

	//attach to the device (ret - 1 )
	struct fd_dev_node * c_fd = kernel_fd_get(fid);
	uint8_t res = vt_attach_driver(c_fd,  ret-1, VT_ATT_ATTACH);

	if (res == F_FAIL)
	{
		runerrord0(3,3, error_capt, no_fd_created);
		kernel_fd_destroy(fid);
		return 1;
	}
*/
	return 1;
}

static int8_t __man_pressed_e(FILE * file, int16_t offset)
{
    struct dev_descr_node descr_data;
    int8_t ret = hal_dd_mem_cpy(offset, &descr_data);
    if (ret != 0)
    {
        runerrord0(file, 3,3, error_capt, dd_dev_memcpy_failed);
        return 1;
    }

    //retrieving node
    struct dev_descr_node * node = hal_node_get(descr_data.dev_id);

    if (node == NULL)
    {
        runerrord0(file, 3,3, error_capt, dd_ret_null);
        return 1;
    }

    ret = vt_modify_node(file, node);
    if (ret == F_OK)
    {
        return 0;
    }
    else
    {
        runerrord0(file, 3,3, error_capt, query_failed);
        return 1;
    }
    return 1;
}

static int8_t __man_pressed_d(FILE * file, int16_t offset)
{
    struct dev_descr_node descr_data;
    int8_t ret = hal_dd_mem_cpy(offset, &descr_data);
    if (ret != 0)
    {
        runerrord0(file, 3,3, error_capt, dd_dev_memcpy_failed);
        return 1;
    }

    //retrieving node
    struct dev_descr_node * node = hal_node_get(descr_data.dev_id);

    if (node == NULL)
    {
        runerrord0(file, 3,3, error_capt, dd_ret_null);
        return 1;
    }

    t_dev_id res = hal_dev_node_destroy(node);
    if (res != 0)
    {
        runerrord0(file, 3,3, error_capt, unable_to_remove);
        return 1;
    }

    return 0;
}

static int8_t __man_pressed_v(FILE * file)
{

    int ret = runmenu0(file, 10, 10, 0, 10, dd_dev_captions);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return 1;
	}

	enum dev_descr_code ddc = (enum dev_descr_code) (ret-1);
    const uint8_t * hndlr_ids;

    switch (ddc)
    {
        case DEV_DESCR_SPI:
            ret = runmenu0(file, 14, 6, 0, 10, spi_handlers_captions);
            hndlr_ids = &spi_handlers_ids[ret-1];
        break;

        case DEV_DESCR_OWI:
            ret = runmenu0(file, 14, 6, 0, 10, owi_handlers_captions);
            hndlr_ids = &owi_handlers_ids[ret-1];
        break;

        case  DEV_DESCR_IO:
            ret = runmenu0(file, 14, 6, 0, 10, io_handlers_captions);
            hndlr_ids = &io_handlers_ids[ret-1];
        break;

        case DEV_DESCR_VIRTUAL:
            ret = runmenu0(file, 14, 6, 0, 10, virtual_handlers_captions);
            hndlr_ids = &virtual_handlers_ids[ret-1];
        break;

        default:
            runerrord0(file, 3,3, error_capt, not_supported);
        break;
    }

    if (ret == UI_NOTHING)
    {
        return 1;
    }

    //fetching id number
    enum dev_desc_handlers ddh = (enum dev_desc_handlers) pgm_read_byte(hndlr_ids);//&spi_handlers_ids[ret-1]);

	//call dialogs to collect data
	gotoxy(file, 10, 9);
	fprintf_P(file, PSTR("DDC: %u DDH: %u\r\n"), ddc, ddh);
    ret = vt_attach_handler(file, NULL, ddc, ddh);
    if (ret == F_OK)
    {
        return 0;
    }
    else
    {
        runerrord0(file, 3,3, error_capt, no_fd_created);
    }
/*	fd_dev_id fid = kernel_fd_create();
	if (fid == K_NO_FD)
	{
		//show error dialog
		runerrord0(3,3, error_capt, no_fd_created);
		return 1;
	}

	//attach to handler
	struct fd_dev_node * c_fd = kernel_fd_get(fid);
	uint8_t res = vt_attach_handler(c_fd,  ret-1, VT_ATT_ATTACH);
	if (res == F_FAIL)
	{
		runerrord0(3,3, error_capt, no_fd_created);
		kernel_fd_destroy(fid);
	}
*/
	return 1;
}

static void __vt_eepr_conf(FILE * file)
{
	drawframe(file, 10, 10, 50, 4, WHITE | BLUE <<4);
	putlabel(file, 12, 12, YELLOW | BLUE <<4, label_eepr_d2);

//	fileflush(file);

	while (1)
	{
		if (_getch(file) == KB_ENTER)
		{
			break;
		}
	}

	return;
}

static void __vt_eepr_writing(FILE * file, t_dev_count inp)
{
	labelf_P(file, 12, 13, PSTR("Writing device: %d "), inp);
	return;
}

static void __vt_eepr_error(FILE * file, t_dev_count inp)
{
    runerrord1(file, 3,3, error_capt, PSTR("Writing device [%d] failed"), inp);
	return;
}

static void
__vt_streams(FILE * file)
{
    textbackground(file, BLACK);
    clrscr(file);
	cursoroff(file);

	putlabel(file, 1, 1, YELLOW | BLACK << 4, label_tb1_title);
	putlabel(file, 1, 3, GREEN | BLACK << 4, label_tb1_num);
    putlabel(file, 6, 3, WHITE | BLACK << 4, label_tb1_addr);
    putlabel(file, 20, 3, WHITE | BLACK << 4, label_tb1_flags1);
    putlabel(file, 27, 3, WHITE | BLACK << 4, label_tb1_dest);
    putlabel(file, 39, 3, WHITE | BLACK << 4, label_tb1_ext);
    putlabel(file, 53, 3, WHITE | BLACK << 4, label_tb1_flags2);


    struct hal_stream_envelope env = {0};
    t_stream_id sno = 0;

    while (hal_cpy_stream_mem(sno, &env) != 1)
    {
        textattr(file, GREEN | BLACK << 4);
        labelf_P(file, 1, 4+sno, PSTR("%d"), env.sid);
        textattr(file, WHITE | BLACK << 4);
        labelf_P(file, 27, 4+sno, PSTR("DID:%d[%p]"), env.dest->dev_id, env.dest);
        labelf_P(file, 6, 4+sno, PSTR("%p"), env.node);
        labelf_P(file, 20, 4+sno, PSTR("%X"), env.node->flags);
        labelf_P(file, 39, 4+sno, PSTR("EXT:%p"), env.node->udata);


        sno++;
    }

    labelf_P(file, 1, 24, PSTR("streams: %d stdin: [%p] stdout: [%p]"), sno, stdin, stdout);
    putlabel(file, 1, 25, RED | BLACK << 4, label_netstat_arp_bottom);

//    fileflush(file);

    while (1)
    {
        _wait_for_kbhit(file);
        {
            switch(_getch(file))
            {
                case KB_F5:
                     return;
                break;

                default:
                break;
            }
        }
    }

    return;
}

static void
__vt_io_allocs(FILE * file)
{
    textbackground(file, BLACK);
    clrscr(file);
	cursoroff(file);

    putlabel(file, 1, 1, YELLOW | BLACK << 4, label_tb2_title);
	putlabel(file, 1, 3, GREEN | BLACK << 4, label_tb2_recno);
    putlabel(file, 10, 3, WHITE | BLACK << 4, label_tb2_owner);
    putlabel(file, 24, 3, WHITE | BLACK << 4, label_tb2_port);
    putlabel(file, 30, 3, WHITE | BLACK << 4, label_tb2_pins);

    struct hw_port_bind_rec hwn = {0};
    uint8_t i = 0;

    while (hal_hw_mem_cpy(i, &hwn) != 1)
    {
        textattr(file, GREEN | BLACK << 4);
        labelf_P(file, 1, 4+i, PSTR("%d"), i);
        textattr(file, WHITE | BLACK << 4);
        labelf_P(file, 10, 4+i, PSTR("DID:%d"), hwn.idev);
        labelf_P(file, 24, 4+i, PSTR("%d"), hwn.nport);
        labelf_P(file, 30, 4+i, PSTR("%X"), hwn.pins);
        i++;
    }

    putlabel(file, 1, 25, RED | BLACK << 4, label_netstat_arp_bottom);

//    fileflush(file);

    while (1)
    {
        _wait_for_kbhit(file);
        {
            switch(_getch(file))
            {
                case KB_F5:
                     return;
                break;

                default:
                break;
            }
        }
    }

    return;
}

static void __vt_information_window(FILE * file)
{

__vt_information_window_ref:
	drawframe(file, 10, 10, 50, 10, WHITE | BLUE <<4);
	//putlabelint(12, 12, YELLOW | BLUE <<4, 29, label_ram, kernel_get_heap_stack_gap());

	drawgauge2(file, 12, 13, 48, 12, 15, 'H', 40, 48, 'S');

	//putlabelint(12, 14, YELLOW | BLUE <<4, 29, label_ticks, kernel_timer_get_sec());
//    fileflush(file);
	while (1)
	{


		while (!_kbhit(file));
		 switch(_getch(file))
		 {
			 case KB_ESC:
				return;
			 case KB_F5:
				goto __vt_information_window_ref;
			 default:
				break;
		 }

	}


	return;
}

static void __vt_error_list(FILE * file)
{
	drawframe(file, 10, 5, 9, 11, WHITE | BLUE <<4);
	putlabel(file, 11, 6, WHITE | BLUE << 4, PSTR("Errors:\0"));
	textattr(file, RED | BLUE << 4);
	uint8_t i = 0;
	for (; i < MAX_ERROR_SIZE; i++)
	{
		labelf_P(file, 12, 7+i, PSTR("%c%u"), (i == getSysEPos()) ? '>' : ' ', getSysErrorAt(i));
	}

	i++;
	labelf_P(file, 11, 7+i, PSTR("F5 - close"));

//	fileflush(file);

	while (1)
	{
		 if (_kbhit(file))
		 {
			 switch(_getch(file))
			 {
				 case KB_F5: return;
				 default:
				 break;
			 }
		 }
	}
}

#if BUILD_WITH_SRAM_VIEWER == YES
const char hexchars[16] PROGMEM = "0123456789ABCDEF";
const char m_quit[] PROGMEM = "Quit";
const char * const memview_fkeys[10] PROGMEM = {
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    m_quit
};

static void
drawmem(FILE * file, const unsigned char *mem)
{
	const unsigned char *mptr;
	int i,j;
	textbackground(file, BLUE);
	for (i=0;i<23 && !_kbhit(file);i++) {
		mptr = &mem[i*16];
		gotoxy(file, 1,i+2);
		textcolor(file, YELLOW);
		j = 8;
		do {
			j--;
			_putch(file, pgm_read_byte(&hexchars[  (((int)mptr)>>(j*4)) &0xF] ));
		} while (j>0);
		_cputs_P(file, PSTR(":  "));
		textcolor(file, WHITE);

		for (j=0;j<16;j++) {
			_putch(file, pgm_read_byte(&hexchars[*mptr>>4]));
			_putch(file, pgm_read_byte(&hexchars[*mptr&0xF]));
			mptr++;
			_putch_P(file, PSTR(" "));

			if (j == 7)
            {
				_putch_P(file, PSTR("-"));
				_putch_P(file, PSTR(" "));
			}
		}

		_putch(file, PSTR("\179"));
		_putch_P(file, PSTR(" "));

		mptr = &mem[i*16];
		for (j=0;j<16;j++) {
			if (*mptr >= ' ')
			_putch(file, *mptr);
			else _putch_P(file, PSTR("."));
			mptr++;
		}

		clreol(file);
	}
}


static void
__memviewer(FILE * file)
{
    uint8_t * start = 0;
	int key;
	const unsigned char *mem;

	textattr(file, LIGHTGRAY  | BLUE <<4);
	clrscr(file);
	textcolor(file, BLACK);
	textbackground(file, CYAN);
	fprintf_P(file, PSTR("SRAM Memory viewer"));
	clreol(file);

	cursoroff(file);
	drawfkeys(file, memview_fkeys);

	mem=(const unsigned char *)start;
	for (;;) {
		drawmem(file, mem);
//        fileflush(file);
		//   if (_kbhit()) {
		key = _getch(file);
		switch(key) {
			case KB_LEFT:
			    if ((int)mem > 0 )
                {
                    (int)mem--;
                }
            break;
			case KB_RIGHT:
			    if ((mem+1) < RAMEND)
			    mem++;
            break;
			case KB_UP:
			    if (mem >= (unsigned char const *)16)
                {
                    mem-=16;
                }
            break;
			case KB_DOWN:
			    mem+=16;
            break;
			case KB_PGUP:
			    if (mem >= (unsigned char const *)(16*23))
                {
                    mem-=16*23;
                }
            break;
			case KB_PGDN:
                if ( RAMEND >= (unsigned char const *)(uint16_t)mem+(16*23))
                {
                    mem+=16*23;
                }
            break;
			case KB_F10:
			    //cursoron();
			    return;
		}
		// }

	}
}
#endif // BUILD_WITH_SRAM_VIEWER

#if BUILD_WITH_TOP == YES

#include <time.h>
#include "../../os/kernel/kernel_memory.h"
#include "../../os/kernel/kernel_mutex.h"
#include "../../os/kernel/kernel_sem.h"
#include "../../os/kernel/kernel_fifo.h"
#include "../../os/kernel/kernel_isr.h"

static void
ui_top(FILE * file)
{

    uint8_t exit = 0;
    struct kproc_mutex * mx = NULL;
    struct kprocess proc;
    while(exit != 2)
    {
        exit = 0;

        cursoroff(file);
        textbackground(file, BLACK);
        clrscr(file);
        textcolor(file, YELLOW);
        textbackground(file, BLUE);
        gotoxy(file, 1, 25);
        fprintf_P(file, PSTR("Press F5 to exit. Press F1 to inspect"));
        gotoxy(file, 1,4);
        clreol(file);
        fprintf_P(file, PSTR("PID"));
        gotoxy(file, 8,4);
        fprintf_P(file, PSTR("STS"));
        gotoxy(file, 16,4);
        fprintf_P(file, PSTR("STC"));
        gotoxy(file, 24,4);
        fprintf_P(file, PSTR("STE"));
        gotoxy(file, 32,4);
        fprintf_P(file, PSTR("MEM%%"));
        gotoxy(file, 40,4);
        fprintf_P(file, PSTR("CPU%%"));
        gotoxy(file, 48,4);
        fprintf_P(file, PSTR("FLAGS"));
        gotoxy(file, 56,4);
        fprintf_P(file, PSTR("STATE"));
        gotoxy(file, 64,4);
        fprintf_P(file, PSTR("TITLE"));

        while(!exit)
        {
            uint16_t pids = kernel_processes_get_amount();
            k_iid isrs = kernel_isr_get_amount();

            cursoroff(file);
            gotoxy(file, 1,1);
            textbackground(file, BLACK);
            clreol(file);
            textcolor(file, YELLOW);
            textbackground(file, BLUE);

            uint8_t proc_cpu = kernel_get_proc_cpu_utiliz();
            uint32_t proc_isr = kernel_get_isr_cpu_msec();
            k_proc_stack * com_stack = kernel_get_common_stack_address();
            uint16_t sa = (uint16_t)com_stack - ((uint16_t)com_stack - HEAP_ENDS_AT);
            fprintf_P(file, PSTR("Processes: %u CPU: %u%% ISR: %lu(ms) SYS:%u%%\r\n"), pids, proc_cpu, proc_isr, 100-proc_cpu);
            fprintf_P(file, PSTR("Memory: TH:%uB HA:%uB MD:%u Frag:%uB HL:%uB ST[%p:%p=%uB]\r\n"),
                kernel_get_total_heap(), kernel_get_total_heap_used(), kernel_get_total_heap_descr(),
                kernel_get_total_heap_fragm(), (kernel_get_total_heap() - kernel_get_total_heap_used()),
                com_stack, sa, ((uint16_t)com_stack - HEAP_ENDS_AT));
            //time
            gotoxy(file, 62, 1);
            time_t ms = kernel_get_seconds();
            struct tm * tmm = gmtime(&ms);
            fprintf_P(file, PSTR("%u:%u:%u:%u"), (tmm->tm_mday-1), tmm->tm_hour, tmm->tm_min, tmm->tm_sec);

            //mutexes
            /*gotoxy(1, 6+pids+isrs);
            printf_P(PSTR("MUTEXES"));
            gotoxy(1, 10+pids+isrs);
            printf_P(PSTR("SEMAPHORES"));*/

            textcolor(file, WHITE);
            textbackground(file, BLACK);
            uint8_t y_off = 5;
            for (uint16_t pid = 0; pid < pids; pid++)
            {
                int8_t status = kernel_processes_get_procinfo(pid, &proc);
                if (status != 0)
                {
                    continue;
                }

                gotoxy(file, 1,y_off);
                clreol(file);
                fprintf_P(file, PSTR("%d"), proc.pid);
                gotoxy(file, 8,y_off);
                fprintf_P(file, PSTR("%u"), proc.stack_start);
                gotoxy(file, 16,y_off);
                fprintf_P(file, PSTR("%u"), proc.stack_saved);
                gotoxy(file, 24,y_off);
                fprintf_P(file, PSTR("%u"), proc.stack_end);
                gotoxy(file, 32,y_off);
                div_t mper = div(( (proc.stack_start - proc.stack_saved)*100 ),(proc.stack_start - proc.stack_end + 1));
                fprintf_P(file, PSTR("%u%%"), (uint16_t)mper.quot);
                gotoxy(file, 40,y_off);
                fprintf_P(file, PSTR("%u%%"), proc.cpu_usage);
                gotoxy(file, 48,y_off);
                char fr = (proc.flags & KPROC_FLAG_RUN) ? 'R' : '\0';
                char fs = (proc.flags & KPROC_FLAG_SLEEP) ? 'S' : '\0';
                char fw = (proc.flags & KPROC_FLAG_WAIT) ? 'W' : '\0';
                char fc = (proc.flags & KPROC_FLAG_COOP) ? 'C' : 'P';
                char fpm = (proc.flags & KPROC_FLAG_TYPE_SYSTEM) ? '+' : '\0';
                fprintf_P(file, PSTR("%c%c%c%c%c"), fr, fs, fw, fc, fpm);
                gotoxy(file, 56,y_off);
                fprintf_P(file, PSTR("%S"), proc.state);
                gotoxy(file, 64,y_off);
                fprintf_P(file, PSTR("%S"), proc.title);
                y_off++;
            }

            textcolor(file, RED);

            struct kisr ksr = {0};
            for (k_iid pid = 0; pid < isrs; pid++)
            {
                int8_t status = kernel_isr_get_info(pid, &ksr);
                if (status != 0)
                {
                    gotoxy(file, 1,y_off);
                    clreol(file);
                    fprintf_P(file, PSTR("ISR:internal error"));
                    y_off++;
                    continue;
                }

                gotoxy(file, 1,y_off);
                clreol(file);
                fprintf_P(file, PSTR("ISR%d"), ksr.isr_num);
                gotoxy(file, 16,y_off);
                fprintf_P(file, PSTR("N/A"));
                gotoxy(file, 25,y_off);
                fprintf_P(file, PSTR("%lu(ms)/calls:%lu"), kernel_isr_tcnt2ms(ksr.tcnt_cnt), ksr.total_cals);
                gotoxy(file, 48,y_off);
                //char in = (ksr.flags.flags & KISRF_ISR) ? 'I' : '-';
                char su = (ksr.flags.flags & KISRF_SUSPEND) ? 'S' : '-';
                char de = (ksr.flags.flags & KISRF_DELAYED) ? 'D' : '-';
                fprintf_P(file, PSTR("%u%c%c"), ksr.flags.cnt, su, de);
                gotoxy(file, 56,y_off);
                fprintf_P(file, PSTR("%p"), ksr.ISR_HANDLER);
                gotoxy(file, 64,y_off);
                fprintf_P(file, PSTR("%S"), kernel_isr_get_caption(ksr.isr_num));

                y_off++;
            }

            textcolor(file, WHITE);

                if (mx != NULL)
                {
                    gotoxy(file, 1, 6+pids+1+isrs);
                    clreol(file);
                    fprintf_P(file, PSTR("Owner:%u wating:%u order:%u counter%u"), kernel_mutex_get_cnt(mx), kernel_mutex_get_cwating(mx), kernel_mutex_get_corder(mx), kernel_mutex_get_ccounter(mx));
                }

                /*if (sem0 != NULL)
                {
                    gotoxy(1, 10+pids+1+isrs);
                    clreol();
                    printf_P(PSTR("SID:%"PRIu8" CA:%"PRIu8" FR:%"PRIu8" WA:%"PRIu8), 0, kernel_sem_get_total(sem0), kernel_sem_get_free(sem0), kernel_sem_get_waiting(sem0));
                }*/

    //        fileflush(file);
            kernel_process_usleep(1000000, 0);
            if (_kbhit(file))
            {
                switch(_getch(file))
                {
                    case KB_F5:
                         exit = 2;
                    break;
                    case KB_F1:
                    {
                        uint8_t data[4];
                        memset(data, 0, 2);
                        int16_t ret = draw_input_dialog(file, 10, 10, proc_sel, data, 4);
                        if (ret < 0)
                        {
                            runerrord0(file, 3,3, error_capt, length_assert);
                            exit = 1;
                            break;
                        }

                        k_pid pidsel = (uint8_t) atoi ( (const char *) data );

                        mx = kernel_mutex_get_proc_waiting(pidsel);
                        if (mx == NULL)
                        {
                            runerrord0(file, 3,3, proc_mux_nf, length_assert);
                            exit = 1;
                            break;
                        }
                        exit = 1;
                    }
                    break;
                    default:
                    break;
                }
            }
        }
    }

    return;
}
#endif // BUILD_WITH_TOP



static void
__netstat(FILE * file)
{
    uint8_t y = 0;
    while (1)
    {
        textbackground(file, BLACK);
        clrscr(file);
        cursoroff(file);

        y = 0;

        putlabel(file, 1, 1, YELLOW | BLACK << 4, label_netstat_title);

        putlabel(file, 1, 2, GREEN | BLACK << 4, label_netstat_if);

        putlabel(file, 1, 3, WHITE | BLACK << 4, label_netstat_if_id);
        putlabel(file, 5, 3, WHITE | BLACK << 4, label_netstat_if_did);
        putlabel(file, 9, 3, WHITE | BLACK << 4, label_netstat_if_mac);
        putlabel(file, 30, 3, WHITE | BLACK << 4, label_netstat_if_ip);
        putlabel(file, 49, 3, WHITE | BLACK << 4, label_netstat_if_mask);
        putlabel(file, 65, 3, WHITE | BLACK << 4, label_netstat_if_gateway);

        textcolor(file, WHITE);
        textbackground(file, BLACK);

        struct knetdevice_list * net_list = hal_net_mem_knetdev();
        ITERATE_BLOCK_DIRECT_MAN(struct knetdevice_list, net_list, knd)
        {
            labelf_P(file, 1, 4+y, PSTR("%"PRId8), knd->node->net_dev_id);
            labelf_P(file, 5, 4+y, PSTR("%"PRId8), knd->node->ddn->dev_id);
            labelf_P(file, 9, 4+y, PSTR("%x:%x:%x:%x:%x:%x"),
                     knd->node->mac[0], knd->node->mac[1], knd->node->mac[2],
                     knd->node->mac[3], knd->node->mac[4], knd->node->mac[5]);
            labelf_P(file, 30, 4+y, PSTR("%u.%u.%u.%u"),
                     ((uint8_t*)&knd->node->nic_set.ipv4)[0], ((uint8_t*)&knd->node->nic_set.ipv4)[1],
                     ((uint8_t*)&knd->node->nic_set.ipv4)[2], ((uint8_t*)&knd->node->nic_set.ipv4)[3]);
            labelf_P(file, 49, 4+y, PSTR("%u.%u.%u.%u"),
                     ((uint8_t*)&knd->node->nic_set.ipv4_mask)[0], ((uint8_t*)&knd->node->nic_set.ipv4_mask)[1],
                     ((uint8_t*)&knd->node->nic_set.ipv4_mask)[2], ((uint8_t*)&knd->node->nic_set.ipv4_mask)[3]);
            labelf_P(file, 65, 4+y, PSTR("%u.%u.%u.%u"),
                     ((uint8_t*)&knd->node->nic_set.ipv4_gateway)[0], ((uint8_t*)&knd->node->nic_set.ipv4_gateway)[1],
                     ((uint8_t*)&knd->node->nic_set.ipv4_gateway)[2], ((uint8_t*)&knd->node->nic_set.ipv4_gateway)[3]);

            ITERATE_NEXT(knd);
            y++;
        }

        putlabel(file, 1, 4+y, GREEN | BLACK << 4, label_netstat_listen);
        y++;

        putlabel(file, 1, 4+y, WHITE | BLACK << 4, label_netstat_lis_lid);
        putlabel(file, 5, 4+y, WHITE | BLACK << 4, label_netstat_lis_proto);
        putlabel(file, 16, 4+y, WHITE | BLACK << 4, label_netstat_if_ip);
        y++;
        textcolor(file, WHITE);
        textbackground(file, BLACK);

        struct knetlisten_list * net_lis = hal_net_mem_knetlisten();
        ITERATE_BLOCK_DIRECT_MAN(struct knetlisten_list, net_lis, kls)
        {

            labelf_P(file, 1, 4+y, PSTR("%"PRId8), kls->node->k_listen);
            if (kls->node->e_knet_proto == KNET_TCP)
            {
                labelf_P(file, 5, 4+y, PSTR("TCP"));
            }
            else if (kls->node->e_knet_proto == KNET_UDP)
            {
                labelf_P(file, 5, 4+y, PSTR("UDP"));
            }//inet_addr(255,255,255,255)
          //  uint32_t ip = htonl(kls->node->laddr);
            labelf_P(file, 16, 4+y, PSTR("%u.%u.%u.%u:%"PRIu16),
                     /*((uint8_t*)kls->node->laddr)[0], ((uint8_t*)kls->node->laddr)[1],
                     ((uint8_t*)kls->node->laddr)[2], ((uint8_t*)kls->node->laddr)[3],*/
                     ((uint8_t*)&kls->node->laddr)[0], ((uint8_t*)&kls->node->laddr)[1],
                     ((uint8_t*)&kls->node->laddr)[2], ((uint8_t*)&kls->node->laddr)[3],
                     kls->node->lport);

            ITERATE_NEXT(kls);
            y++;
        }

        putlabel(file, 1, 4+y, GREEN | BLACK << 4, label_netstat_states);
        y++;

        putlabel(file, 1, 4+y, WHITE | BLACK << 4, label_netstat_sta_id);
        putlabel(file, 5, 4+y, WHITE | BLACK << 4, label_netstat_lis_proto);
        putlabel(file, 16, 4+y, WHITE | BLACK << 4, label_netstat_if_ip);
        putlabel(file, 40, 4+y, WHITE | BLACK << 4, label_netstat_if_rip);
        y++;

        struct knetstate_list * net_sta = hal_net_mem_knetstate();
        ITERATE_BLOCK_DIRECT_MAN(struct knetstate_list, net_sta, nst)
        {

            labelf_P(file, 1, 4+y, PSTR("%"PRId8), nst->node->kstate_id);
            labelf_P(file, 5, 4+y, PSTR("%S"),
                     (nst->node->e_knet_proto == KNET_TCP) ? PSTR("TCP\0") : (nst->node->e_knet_proto == KNET_UDP) ? PSTR("UDP\0") :  PSTR("OTHER\0") );
            labelf_P(file, 16, 4+y, PSTR("%u.%u.%u.%u:%"PRIu16),
                     ((uint8_t*)&nst->node->laddr)[0], ((uint8_t*)&nst->node->laddr)[1],
                     ((uint8_t*)&nst->node->laddr)[2], ((uint8_t*)&nst->node->laddr)[3],
                     nst->node->lport);
            labelf_P(file, 40, 4+y, PSTR("%u.%u.%u.%u:%"PRIu16),
                     ((uint8_t*)&nst->node->lport)[0], ((uint8_t*)&nst->node->lport)[1],
                     ((uint8_t*)&nst->node->lport)[2], ((uint8_t*)&nst->node->lport)[3],
                     nst->node->rport);


            ITERATE_NEXT(nst);
            y++;
        }


        putlabel(file, 1, 25, WHITE | BLUE << 4, label_netstat_bottom);

//        fileflush(file);
        //   if (_kbhit()) {
        int key = _getch(file);
        switch(key)
        {
            case KB_F1:
                #if WITH_DHCP == YES
                {
                    char data[4];
                    memset(data, 0, 4);
                    int16_t ret = draw_input_dialog(file, 10, 10, netdev_pan_netid, data, 4);
                    if (ret < 1)
                    {
                        runerrord0(file, 3,3, error_capt, length_assert);
                        break;
                    }

                    t_netdev_id ndid = (uint8_t) atoi ( (const char *) data );

                    struct knetdevice * knd = hal_get_knetdev(ndid);
                    if (knd == NULL)
                    {
                        runerrord0(file, 3,3, error_capt, netdev_pan_notfound);
                        break;
                    }

                    drawframe(file, 10, 5, 50, 5, WHITE | BLUE <<4);
                    gotoxy(file, 11, 9);
                   // fprintf_P(file, PSTR("Press F5 to close"));
                    gotoxy(file, 11, 7);
                  //  fprintf_P(file, PSTR("DHCP STATUS:"));

                    if (dhclient(knd) != 0)
                    {
                        runerrord0(file, 3,3, error_capt, dhclient_failed);
                        break;
                    }


                    enum dhcp_status_code * e_code = dhcp_get_status(knd);
                    data[0] = 0;
                    while (data[0] == 0)
                    {
                        if ( (_kbhit(file)) && (_getch(file) == KB_F5))
                        {
                            break;
                        }
                        switch (*e_code)
                        {
                            case DHCP_INIT:
                                gotoxy(file, 25, 7);
                                fprintf_P(file, PSTR("INIT"));
                            break;

                            case DHCP_ASSIGNED:
                                gotoxy(file, 25, 7);
                                fprintf_P(file, PSTR("ASSIGNED"));
                                data[0] = 1;
                            break;

                            case DHCP_WAITING_OFFER:
                                gotoxy(file, 25, 7);
                                fprintf_P(file, PSTR("WAITING_OFFER"));
                            break;

                            case DHCP_WAITING_ACK:
                                gotoxy(file, 25, 7);
                                fprintf_P(file, PSTR("WATING_ACK"));
                            break;
                        }

                    }
                }
                #else
                    runerrord0(file, 3,3, error_capt, dhclient_not_sup);
                #endif // WITH_DHCP
            break;

            case KB_F2:
            {
                char data[16];
                char ip[4];
                memset(data, 0, 16);
                int16_t ret = draw_input_dialog(file, 10, 10, netdev_pan_netid, data, 4);
                if (ret < 1)
                {
                    runerrord0(file, 3,3, error_capt, length_assert);
                    break;
                }

                t_netdev_id ndid = (uint8_t) atoi ( (const char *) data );

                struct knetdevice * knd = hal_get_knetdev(ndid);
                if (knd == NULL)
                {
                    runerrord0(file, 3,3, error_capt, netdev_pan_notfound);
                    break;
                }

                memset(data, 0, 16);
                ret = draw_input_dialog(file, 10, 10, inp_ip_addr, data, 16);
                if (ret < 1)
                {
                    runerrord0(file, 3,3, error_capt, length_assert);
                    break;
                }

                __convert_ip(data, ip);
                knd->nic_set.ipv4 = *(uint32_t*)ip;

                memset(data, 0, 16);
                ret = draw_input_dialog(file, 10, 10, inp_mask_addr, data, 16);
                if (ret < 1)
                {
                    runerrord0(file, 3,3, error_capt, length_assert);
                    break;
                }

                __convert_ip(data, ip);
                knd->nic_set.ipv4_mask = *(uint32_t*)ip;

                memset(data, 0, 16);
                ret = draw_input_dialog(file, 10, 10, inp_gateway_addr, data, 16);
                if (ret < 1)
                {
                    runerrord0(file, 3,3, error_capt, length_assert);
                    break;
                }

                __convert_ip(data, ip);
                knd->nic_set.ipv4_gateway = *(uint32_t*)ip;

            }
            break;

            case KB_F3:
            {
                char data[4];
                memset(data, 0, 4);
                int16_t ret = draw_input_dialog(file, 10, 10, netdev_pan_netid, data, 4);
                if (ret < 1)
                {
                    runerrord0(file, 3,3, error_capt, length_assert);
                    break;
                }

                t_netdev_id ndid = (uint8_t) atoi ( (const char *) data );

                struct knetdevice * knd = hal_get_knetdev(ndid);
                if (knd == NULL)
                {
                    runerrord0(file, 3,3, error_capt, netdev_pan_notfound);
                    break;
                }

                textbackground(file, BLACK);
                clrscr(file);
                cursoroff(file);

                putlabel(file, 1, 2, GREEN | BLACK << 4, label_netstat_arp);
                putlabel(file, 1, 3, YELLOW| BLACK << 4, label_netstat_sta_id);
                putlabel(file, 8, 3, YELLOW| BLACK << 4, label_netstat_if_ip);
                putlabel(file, 30, 3, YELLOW| BLACK << 4, label_netstat_if_mac);
                putlabel(file, 70, 3, YELLOW| BLACK << 4, label_tb1_flags1);

                for (uint8_t i = 0; i < ARP_CACHE_SIZE; i++)
                {
                    labelf_P(file, 1, 4+i, PSTR("%"PRIu8), i);
                    labelf_P(file, 8, 4+i, PSTR("%u.%u.%u.%u"),
                             ((uint8_t*)&knd->arp[i].ip_addr)[0], ((uint8_t*)&knd->arp[i].ip_addr)[1],
                             ((uint8_t*)&knd->arp[i].ip_addr)[2], ((uint8_t*)&knd->arp[i].ip_addr)[3]);
                    labelf_P(file, 30, 4+i, PSTR("%x:%x:%x:%x:%x:%x"),
                             knd->arp[i].mac_addr[0], knd->arp[i].mac_addr[1], knd->arp[i].mac_addr[2],
                             knd->arp[i].mac_addr[3], knd->arp[i].mac_addr[4], knd->arp[i].mac_addr[5]);
                    labelf_P(file, 70, 4+i, PSTR("%x"), knd->arp[i].flags);
                }

                putlabel(file, 1, 25, WHITE | BLUE << 4, label_netstat_arp_bottom);

                data[0] = 0;
                while (data[0] == 0)
                {
                    if ( (_kbhit(file)) && (_getch(file) == KB_F5))
                    {
                        break;
                    }
                }
            }
            break;

            case KB_F5:
                return;
            break;
        }
    }
    return;
}

