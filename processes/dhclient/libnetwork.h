#ifndef LIBNETWORK_H_INCLUDED
#define LIBNETWORK_H_INCLUDED

#if (WITH_DHCP == YES)


enum dhcp_status_code {
	DHCP_INIT,
	DHCP_ASSIGNED,
	DHCP_WAITING_OFFER,
	DHCP_WAITING_ACK
};

struct dhcp_record
{
	enum dhcp_status_code dhcp_status;
    t_net_addr dhcp_server;
    uint32_t dhcp_renew_time;
    uint32_t dhcp_transaction_id;
    struct knetdevice * kndev;
};

/*#define DHCP_SERVER_PORT		htons(67)
#define DHCP_CLIENT_PORT		htons(68)*/

#define DHCP_SLOTS 2

#define DHCP_SERVER_PORT		67
#define DHCP_CLIENT_PORT		68

#define DHCP_OP_REQUEST			1
#define DHCP_OP_REPLY			2

#define DHCP_HW_ADDR_TYPE_ETH	1

#define DHCP_FLAG_BROADCAST		htons(0x8000)

#define DHCP_MAGIC_COOKIE		htonl(0x63825363)

typedef struct dhcp_message {
	uint8_t operation;
	uint8_t hw_addr_type;
	uint8_t hw_addr_len;
	uint8_t unused1;
	uint32_t transaction_id;
	uint16_t second_count;
	uint16_t flags;
	uint32_t client_addr;
	uint32_t offered_addr;
	uint32_t server_addr;
	uint32_t unused2;
	uint8_t hw_addr[16];
	uint8_t unused3[192];
	uint32_t magic_cookie;
	uint8_t options[];
} dhcp_message_t;

#define DHCP_CODE_PAD			0
#define DHCP_CODE_END			255
#define DHCP_CODE_SUBNETMASK	1
#define DHCP_CODE_GATEWAY		3
#define DHCP_CODE_REQUESTEDADDR	50
#define DHCP_CODE_LEASETIME		51
#define DHCP_CODE_MESSAGETYPE	53
#define DHCP_CODE_DHCPSERVER	54
#define DHCP_CODE_RENEWTIME		58
#define DHCP_CODE_REBINDTIME	59

typedef struct dhcp_option {
	uint8_t code;
	uint8_t len;
	uint8_t data[];
} dhcp_option_t;

#define DHCP_MESSAGE_DISCOVER	1
#define DHCP_MESSAGE_OFFER		2
#define DHCP_MESSAGE_REQUEST	3
#define DHCP_MESSAGE_DECLINE	4
#define DHCP_MESSAGE_ACK		5
#define DHCP_MESSAGE_NAK		6
#define DHCP_MESSAGE_RELEASE	7
#define DHCP_MESSAGE_INFORM		8

int8_t dhclient(struct knetdevice * kndev);
int8_t dhcp_wait_for_assignment(struct knetdevice * kndev);
enum dhcp_status_code * dhcp_get_status(struct knetdevice * kndev);
t_dev_id dhcp_get_did(uint8_t slot);
#endif
#endif // LIBNETWORK_H_INCLUDED
