#ifndef TABITA_H_INCLUDED
#define TABITA_H_INCLUDED

//SINGLETON
#include "tabita_protocol.h"
int8_t tabita(void);

uint8_t * tabita_prepare_header(uint8_t * buf, enum tabita_pkt_oper e_oper);
uint8_t * tabita_cound_crc16(uint16_t payload_size, uint8_t * buf);

#endif // TABITA_H_INCLUDED
