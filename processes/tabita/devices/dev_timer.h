#ifndef DEV_TIMER_H_INCLUDED
#define DEV_TIMER_H_INCLUDED

#define FOREACH_TIMER_CMDS(DV)\
    DV(TIMER_CMD_SET_OCR_REGISTER, (uint8_t) 1)\
    DV(TIMER_CMD_OPERATION_NOT_SUP, (uint8_t) 0xFF)

enum timer_cmds
{
    FOREACH_TIMER_CMDS(GENERATE_ENUM_VAL)
};



struct proto_timer_set_ocr
{
    uint8_t e_reg_id; //enum timer_ocr_registers
    uint16_t ocr_val;
};

struct proto_timer_set_ocr_ans
{
    uint8_t e_reg_id; //enum timer_ocr_registers
    uint16_t ocr_val;
};

#endif // DEV_TIMER_H_INCLUDED
