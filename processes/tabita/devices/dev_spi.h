#ifndef DEV_SPI_H_INCLUDED
#define DEV_SPI_H_INCLUDED

//protocol
#define FOREACH_SPI_CMD(DV)\
    DV(SPI_CMD_OPER_NOT_SUP,  (uint8_t) 0xff)

enum spi_cmd
{
	FOREACH_SPI_CMD(GENERATE_ENUM_VAL)
};



#endif // DEV_SPI_H_INCLUDED
