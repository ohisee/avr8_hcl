#ifndef DEV_ADC_H_INCLUDED
#define DEV_ADC_H_INCLUDED

#define FOREACH_ADC_CMD(DV)\
    DV(ADC_CMD_LIST_CHANNEL, (uint8_t) 0)\
	DV(ADC_CMD_ENABLE_CHANNEL, (uint8_t) 1)\
	DV(ADC_CMD_DISABLE_CHANNEL, (uint8_t) 2)\
	DV(ADC_CMD_RUN_READ_CHANNEL, (uint8_t) 3)\
	DV(ADC_CMD_RUN_READ_ALL, (uint8_t) 4)\
	DV(ADC_CMD_READ_RESULT, (uint8_t) 5)\
	DV(ADC_CMD_READ_REASULT_ALL, (uint8_t) 6)\
	DV(ADC_CMD_OPER_NOT_SUP, (uint8_t) 0xFF)


enum adc_cmds
{
	FOREACH_ADC_CMD(GENERATE_ENUM_VAL)
};

struct proto_adc_list_chanls_ans
{
    int8_t chan_id;
    uint8_t flags;
    uint8_t data[];
};

struct proto_adc_chan_enable
{
    uint8_t e_chan_sel; //enum adc_channel_selector
    uint8_t e_p_ref; //enum p_adc_ref
    uint8_t e_p_presc; //enum p_adc_presc
    uint8_t flags;
};

struct proto_adc_chan_enable_ans
{
    uint8_t e_chan_sel; //enum adc_channel_selector
    int8_t chanid;
};

struct proto_adc_chan_disable
{
    int8_t chanid;
};

struct proto_adc_readchan
{
    int8_t chanid;
};

struct proto_adc_readres
{
    uint8_t chanid;
};

struct proto_adc_readres_ans
{
    int8_t chanid;
    uint16_t read;

    uint8_t next[];
};

#endif // DEV_ADC_H_INCLUDED
