
#include "../../hal/build_defs.h"

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "limits.h"

#include "../../hal/hal.h"

#include "../../os/kernel/kernel_process.h"
#include "../../os/kernel/kernel_memory.h"
#include "../../os/kernel/kernel_mutex.h"

#include "tabita_protocol.h"
#include "tabita.h"

#include "../../hal/drivers/serial/usart.h"
#include "../../hal/drivers/timer/timer.h"
#include "../../hal/drivers/io/io.h"
#include "../../hal/drivers/adc/adc.h"
#include "../../hal/drivers/spi/spi.h"
#include "../../hal/drivers/ds18x20/local/ds18x20.h"
#include "../../hal/drivers/ds18x20/ds18.h"
#include "../../hal/drivers/annunciator_panel/a_panel.h"
#include "../../hal/drivers/enc28j60/enc28.h"
#include "../../hal/drivers/input_scanner/inpscan.h"

#include "devices/dev_usart.h"
#include "devices/dev_timer.h"
#include "devices/dev_io.h"
#include "devices/dev_adc.h"
#include "devices/dev_spi.h"
#include "devices/dev_ds18x20.h"
#include "devices/dev_anonpan.h"
#include "devices/dev_enc28.h"
#include "devices/dev_inpscan.h"

#include "../../hal/lib/nongnu/avr-gcc/crc16.h"

static struct knetlisten * sock = NULL;
static t_trans_id slave_tid = 0;
//volatile t_trans_id seq_num = 0;

int8_t usart_handling(struct dev_descr_node * dds, struct tabita_header * ans_head, struct tabita_request * tr)
{
    struct tabita_request_ans * ans = (struct tabita_request_ans*) ans_head->data;
    ans_head->payload_len = sizeof(struct tabita_header) + sizeof(struct tabita_request_ans);

    struct dim_rec rdim;
    uint8_t offset = 0;

    switch (tr->opcode)
    {
        case USART_CMD_SENDBYTE:
        {
            struct proto_usart_send * ud = (struct proto_usart_send*) tr->data;

            if (usart_sync_send_byte(dds->instance, ud->data) == -1)
            {
                //fail
                ans->error_no = getSysErrror();
            }
            else
            {
                //ok
                ans->error_no = 0;
            }

            ans->opcode = USART_CMD_SENDBYTE;
        }
        break;

        default:
            ans->opcode = USART_CMD_OPERATION_NOT_SUP;
            setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
            ans->error_no = SYSTEM_OPERATION_NOT_SUPPORTED;
        break;
    }

    return 0;
}

int8_t inpscan_handling(struct dev_descr_node * dds, struct tabita_header * ans_head, struct tabita_request * tr)
{
    struct tabita_request_ans * ans = (struct tabita_request_ans*) ans_head->data;
    ans_head->payload_len = sizeof(struct tabita_header) + sizeof(struct tabita_request_ans);

    struct dim_rec rdim;
    uint8_t offset = 0;

    switch (tr->opcode)
    {
        case INPSCAN_CMD_LIST_SCANS:
        {
            ans->opcode = INPSCAN_CMD_LIST_SCANS;
            ans->error_no = 0;
            struct proto_inpscan_list_scans * scanl = ans->data;
            while (inpscan_get_mem(dds, offset, &rdim) == 0)
            {
                scanl->io_did = rdim.ctrld->ddn->dev_id;
                io_get_mask(rdim.ctrld, &scanl->scanmask);
                scanl->readmask = rdim.scanres;
                scanl = scanl->next;
                offset++;
            }
            scanl->io_did -1;
            offset++;

            ans_head->payload_len += (sizeof(struct proto_inpscan_list_scans) * offset);
        }
        break;

        default:
            ans->opcode = ADC_CMD_OPER_NOT_SUP;
            setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
            ans->error_no = SYSTEM_OPERATION_NOT_SUPPORTED;
        break;
    }

    return 0;
}

int8_t io_handling(struct dev_descr_node * dds, struct tabita_header * ans_head, struct tabita_request * tr)
{
    struct tabita_request_ans * ans = (struct tabita_request_ans*) ans_head->data;
    ans_head->payload_len = sizeof(struct tabita_header) + sizeof(struct tabita_request_ans);

    switch (tr->opcode)
    {
        case IO_CMD_WRITE8:
        {
            ans->opcode = IO_CMD_WRITE8;
            struct proto_io_rw_ra * iow = (struct proto_io_rw_ra *) tr->data;
            struct proto_io_rw_ra * iowans = (struct proto_io_rw_ra *) ans->data;
            ans_head->payload_len += sizeof(struct proto_io_rw_ra);
            iowans->val = iow->val;
            if (io_write8(dds->instance, iow->val) != 0)
            {
                ans->error_no = getSysErrror();
            }
            else
            {
                ans->error_no = 0;
            }
        }
        break;

        case IO_CMD_READ8:
        {
            ans->opcode = IO_CMD_READ8;
            struct proto_io_rw_ra * iow = (struct proto_io_rw_ra *) ans->data;
            ans_head->payload_len += sizeof(struct proto_io_rw_ra);
            if (io_read8_direct(dds->instance, &iow->val) != 0)
            {
                ans->error_no = getSysErrror();
            }
            else
            {
                ans->error_no = 0;
            }
        }
        break;

        default:
            ans->opcode = IO_CMD_OPER_NOT_SUP;
            setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
            ans->error_no = SYSTEM_OPERATION_NOT_SUPPORTED;
        break;
    }

    return 0;
}

int8_t pwm_handling(struct dev_descr_node * dds, struct tabita_header * ans_head, struct tabita_request * tr)
{
    struct tabita_request_ans * ans = (struct tabita_request_ans*) ans_head->data;
    ans_head->payload_len = sizeof(struct tabita_header) + sizeof(struct tabita_request_ans);

    switch (tr->opcode)
    {
        case TIMER_CMD_SET_OCR_REGISTER:
        {
            ans->opcode = TIMER_CMD_SET_OCR_REGISTER;
            struct proto_timer_set_ocr * pso = (struct proto_timer_set_ocr *) tr->data;
            memmove(ans->data, pso, sizeof(struct proto_timer_set_ocr));
            ans_head->payload_len += sizeof(struct proto_timer_set_ocr);

            if (timer_update_single_ocr(dds, pso->e_reg_id, pso->ocr_val) != 0)
            {
                ans->error_no = getSysErrror();
            }
            else
            {
                ans->error_no = 0;
            }
        }
        break;

        default:
            ans->opcode = TIMER_CMD_OPERATION_NOT_SUP;
            setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
            ans->error_no = SYSTEM_OPERATION_NOT_SUPPORTED;
        break;
    }

    return 0;
}

ENUM_MATCH_CONVERT_FUNCTION_DEC(adc_refs, enum adc_ref, enum p_adc_ref);
ENUM_MATCH_CONVERT_FUNCTION_DEC(adc_prescs, enum adc_presc, enum p_adc_presc);
ENUM_MATCH_CONVERT_FUNCTION(adc_refs, enum adc_ref, enum p_adc_ref, FOREACH_P_ADC_REF);
ENUM_MATCH_CONVERT_FUNCTION(adc_prescs, enum adc_presc, enum p_adc_presc, FOREACH_P_ADC_PRESC);
int8_t adc_handling(struct dev_descr_node * dds, struct tabita_header * ans_head, struct tabita_request * tr)
{
    clearSysError();
    struct tabita_request_ans * ans = (struct tabita_request_ans*) ans_head->data;
    ans_head->payload_len = sizeof(struct tabita_header) + sizeof(struct tabita_request_ans);
   // printf_P(PSTR("ADC rcv: %d\r\n"), tr->opcode);
    switch (tr->opcode)
    {
        case ADC_CMD_LIST_CHANNEL:
        {
            struct proto_adc_list_chanls_ans * listing = (struct proto_adc_list_chanls_ans*) ans->data;
            struct adc_channel adcc;
            uint8_t offset = 0;
            ans->opcode = ADC_CMD_LIST_CHANNEL;

            while (get_adc_mem(&adcc, offset) != 1)
            {
                listing->chan_id = adcc.aid;
                listing->flags = adcc.flags;
                listing = listing->data;
                offset++;
            }
            listing->chan_id = -1;
            listing->flags = 0;
           // printf_P(PSTR("ADC rcv ans: %d\r\n"), ans->opcode);
            //determine size
            ans_head->payload_len += (sizeof(struct proto_adc_list_chanls_ans) * (offset+1));
        }
        break;

        case ADC_CMD_ENABLE_CHANNEL:
        {
            struct proto_adc_chan_enable * pach = (struct proto_adc_chan_enable*) tr->data;
            struct proto_adc_chan_enable_ans * pach_ans = (struct proto_adc_chan_enable_ans*) ans->data;
           // printf_P(PSTR("ADC rcv ADC_CMD_ENABLE_CHANNEL: %d\r\n"), tr->opcode);
            ans->opcode = ADC_CMD_ENABLE_CHANNEL;
            //printf_P(PSTR("rcv: chansel: %u ref: %u presc: %u flags: %u\r\n"),pach->e_chan_sel, pach->e_p_ref, pach->e_p_presc, pach->flags);
            enum adc_ref e_ref = CALL_MATCH_CONVERT(adc_refs)(pach->e_p_ref);
            enum adc_presc e_presc = CALL_MATCH_CONVERT(adc_prescs)(pach->e_p_presc);
            if ( (e_ref != -1) && (e_presc != -1) )
            {
                pach_ans->chanid = adc_enable_channel(dds, pach->e_chan_sel, e_ref, e_presc, pach->flags, NULL);
                pach_ans->e_chan_sel = pach->e_chan_sel;
                if (pach_ans->chanid == -1)
                {
                //printf_P(PSTR("FAILED2 ref %u presc %u sel %u\r\n"), e_ref, e_presc, pach->e_chan_sel);
                    ans->error_no = getSysErrror();
                }
                else
                {
                //printf_P(PSTR("ok ref %u presc %u sel %u\r\n"), e_ref, e_presc, pach->e_chan_sel);
                    ans->error_no = 0;
                }
            }
            else
            {
            //printf_P(PSTR("FAILED ref %u presc %u sel %u\r\n"), e_ref, e_presc, pach->e_chan_sel);
                pach_ans->chanid = -1;
                pach_ans->e_chan_sel = pach->e_chan_sel;
                ans->error_no = getSysErrror();
            }



            ans_head->payload_len += sizeof(struct proto_adc_chan_enable_ans);
        }
        break;

        case ADC_CMD_DISABLE_CHANNEL:
        {
            struct proto_adc_chan_disable * pacd = (struct proto_adc_chan_disable *) tr->data;
            ans->opcode = ADC_CMD_DISABLE_CHANNEL;

            if (adc_disable_channel(pacd->chanid) != 0)
            {
                ans->error_no = getSysErrror();
            }
            else
            {
                ans->error_no = 0;
            }
        }
        break;

        case ADC_CMD_RUN_READ_CHANNEL:
        {
            struct proto_adc_readchan * pacr = (struct proto_adc_readchan *) tr->data;
            struct proto_adc_readres_ans * para = (struct proto_adc_readres_ans*) ans->data;
            ans->opcode = ADC_CMD_RUN_READ_CHANNEL;
            ans_head->payload_len += sizeof(struct proto_adc_readres_ans);

            para->chanid = pacr->chanid;

            if (adc_run_reading(pacr->chanid, &para->read) != 0)
            {
                ans->error_no = getSysErrror();
            }
            else
            {
                ans->error_no = 0;
            }
        }
        break;

        case ADC_CMD_RUN_READ_ALL:
        {
            struct proto_adc_readres_ans * para = (struct proto_adc_readres_ans*) ans->data;
            ans->opcode = ADC_CMD_RUN_READ_ALL;

            if (adc_run_reading_SYNC() != 0)
            {
                ans->error_no = getSysErrror();
            }
            else
            {
                ans->error_no = 0;
            }

            uint8_t i = 0;
            while (adc_get_reading_offset(&para[i].read, &para[i].chanid, i) == 0)
            {
                i++;
            }
            para[i].read = 0;
            para[i].chanid = -1;
            i++;

            ans_head->payload_len += (sizeof(struct proto_adc_readres_ans)*i);
        }
        break;

        case ADC_CMD_READ_RESULT:
        {
            struct proto_adc_readres * pard = (struct proto_adc_readres*) tr->data;
            struct proto_adc_readres_ans * para = (struct proto_adc_readres_ans*) ans->data;
            ans->opcode = ADC_CMD_READ_RESULT;

            ans_head->payload_len += sizeof(struct proto_adc_readres_ans);

            para->chanid = pard->chanid;

            if (adc_get_reading(pard->chanid, &para->read) == 0)
            {
                ans->error_no = 0;
            }
            else
            {
                ans->error_no = getSysErrror();
            }

        }
        break;

        case ADC_CMD_READ_REASULT_ALL:
        {
            struct proto_adc_readres * pard = (struct proto_adc_readres*) tr->data;
            struct proto_adc_readres_ans * para = (struct proto_adc_readres_ans*) ans->data;
            ans->opcode = ADC_CMD_READ_REASULT_ALL;

            uint8_t i = 0;
            while (adc_get_reading_offset(&para[i].read, &para[i].chanid, i) == 0)
            {
                i++;
            }
            para[i].read = 0;
            para[i].chanid = -1;
            i++;

            ans_head->payload_len += (sizeof(struct proto_adc_readres_ans)*i);
        }
        break;

        default:
            ans->opcode = ADC_CMD_OPER_NOT_SUP;
            setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
            ans->error_no = SYSTEM_OPERATION_NOT_SUPPORTED;
        break;
    }

    return 0;
}

int8_t spi_handling(struct dev_descr_node * dds, struct tabita_header * ans_head, struct tabita_request * tr)
{
    struct tabita_request_ans * ans = (struct tabita_request_ans*) ans_head->data;
    ans_head->payload_len = sizeof(struct tabita_header) + sizeof(struct tabita_request_ans);

    switch (tr->opcode)
    {
        default:
            ans->opcode = SPI_CMD_OPER_NOT_SUP;
            setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
            ans->error_no = SYSTEM_OPERATION_NOT_SUPPORTED;
        break;
    }

    return 0;
}

int8_t ds18x20_handling(struct dev_descr_node * dds, struct tabita_header * ans_head, struct tabita_request * tr)
{
    struct tabita_request_ans * ans = (struct tabita_request_ans*) ans_head->data;
    ans_head->payload_len = sizeof(struct tabita_header) + sizeof(struct tabita_request_ans);

    switch (tr->opcode)
    {
        case DS18_CMD_SEARCH_ROM:
        {
            ans->opcode = DS18_CMD_SEARCH_ROM;
            if (ds18_rescan_bus(dds->instance) == -1)
            {
                ans->error_no = getSysErrror();
            }
            else
            {
                ans->error_no = 0;
            }
        }
        break;

        case DS18_CMD_LIST_BUS_ROMS:
        {
            struct ds18_proto_bus_roms * dsbus = (struct ds18_proto_bus_roms *) ans->data;
            struct ds18_bus * bus = (struct ds18_bus*) dds->instance;
            uint8_t i = 0;

            ans->opcode = DS18_CMD_LIST_BUS_ROMS;

            ITERATE_BLOCK_DIRECT_MAN(struct ds18_bus_dev, bus->ow_bus, __it_ow)
            {
                dsbus[i].id = __it_ow->sid;
                dsbus[i].flags = __it_ow->flags;
                memcpy(dsbus[i].romcode, __it_ow->rom_code, OW_ROMCODE_SIZE);
                i++;
                ITERATE_NEXT(__it_ow);
            }
            dsbus[i].id = -1;
            dsbus[i].flags = 0;
            memset(dsbus[i].romcode, 0, OW_ROMCODE_SIZE);
            i++;

            ans_head->payload_len += sizeof(struct ds18_proto_bus_roms) * i;
        }
        break;

        case DS18_CMD_READ_BUS_SINGLE_ROM:
        {
            struct ds18_proto_meas_single * ds18m = (struct ds18_proto_meas_single *) tr->data;
            struct ds18_proto_meas_single_ans * ds18ans = (struct ds18_proto_meas_single_ans *) ans->data;
            uint32_t decel = 0;

            ans->opcode = DS18_CMD_READ_BUS_SINGLE_ROM;

            if (ds18_read_blocking(dds->instance, ds18m->id, &decel) != DS18X20_ERROR)
            {
                ans->error_no = 0;
            }
            else
            {
                ans->error_no = getSysErrror();
            }

            ds18ans->decel = decel;
            ds18ans->seid = ds18m->id;

            ans_head->payload_len += sizeof(struct ds18_proto_meas_single_ans);
        }
        break;

        case DS18_CMD_READ_BUS_MULTIPLE:
        {
            struct ds18_proto_meas_single * ds18m = (struct ds18_proto_meas_single *) tr->data;
            struct ds18_proto_meas_single_ans * ds18ans = (struct ds18_proto_meas_single_ans *) ans->data;

            ans->opcode = DS18_CMD_READ_BUS_MULTIPLE;

            if (ds18_read_all_blocking(dds->instance) != DS18X20_ERROR)
            {
                ans->error_no = 0;
            }
            else
            {
                ans->error_no = getSysErrror();
            }

            struct ds18_bus * bus = (struct ds18_bus*) dds->instance;
            uint8_t i = 0;

            ITERATE_BLOCK_DIRECT_MAN(struct ds18_bus_dev, bus->ow_bus, __it_ow)
            {
                ds18ans[i].decel = __it_ow->result;
                ds18ans[i].seid = __it_ow->sid;

                i++;
                ITERATE_NEXT(__it_ow);
            }
            ds18ans[i].decel = 0;
            ds18ans[i].seid = -1;
            i++;

            ans_head->payload_len += sizeof(struct ds18_proto_meas_single_ans) * i;
        }
        break;

        default:
            ans->opcode = DS18_CMD_OPER_NOT_SUP;
            setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
            ans->error_no = SYSTEM_OPERATION_NOT_SUPPORTED;
        break;
    }

    return 0;
}

int8_t anonpan_handling(struct dev_descr_node * dds, struct tabita_header * ans_head, struct tabita_request * tr)
{
    struct tabita_request_ans * ans = (struct tabita_request_ans*) ans_head->data;
    struct a_pan_proto_cell * cell = (struct a_pan_proto_cell*) tr->data;
    ans_head->payload_len = sizeof(struct tabita_header) + sizeof(struct tabita_request_ans);

    switch (tr->opcode)
    {
        case A_PAN_CMD_TURNON_SINGLE:
        {
            ans->opcode = A_PAN_CMD_TURNON_SINGLE;

            if (a_panel_turn_on_single(dds, cell->id) == 0)
            {
                ans->error_no = 0;
            }
            else
            {
                ans->error_no = getSysErrror();
            }
        }
        break;

        case A_PAN_CMD_TURNOFF_SINGLE:
        {
            ans->opcode = ans->opcode = A_PAN_CMD_TURNON_SINGLE;

            if (a_panel_turn_off_single(dds, cell->id) == 0)
            {
                ans->error_no = 0;
            }
            else
            {
                ans->error_no = getSysErrror();
            }
        }
        break;

        case A_PAN_CMD_BLINK_SINGLE:
        {
            ans->opcode = ans->opcode = A_PAN_CMD_BLINK_SINGLE;

            if (a_panel_turn_blink_single(dds, cell->id) == 0)
            {
                ans->error_no = 0;
            }
            else
            {
                ans->error_no = getSysErrror();
            }
        }
        break;

        default:
            ans->opcode = A_PAN_CMD_OPER_NOT_SUP;
            setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
            ans->error_no = SYSTEM_OPERATION_NOT_SUPPORTED;
        break;
    }

    return 0;
}

int8_t enc20_handling(struct dev_descr_node * dds, struct tabita_header * ans_head, struct tabita_request * tr)
{
    struct tabita_request_ans * ans = (struct tabita_request_ans*) ans_head->data;
    ans_head->payload_len = sizeof(struct tabita_header) + sizeof(struct tabita_request_ans);

    switch (tr->opcode)
    {
        default:
            ans->opcode = ENC28_CMD_OPERATION_NOT_SUP;
            setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
            ans->error_no = SYSTEM_OPERATION_NOT_SUPPORTED;
        break;
    }

    return 0;
}

int8_t tabita_req_rcv(struct knetdevice * kndev, struct knetstate * state, struct tabita_header * head, struct tabita_request * tr)
{
    struct tabita_header * ans_head = (struct tabita_header *) hal_net_grab_tx_buffer(kndev, KNET_UDP);
    struct tabita_request_ans * reqa = (struct tabita_request_ans *) ans_head->data;

    ans_head->e_type = TAB_PKT_TYPE_SLAVE;
    ans_head->seq_num_master = head->seq_num_master;
    ans_head->seq_num_slave = -1;
    ans_head->e_oper = TAB_PKT_OPER_REQUEST_DD;

    struct dev_descr_node * node = hal_node_get(tr->did);
    if (node == NULL)
    {
        reqa->error_no = getSysErrror();
        reqa->opcode = tr->opcode;
        reqa->did = -1;
        //determine size
        ans_head->payload_len = sizeof(struct tabita_header) + sizeof(struct tabita_request_ans);

    }
    else
    {
        reqa->did = node->dev_id;

        if (node->e_dh_code == ROOT_DRIVER_DEV)
        {
            switch (node->e_dev_descr)
            {
                case DEV_DESCR_NULL:

                break;

                case DEV_DESCR_VIRTUAL:

                break;

                case DEV_DESCR_USB:

                break;

                case DEV_DESCR_IO:
                    io_handling(node, ans_head, tr);
                break;

                case DEV_DESCR_EXTINT:

                break;

                case DEV_DESCR_ADC:
                    adc_handling(node, ans_head, tr);
                break;

                case DEV_DESCR_COMPAR:

                break;

                case DEV_DESCR_TIMER:
                    pwm_handling(node, ans_head, tr);
                break;

                case DEV_DESCR_USART:
                    usart_handling(node, ans_head, tr);
                break;

                case DEV_DESCR_TWI:

                break;

                case DEV_DESCR_OWI:

                break;

                case DEV_DESCR_SPI:
                    spi_handling(node, ans_head, tr);
                break;

                default:
                    reqa->error_no = SYSTEM_TABITA_UNKNOWN_DEV;
                    reqa->opcode = tr->opcode;
                break;
            }
        }
        else
        {
            switch (node->e_dh_code)
            {
                case SOFTWARE_OWI_DS18X20:
                    ds18x20_handling(node, ans_head, tr);
                break;

                case IO_ANNUNCIATOR_PANEL:
                    anonpan_handling(node, ans_head, tr);
                break;

                case SPI_ENC28J60:
                    enc20_handling(node, ans_head, tr);
                break;

                case IO_DIG_INP_MON:
                    inpscan_handling(node, ans_head, tr);
                break;

                default:
                    reqa->error_no = SYSTEM_TABITA_UNKNOWN_DEV;
                    reqa->opcode = tr->opcode;
                break;
            }
        }

    }

    //calculate CRC16
        //todo

    hal_send_dgram(kndev,
                   state->raddr,
                   state->rport,
                   state->lport,
                   KNET_UDP,
                   kndev->tx_buf,
                   ans_head->payload_len);

    hal_net_relese_tx_buffer(kndev);
}

int8_t tabita_onrcv(struct knetdevice * kndev,
                    struct knetstate * state,
                    uint8_t *frame,
                    uint16_t len)
{
    struct tabita_header * head = (struct tabita_header *) frame;

    //check origin
    if (head->e_type == TAB_PKT_TYPE_SLAVE)
    {
        //node direct intercommunication not supported and should work on sep. port
        setSysError(SYSTEM_TABITA_INTERCOM_NOT_SUPP);
        goto leave_rx;
    }

    //perform CRC16 check
   // uint16_t crc16 = CRC16_INIT;
   /*printf_P(PSTR("\r\n e_type:%d\r\n"), head->e_oper);
   printf_P(PSTR("seq_num_master:%d\r\n"), head->seq_num_master);
   printf_P(PSTR("seq_num_slave:%d\r\n"), head->seq_num_slave);
   printf_P(PSTR("payload_len:%d\r\n"), head->payload_len);
   printf_P(PSTR("crc16:%d\r\n"), head->crc16);
   printf_P(PSTR("e_oper:%d\r\n"), head->e_oper);

   for (uint16_t i = 0; i < len; i++)
   {
    printf_P(PSTR("%x "), frame[i]);
   }*/
    /*for (uint16_t i = 0; i < head->payload_len; i++)
    {
      //  crc16 = crc16_update(crc16, head->data[i]);
      printf_P(PSTR("%x "), frame[i]);
    }*/

    /*if (crc16 != head->crc16)
    {
        setSysError(SYSTEM_TABITA_CRC16_FAILED);
        goto leave_rx;
    }*/

    switch (head->e_oper)
    {
        case TAB_PKT_OPER_CREATE_DD:
        {
            struct tabita_create * tbc = (struct tabita_create *) head->data;
            struct dev_descr_node * node = hal_dev_node_create(K_NO_DD,
                                                                tbc->e_dev_descr,
                                                                tbc->e_dh_code,
                                                                tbc->data,
                                                                len-sizeof(struct tabita_header)-sizeof(struct tabita_create));

            struct tabita_header * ans_head = (struct tabita_header *) hal_net_grab_tx_buffer(kndev, KNET_UDP);

            ans_head->e_type = TAB_PKT_TYPE_SLAVE;
            ans_head->seq_num_master = head->seq_num_master;
            ans_head->seq_num_slave = -1;
            ans_head->e_oper = TAB_PKT_OPER_CREATE_DD;

            struct tabita_create_answer * tca = (struct tabita_create_answer *) ans_head->data;

            if (node == NULL)
            {
                tca->did = K_NO_DD;
                tca->error_id = getSysErrror();
            }
            else
            {
                tca->did = node->dev_id;
                tca->error_id = 0;
            }

            //determine size
            ans_head->payload_len = sizeof(struct tabita_header) + sizeof(struct tabita_create_answer);

            //calculate CRC16
            //todo

            hal_send_dgram(kndev,
                           state->raddr,
                           state->rport,
                           state->lport,
                           KNET_UDP,
                           kndev->tx_buf,
                           ans_head->payload_len);

            hal_net_relese_tx_buffer(kndev);
        }
        break;

        case TAB_PKT_OPER_MODIFY_DD:

        break;

        case TAB_PKT_OPER_DELETE_DD:

        break;

        case TAB_PKT_OPER_REQUEST_DD:
        {
            struct tabita_request * tr = (struct tabita_request*) head->data;
            tabita_req_rcv(kndev, state, head, tr);
        }
        break;

        case TAB_PKT_OPER_LIST_DD:
        {
            struct tabita_header * ans_head = (struct tabita_header *) hal_net_grab_tx_buffer(kndev, KNET_UDP);
            struct tabita_list_dd_answer * ddans =  (struct tabita_list_dd_answer *) ans_head->data;

            ans_head->e_type = TAB_PKT_TYPE_SLAVE;
            ans_head->seq_num_master = head->seq_num_master;
            ans_head->seq_num_slave = -1;
            ans_head->e_oper = TAB_PKT_OPER_LIST_DD;

            struct dev_descr_node fdl;
            t_dev_id offset = 0;

            while ( hal_dd_mem_cpy(offset, &fdl) != K_NO_DD)
            {
                ddans->did = fdl.dev_id;
                ddans->e_dev_descr = fdl.e_dev_descr;
                ddans->e_dh_code = fdl.e_dh_code;
                ddans = ddans->next;

                offset++;
            }
            ddans->did = K_NO_DD;
            ddans->e_dev_descr = 0;
            ddans->e_dh_code = 0;


            //determine size
            ans_head->payload_len = sizeof(struct tabita_header) + (sizeof(struct tabita_list_dd_answer) * (offset+1));

            //calculate CRC16
            //todo

            hal_send_dgram(kndev,
                           state->raddr,
                           state->rport,
                           state->lport,
                           KNET_UDP,
                           kndev->tx_buf,
                           ans_head->payload_len);

            hal_net_relese_tx_buffer(kndev);

        }
        break;

        case TAB_PKT_OPER_LIST_PR:
        {
            uint16_t pids = kernel_processes_get_amount();
            struct tabita_header * ans_head = (struct tabita_header *) hal_net_grab_tx_buffer(kndev, KNET_UDP);
            struct tabita_list_pr_answer * ans_list = (struct tabita_list_pr_andwer *) ans_head->data;
            struct kprocess proc;

            ans_head->e_type = TAB_PKT_TYPE_SLAVE;
            ans_head->seq_num_master = head->seq_num_master;
            ans_head->seq_num_slave = -1;
            ans_head->e_oper = TAB_PKT_OPER_LIST_PR;

            for (uint8_t i = 0; i < pids; i++)
            {
                int8_t status = kernel_processes_get_procinfo(i, &proc);
                if (status != 0)
                {
                    continue;
                }

                ans_list[i].pid = proc.pid;
                ans_list[i].flags = proc.flags;
                ans_list[i].stack_end = proc.stack_end;
                ans_list[i].stack_ptr = proc.stack_saved;
                ans_list[i].stack_start = proc.stack_start;
                #if (KERNEL_MEASURE_PERF==YES)
                ans_list[i].cpup = proc.cpu_usage;
                #else
                ans_list[i].cpup = 0;
                #endif

            }
            ans_list[pids].pid = -1;
            ans_list[pids].flags = 0;
            ans_list[pids].stack_end = 0;
            ans_list[pids].stack_ptr = 0;
            ans_list[pids].stack_start = 0;
            #if (KERNEL_MEASURE_PERF==YES)
            ans_list[pids].cpup = 0;
            #else
            ans_list[pids]->cpup = 0;
            #endif

            //determine size
            ans_head->payload_len = sizeof(struct tabita_header) + (sizeof(struct tabita_list_pr_answer) * (pids+1));

            //calculate CRC16
            //todo

            hal_send_dgram(kndev,
                           state->raddr,
                           state->rport,
                           state->lport,
                           KNET_UDP,
                           kndev->tx_buf,
                           ans_head->payload_len);

            hal_net_relese_tx_buffer(kndev);
        }
        break;

        case TAB_PKT_OPER_QUERY_PR:

        break;

        case TAB_PKT_OPER_AUTH:

        break;
    }

leave_rx:
    //release RX buffer
    hal_net_onrcv_release_rx(kndev);
    return 0;
}

int8_t
tabita(void)
{
    if (sock == NULL)
    {
        struct knetlisten_handlers hndlrs;
        hndlrs.ONRCV = tabita_onrcv;

        sock = hal_net_listen_socket(KNET_UDP, inet_addr(0,0,0,0), TABITA_PORT, &hndlrs);
    }
    else
    {
        return 1;
    }

    return 0;
}

uint8_t *
tabita_prepare_header(uint8_t * buf, enum tabita_pkt_oper e_oper)
{
    if (buf == NULL)
    {
        return NULL;
    }

    struct tabita_header * ans_head = (struct tabita_header *) buf;

    ans_head->e_type = TAB_PKT_TYPE_SLAVE;
    ans_head->seq_num_master = -1;
    ans_head->seq_num_slave = slave_tid;
    ans_head->e_oper = e_oper;
    ans_head->crc16 = 0;
    ans_head->payload_len = sizeof(struct tabita_header);

    if (slave_tid == UCHAR_MAX)
    {
        slave_tid = 0;
    }
    else
    {
        slave_tid++;
    }

    return ans_head->data;
}

uint8_t *
tabita_cound_crc16(uint16_t payload_size, uint8_t * buf)
{
    if (buf == NULL)
    {
        return NULL;
    }

    struct tabita_header * ans_head = (struct tabita_header *) buf;

    ans_head->payload_len += payload_size;

    //count crc16

    return buf;
}
