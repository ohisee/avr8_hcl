# avr8_hcl - AVR8 Hardware Control Layer
Hardware Control Layer for AVR8

Basic useful feature list:

 * Attach/Detach required driver without firmware update
 * Controlling access to the I/O
 * Accepts commands via network


Description is available at [NiXD.org](https://www.nixd.org/en/avr8-projects/avr8-dcs-project)
