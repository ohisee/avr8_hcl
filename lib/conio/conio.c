/*
    conio.c
    Standard conio routines.
    Part of MicroVGA CONIO library / demo project
    Copyright (c) 2008-9 SECONS s.r.o., http://www.MicroVGA.com
    Modified by Alexander Morozov NiXD ORG.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../../hal/build_defs.h"
#include <avr/io.h>
#include <stdio.h>
#include <stdint.h>
#include <avr/pgmspace.h>
#include "conio.h"
#include "kbd.h"

static inline void __header(FILE * file);

extern void _wait_for_kbhit(FILE * file)
{
    while (!_kbhit(file))
    {
        kernel_process_usleep(300000, 0);
    }

    return 0;
}

extern void _putch (FILE * file, char ch)
{
	int ret = fputc(ch, file);
	/*if (ret == EOF)
	{
		clearerr(stdout);
	}*/

	return;
}

extern void _putch_P (FILE * file, const char * ch)
{
	int ret = fputs_P(ch, file);
	/*if (ret == EOF)
	{
		clearerr(stdout);
	}*/

	return;
}

extern int _getch(FILE * file)
{
	int ret = 0;
	//while (!_kbhit(file));
	_wait_for_kbhit(file);
	ret = fgetc(file);

	switch (ret)
	{
		case VTK_ESCAPE:
		    while (!_kbhit(file));
			ret = fgetc(file);
			switch (ret)
			{
				case -1:
				case VTK_ESCAPE:
					return AKB_ESCAPE;
				break;
				case BRACE:
				    while (!_kbhit(file));
					ret = fgetc(file);
					switch (ret)
					{
                        case '1':
                            while (!_kbhit(file));
                            ret = fgetc(file);
                            switch (ret)
                            {
                                case AKB_F5:
                                    return KB_F5;
                                case AKB_F6:
                                    return KB_F6;
                                case AKB_F7:
                                    return KB_F7;
                                case AKB_F8:
                                    return KB_F8;
                            }
                        break;
						case AKB_UP:
							return KB_UP;
						case AKB_DOWN:
							return KB_DOWN;
						case AKB_LEFT:
							return KB_LEFT;
						case AKB_RIGHT:
							return KB_RIGHT;
						case AKB_HOME:
							return KB_HOME;
						case AKB_END:
							return KB_END;
                        case AKB_INS:
                            while (!_kbhit(file));
                            ret = fgetc(file);
                            switch (ret)
                            {
                                case '~':
                                    return KB_INSERT;
                                case AKB_F9:
                                    return KB_F9;
                                case AKB_F10:
                                    return KB_F10;
                                case AKB_F11:
                                    return KB_F11;
                                case AKB_F12:
                                    return KB_F12;
                            }
                        case AKB_DEL:
                            return KB_DELETE;
                        case AKB_PGUP:
                            return KB_PGUP;
                        case AKB_PGDOWN:
                            return KB_PGDN;
					}
				break;

				case 'O':
				    while (!_kbhit(file));
					ret = fgetc(file);
					switch (ret)
					{
						case 'P':
							return KB_F1;
						case 'Q':
							return KB_F2;
						case 'R':
							return KB_F3;
						case 'S':
							return KB_F4;
						/*case 'T':
							return KB_F5;
						case 'U':
							return KB_F6;
						case 'V':
							return KB_F7;
						case 'W':
							return KB_F8;
						case 'X':
							return KB_F9;
						case 'Y':
							return KB_F10;
						case 'Z':
							return KB_F11;
						case '[':
							return KB_F12;*/
						default: return ret;
					}
				break;

			}
		break;

		case VTK_BACK:
		    while (!_kbhit(file));
			ret = fgetc(file);
			if (ret == VTK_BACK) return KB_BACK;
		break;

		case VTK_TAB:
			return KB_TAB;

		case VTK_RETURN:
			return KB_ENTER;

		case KB_ENTER:
			return KB_ENTER;

		default:
			return ret;
		break;
	}
	/*if (ret==0)
	{
		while (!_kbhit());
		ret=fgetc(stdin);
		ret = 0x100 | ret;
	}*/

/*	int ret = 0;
	if (!feof(stdin)) {
		ret = fgetc(stdin);
		clearerr(stdin);
	}*/
	return ret;

	/*
	while (!_kbhit());

	ch=UDR; //read uart

	if (ch==0)
	{
		while (!_kbhit());
		ch=UDR;
		ch = 0x100 | ch;
	}

	*/
}

extern int _kbhit (FILE * file)
{
	int chr = 0;
	chr = fgetc(file);
	if (chr == EOF)
	{
		return 0;
	}

	ungetc(chr, file);
	clearerr(file);

	//if (ret) clearerr(stdin);

	return 1;
}

void _cputs(FILE * file, const char *s)
{
	while (*s != 0)
	_putch(file, *s++);
}

void _cputs_P(FILE * file, const char *s)
{
    char ss = 0;
	while ((ss = pgm_read_byte(&(*s++))) != 0)
	_putch(file, ss);
}

int16_t _cgets(FILE * file, char *s, uint8_t buf_len)
{
	char len;
	int ch;

	len=0;

	while (buf_len>len)
	{
		ch=_getch(file);

		if (ch==KB_ENTER)
		break; //enter hit, end of input

		if (ch==KB_ESC) {
			return -1; //cancelled
		}
		else if (ch==KB_BACK)
		{
			if (len>0)
			{
				len--;
				//delete char and go back (if some chars left)
				_putch(file, KB_BACK);
				_putch(file, ' ');
				_putch(file, KB_BACK);

			}

			continue;
		}

		if (ch>0x80 || ch <' ') //skip functions keys
		continue;

		_putch(file, (char)0xff&ch); //print back to screen
		s[len]=(char)0xff&ch;
		len++;
	}

	return len;
}

static inline void __header(FILE * file)
{
	_putch(file, '\033');
	_putch(file, '[');
}
//---commit end---

void clrscr(FILE * file)
{
  __header(file);
  _putch(file, '2');
  _putch(file, 'J');
}

void clreol(FILE * file)
{
  __header(file);
  _putch(file, 'K');
}


void cursoron(FILE * file)
{
  __header(file);
  _putch(file, '2');
  _putch(file, '5');
  _putch(file, 'h');
}

void cursoroff(FILE * file)
{
  __header(file);
  _putch(file, '2');
  _putch(file, '5');
  _putch(file, 'l');
}

void textcolor(FILE * file, int color)
{
  __header(file);
  if (color & 0x8)
	  _putch(file, '1');
  else _putch(file, '2');
  _putch(file, 'm');

  _putch(file, '\033');
  _putch(file, '[');
  _putch(file, '3');
  _putch(file, ((color&0x7)%10)+'0');
  _putch(file, 'm');

  /*  _putch(color / 10 + 0x30);
    _putch(color % 10 + 0x30);
    _putch('m');*/
}

void textbackground(FILE * file, int color)
{
    __header(file);
    if (color & 0x8)
    {
        _putch(file, '5');
    }
    else
    {
        _putch(file, '6');
    }
    _putch(file, 'm');

    _putch(file, '\033');
    _putch(file, '[');
    _putch(file, '4');
    _putch(file, (color&0x7)+'0');
    _putch(file, 'm');

  /* _putch(color / 10 + 0x30);
   _putch(color % 10 + 0x30);
   _putch('m');*/
}

/*void setcolor(int attr)
{
	textattr(ATTR_REST);
	textcolor(attr&0xF);
	textbackground(attr>>4);
}*/

void textattr(FILE * file, enum ATTR attr)
{
	textcolor(file, attr&0xF);
    textbackground(file, attr>>4);

	return;
}

void gotoxy(FILE * file, char x, char y)
{
  if (x>MAX_X || y>MAX_Y)
    return;

  x--;
  y--;

  _putch(file, 0x1B);
  _putch(file, '[');
  _putch(file, (y/10)+'0');
  _putch(file, (y%10)+'0');
  _putch(file, ';');
  _putch(file, (x/10)+'0');
  _putch(file, (x%10)+'0');
  _putch(file, 'f');
}


