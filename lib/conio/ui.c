/*
    ui.c
    User interface functions.
    Part of MicroVGA CONIO library / demo project
    Copyright (c) 2008-9 SECONS s.r.o., http://www.MicroVGA.com
    Modified by Alexander Morozov NiXD ORG.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "../../hal/shared.h"
#include "./conio.h"
#include "./ui.h"
#include "./kbd.h"


int
runmenu0(FILE * file, char x, char y, int defaultitem, int page_size, const char * const menu_itm[])
{
  ASSERT_NULL(menu_itm, runmenu0_null_error);
  int key, i,j, itemno, pageno;
  int nitems, nitemsk, width, pages;
  const char * s;

 //runmenu_reload:
  itemno = defaultitem-1;
  pageno = 0;
  width = 20;

  width = 10;
  nitems = 0;
  //char menu[25];// {[ 0 ... 15-1 ] = 1};
  PGM_P p;

  memcpy_P(&p, &menu_itm[0], sizeof(PGM_P));
 // strcpy_P(menu, p);
  while (pgm_read_byte(p) != 0) {
	//s = p;
    for (j=0;pgm_read_byte(&(*p++));j++);
    if (j>width)
     width = j;
    nitems++;
	memcpy_P(&p, &menu_itm[nitems], sizeof(PGM_P));
	//strcpy_P(menu, p);
  }
  width+=2;

  if (itemno < 0 || itemno > nitems || page_size < itemno)
  {
    itemno = 0;
  }

  pages = (nitems / (page_size+1));
  int len_lst = (nitems > page_size) ? page_size : nitems;
  nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
  while (1)
  {

  cursoroff(file);
  textattr(file, CYAN<<4 | BLACK);
  gotoxy(file, x,y);
  _putch(file, ACS_ULCORNER);
  for (i=0;i<width+2;i++)
  {
		_putch(file, ACS_HLINE);
  }
  _putch(file, ACS_URCORNER);



   for (i = 0; i < len_lst;i++)
   {//i = page_size*pageno;i<page_size*(pageno+1)
	  // if (i > nitems) break;
	   uint8_t ips = page_size*pageno + i;
	   gotoxy(file, x,y+i+1);
	   _putch(file, ACS_VLINE);
	   _putch(file, ' ');
	   if (i == itemno)
	   {
		textattr(file, YELLOW);
	   }
	   if (i < nitemsk)
	   {
	   memcpy_P(&p, &menu_itm[ips], sizeof(PGM_P));
	   // strcpy_P(menu, p);
	   s = p;
	   for (j=0;j<width;j++)
	   {
		   if (pgm_read_byte(&(*s)))
		   {
			   _putch(file, pgm_read_byte(&(*s++)));
		   }
		   else
		   {
			   _putch(file, ' ');
		   }
	   }
	   }
	   else
	   {
		   for (j=0;j<width;j++)
		   {
			    _putch(file, ' ');
		   }
	   }

	   textattr(file, CYAN<<4 | BLACK);
	   _putch(file, ' ');
	   _putch(file, ACS_VLINE);
   }

  uint8_t is = i+1;
  gotoxy(file, x,y+is);
  _putch(file, ACS_LLCORNER);
  for (i=0;i<width+2;i++)
  {
	_putch(file, ACS_HLINE);
  }
  _putch(file, ACS_LRCORNER);
  labelf_P(file, x+2, y+is, PSTR("%i/%i -> %i"), pageno+1, pages+1, nitemsk);

//  fileflush(file);

     key = _getch(file);

     switch(key) {
      case KB_UP:
      		if (itemno>0)
      		{
      			itemno--;
      		}
      	    else
      	    {

      	    	if (pageno>0) pageno--;
      			else pageno = pages;
				nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
				itemno = nitemsk-1;//nitems-1;

      	    }
      break;
      case KB_DOWN:
      		itemno++;
      		if ( (itemno %= nitemsk) == 0)
			{
				pageno++;
				pageno %= (pages+1);
				nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
      			/*pageno++;
      			if (pageno > pages)
				{
					pageno = 1;
				}*/
			}
      break;
      case KB_LEFT:
      		if (pageno>0) pageno--;
      		else pageno = pages;

			nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
			if (nitemsk < itemno)
			{
				itemno = nitemsk-1;
			}

      break;
      case KB_RIGHT:
      		pageno++;
      		pageno %= (pages+1);
			nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
			if (nitemsk < itemno)
			{
				itemno = nitemsk-1;
			}
      break;
      case KB_ESC:
        return UI_NOTHING;
      case KB_ENTER:
        cursoron(file);
        return page_size*pageno+itemno+1;
     }

  }

runmenu0_null_error:
  return UI_NOTHING;
}

int runmenu0_s(FILE * file, char x, char y, int defaultitem, int page_size, const struct menudialog_entry st_data[], int * ret)
{
  ASSERT_NULL(st_data, runmenu0_s_null_error);
  int key, i,j, itemno, pageno;
  int nitems, nitemsk, width, pages;
  const char * s;

 //runmenu_reload:
  itemno = defaultitem-1;
  pageno = 0;
  width = 20;

  width = 10;
  nitems = 0;

  PGM_P p;
  memcpy_P(&p, &(st_data[0].caption), sizeof(PGM_P));

  while (pgm_read_byte(&(*p)) != 0)
  {

    for (j=0;pgm_read_byte(&(*p++));j++);

    if (j>width)
     width = j;
    nitems++;

	memcpy_P(&p, &(st_data[nitems].caption), sizeof(PGM_P));
  }

   fprintf_P(file, PSTR("items: %i\r\n"), nitems);
  width+=2;

  if (itemno < 0 || itemno > nitems || page_size < itemno)
  {
    itemno = 0;
  }

  pages = (nitems / (page_size+1));
  int len_lst = (nitems > page_size) ? page_size : nitems;
  nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
  while (1)
  {

  cursoroff(file);
  textattr(file, CYAN<<4 | BLACK);
  gotoxy(file, x,y);
  _putch(file, ACS_ULCORNER);
  for (i=0;i<width+2;i++)
  {
		_putch(file, ACS_HLINE);
  }
  _putch(file, ACS_URCORNER);



   for (i = 0; i < len_lst;i++)
   {//i = page_size*pageno;i<page_size*(pageno+1)
	  // if (i > nitems) break;
	   uint8_t ips = page_size*pageno + i;
	   gotoxy(file, x,y+i+1);
	   _putch(file, ACS_VLINE);
	   _putch(file, ' ');
	   if (i == itemno)
	   {
		textattr(file, YELLOW);
	   }
	   if (i < nitemsk)
	   {
	   memcpy_P(&p, &(st_data[ips].caption), sizeof(PGM_P));
	   // strcpy_P(menu, p);
	   s = p;
	   for (j=0;j<width;j++)
	   {
		   if (pgm_read_byte(&(*s)))
		   {
			   _putch(file, pgm_read_byte(&(*s++)));
		   }
		   else
		   {
			   _putch(file, ' ');
		   }
	   }
	   }
	   else
	   {
		   for (j=0;j<width;j++)
		   {
			    _putch(file, ' ');
		   }
	   }

	   textattr(file, CYAN<<4 | BLACK);
	   _putch(file, ' ');
	   _putch(file, ACS_VLINE);
   }

  uint8_t is = i+1;
  gotoxy(file, x,y+is);
  _putch(file, ACS_LLCORNER);
  for (i=0;i<width+2;i++)
  {
	_putch(file, ACS_HLINE);
  }
  _putch(file, ACS_LRCORNER);
  labelf_P(file, x+2, y+is, PSTR("%i/%i -> %i"), pageno+1, pages+1, nitemsk);

//fileflush(file);

  while (!_kbhit(file)) ;

   if (_kbhit(file)) {
     key = _getch(file);
     switch(key) {
      case KB_UP:
      		if (itemno>0)
      		{
      			itemno--;
      		}
      	    else
      	    {

      	    	if (pageno>0) pageno--;
      			else pageno = pages;
				nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
				itemno = nitemsk-1;//nitems-1;

      	    }
      break;
      case KB_DOWN:
      		itemno++;
      		if ( (itemno %= nitemsk) == 0)
			{
				pageno++;
				pageno %= (pages+1);
				nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
      			/*pageno++;
      			if (pageno > pages)
				{
					pageno = 1;
				}*/
			}
      break;
      case KB_LEFT:
      		if (pageno>0) pageno--;
      		else pageno = pages;

			nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
			if (nitemsk < itemno)
			{
				itemno = nitemsk-1;
			}

      break;
      case KB_RIGHT:
      		pageno++;
      		pageno %= (pages+1);
			nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
			if (nitemsk < itemno)
			{
				itemno = nitemsk-1;
			}
      break;
      case KB_ESC: cursoron(file); return UI_NOTHING;
      case KB_ENTER: cursoron(file);
		  //printf_P(PSTR("ret: %i id:%i %i %i %i\r\n"), pgm_read_dword(&(st_data[page_size*pageno+itemno].val)), page_size*pageno+itemno, itemno, page_size, pageno);
		*ret = pgm_read_dword(&(st_data[page_size*pageno+itemno].val));
		return page_size*pageno+itemno+1;
     }
   }
  }

runmenu0_s_null_error:
  return UI_NOTHING;
}


int runmenu1(FILE * file, char x, char y, int defaultitem, const char * caption, const char * const menu_itm[])
{
	ASSERT_NULL(caption, runmenu1_null_error);

	int key, i,j, itemno;
	int nitems, width;
	const char * s;

	//runmenu_reload:
	itemno = defaultitem-1;

	size_t len = strlen_P(caption);
	width = len;
	nitems = 0;
	//char menu[25];// {[ 0 ... 15-1 ] = 1};
	PGM_P p;

	memcpy_P(&p, &menu_itm[0], sizeof(PGM_P));
	// strcpy_P(menu, p);
	while (pgm_read_byte(p) != 0) {
		//s = p;
		for (j=0;pgm_read_byte(&(*p++));j++);
		if (j>width)
		width = j;
		nitems++;
		memcpy_P(&p, &menu_itm[nitems], sizeof(PGM_P));
		//strcpy_P(menu, p);
	}
	width += 2;

	if (itemno < 0 || itemno > nitems)
	itemno = 0;

	while (1) {

		cursoroff(file);
		textattr(file, CYAN<<4 | BLACK);
		gotoxy(file, x,y);
		_putch(file, ACS_ULCORNER);

        s = caption;
		for (i=0;i<width+2;i++)
		{
		    if (i >= len)
            {
                _putch(file, ACS_HLINE);
            }
            else
            {
                _putch(file, pgm_read_byte(&(*s++)));
            }
		}

		_putch(file, ACS_URCORNER);


		for (i = 0;i<nitems;i++)
		{
			gotoxy(file, x,y+i+1);
			_putch(file, ACS_VLINE);
			_putch(file, ' ');
			if (i == itemno)
			textattr(file, YELLOW);
			memcpy_P(&p, &menu_itm[i], sizeof(PGM_P));
			// strcpy_P(menu, p);
			s = p;
			for (j=0;j<width;j++)
			{
				if (pgm_read_byte(&(*s)))
				{
					_putch(file, pgm_read_byte(&(*s++)));
				}
				else
				{
					_putch(file, ' ');
				}
			}
			textattr(file, CYAN<<4 | BLACK);
			_putch(file, ' ');
			_putch(file, ACS_VLINE);
		}


		gotoxy(file, x,y+nitems+1);
		_putch(file, ACS_LLCORNER);
		for (i=0;i<width+2;i++)
		{
			_putch(file, ACS_HLINE);
		}
		_putch(file, ACS_LRCORNER);

//		fileflush(file);

		while (!_kbhit(file)) ;

		if (_kbhit(file))
		{
			key = _getch(file);
			switch(key)
			{
				case KB_UP:
                    if (itemno>0)
                    {
                        itemno--;
                    }
                    else
                    {
                        itemno = nitems-1;
                    }
                break;
				case KB_DOWN:
                    itemno++;
                    itemno %= nitems;
                break;
				case KB_ESC:
                    cursoron(file);
                return UI_NOTHING;
				case KB_ENTER:
                    cursoron(file);
                return itemno+1;
			}
		}
	}

	return 1;

runmenu1_null_error:
	return UI_NOTHING;
}



void runerrord0(FILE * file, char x, char y, const char * const text, const char * const msg)
{
    ASSERT_NULL(text, runerrord0_null_error);
	ASSERT_NULL(msg, runerrord0_null_error);

    size_t t1 = strlen_P(text);
    size_t t2 = strlen_P(msg);
    if (t1 < t2)
    {
        t1 = t2;
    }
    drawframe(file, x, y, (x+t1+2), 4, WHITE  | RED <<4);
    putlabel(file, x+1, y+1, WHITE  | RED <<4, text);
    putlabel(file, x+2, y+2, WHITE | RED << 4, msg);

//    fileflush(file);

    while (!_kbhit(file));
	(void) _getch(file);

runerrord0_null_error:
    return;
}

void runerrord1(FILE * file, uint8_t x, uint8_t y, uint8_t ln, const char * const text, const char *fmt, ...)
{
	ASSERT_NULL(text, runerrord0_null_error);
	size_t t1 = strlen_P(text);

	drawframe(file, x, y, ln, 3, WHITE  | RED <<4);
	putlabel(file, x+1, y+1, WHITE  | RED <<4, text);
	gotoxy(file, x+1, y+2);
	//putlabel(x+2, y+2, WHITE | RED << 4, msg);
	textattr(file, WHITE | RED << 4);
	va_list arg;
	va_start(arg, fmt);
	vfprintf_P(file, fmt, arg);
	va_end(arg);

//	fileflush(file);

	while (!_kbhit(file));
	(void) _getch(file);
	runerrord0_null_error:
	return;
}

void runinfod0(FILE * file, char x, char y, bool waitkeypr, const char * const text, const char * const msg)
{
	ASSERT_NULL(text, runerrord0_null_error);
    size_t t1 = strlen_P(text);
    size_t t2 = strlen_P(msg);
    if (t1 < t2)
    {
        t1 = t2;
    }
    drawframe(file, x, y, (x+t1+2), 4, WHITE  | GREEN <<4);
    putlabel(file, x+1, y+1, WHITE  | GREEN << 4, text);
    putlabel(file, x+2, y+2, WHITE | GREEN << 4, msg);

//    fileflush(file);

	if (waitkeypr)
	{
		while (!_kbhit(file));
		(void) _getch(file);
	}
runerrord0_null_error:
    return;
}

int16_t draw_input_dialog(FILE * file, char x, char y, const char * text, char * data, uint8_t max_data_len)
{
    ASSERT_NULL(text, error);
    ASSERT_NULL(data, error);

    uint8_t i = 0;

    size_t x1 = strlen_P(text);

    if (x1 < max_data_len)
    {
        x1 = max_data_len;
    }

    //draw input dialog (frame)
    drawframe(file, x, y, x+(uint8_t)x1, 5, WHITE | RED << 4);
    labelf_P(file, x+2, y+1, PSTR("%S"), text);

    gotoxy(file, x+2,y+3);
    textattr(file, WHITE | BLACK <<4);

    for (i=0;i<max_data_len;i++)
    {
        _putch(file, ' ');
    }

    //read data
    gotoxy(file, x+2,y+3);

    cursoron(file);

//    fileflush(file);
    int16_t res = _cgets(file, data, max_data_len);

    cursoroff(file);
//    fileflush(file);
    return res;

error:
    return -1;
}

void drawfkeys(FILE * file, const char *fkeys[])
{
 char s;
 int i, j;

 gotoxy(file, 1,25);
 for (i=0;i<10;i++) {
   textcolor(file, WHITE);
   textbackground(file, BLACK);
   if (i!= 0)
   {
    _putch(file, ' ');
   }

   if (i== 9)
    {
    _putch(file, '1');
    _putch(file, '0');
   } else
   {
   _putch(file, (i%10)+'1');
   }

   textcolor(file, BLACK);
   textbackground(file, CYAN);

   PGM_P p;
  memcpy_P(&p, &fkeys[i], sizeof(PGM_P));
 // s = fkeys[i] ? fkeys[i] : 0;
  for (j=0;j<6;j++)
  {
    s = (p != NULL) ? pgm_read_byte(&(*p++)) : NULL;
   if (s)
   {
        _putch(file, s);
   }
   else
   {
       _putch(file, ' ');
   }
  }
 }
}


//from ram, not PROGMEM
void labelf(FILE * file, uint8_t x, uint8_t y, const char *fmt, ...)
{
	gotoxy(file, x,y);
	va_list arg;
	va_start(arg, fmt);
	vfprintf(file, fmt, arg);
	va_end(arg);
}

void labelf_P(FILE * file, uint8_t x, uint8_t y, const char *fmt, ...)
{
	gotoxy(file, x,y);
	va_list arg;
	va_start(arg, fmt);
	vfprintf_P(file, fmt, arg);
	va_end(arg);
}

void putlabel(FILE * file, uint8_t x, uint8_t y, int32_t color, const char * itm)
{
	textattr(file, color);
	gotoxy(file, x, y);

	char ch = 0;
	uint8_t i = 0;
	while ((ch = pgm_read_byte(&itm[i])) != 0)
	{
		_putch(file, ch);
		++i;
	}

	return;
}

void putlabelint(FILE * file, uint8_t x, uint8_t y, int32_t color, uint8_t w_len, const char * itm, int val)
{
	textattr(file, color);
	gotoxy(file, x, y);

	char ch = 0;
	uint8_t i = 0;
	while ((ch = pgm_read_byte(&itm[i])) != 0)
	{
		_putch(file, ch);
		++i;
	}

	gotoxy(file, x+w_len, y);

	fprintf_P(file, PSTR("%i"), val);
	return;
}

void drawgauge2(FILE * file, uint8_t x, uint8_t y, uint8_t x1, uint8_t x_s1, uint8_t x_s2, char xs, uint8_t x_e1, uint8_t x_e2, char xe)
{
	//ASSERT_NULL(panel, drawgauge2_error);
	if (x1 < x) goto drawgauge2_error;

	gotoxy(file, x,y);
	textbackground(file, LIGHTGRAY);

	uint8_t i = 0;
	for (;i<x1-x;i++)
	{
		_putch(file, ' ');
	}

	textcolor(file, WHITE);
	textbackground(file, LIGHTCYAN);
	gotoxy(file, x_s1,y);
	i = x_s1;
	while (x_s2 > i)
	{
		_putch(file, xs);
		++i;
	}

	textbackground(file, CYAN);
	gotoxy(file, x_e1, y);
	i = x_e1;
	while (x_e2 > i)
	{
		_putch(file, xe);
		++i;
	}

drawgauge2_error:
	return;
}

// drawframe(10, 10, 40, 9, WHITE  | RED <<4);
void drawframe(FILE * file, uint8_t x, uint8_t y, uint8_t width, uint8_t height, uint32_t color)
{
  int i,j;

  textattr(file, color);
  gotoxy(file, x,y);

  _putch(file, ACS_ULCORNER);
  for (i=0;i<width+2;i++)
  {
	_putch(file, ACS_HLINE);
  }
  _putch(file, ACS_URCORNER);

  for (i = 0;i<height;i++)
  {
	  gotoxy(file, x,y+i+1);
	  _putch(file, ACS_VLINE);
	  _putch(file, ' ');

	   for (j=0;j<width;j++)
	   {
			_putch(file, ' ');
	   }
		_putch(file, ' ');
		_putch(file, ACS_VLINE);
  }

  gotoxy(file, x,y+height+1);
  _putch(file, ACS_LLCORNER);
  for (i=0;i<width+2;i++){
	_putch(file, ACS_HLINE);
  }
  _putch(file, ACS_LRCORNER);

  return;
}
