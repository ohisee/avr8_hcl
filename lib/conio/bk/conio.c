/*
    conio.c
    Standard conio routines.
    Part of MicroVGA CONIO library / demo project
    Copyright (c) 2008-9 SECONS s.r.o., http://www.MicroVGA.com
    Modified by Alexander Morozov NiXD ORG.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../../hal/build_defs.h"
#include <avr/io.h>
#include <stdio.h>
#include <stdint.h>
#include "conio.h"
#include "kbd.h"

static inline void __header();

extern void _putch (char ch)
{
	int ret = fputc(ch, stdout);
	/*if (ret == EOF)
	{
		clearerr(stdout);
	}*/

	return;
}

extern int _getch (void)
{
	int ret = 0;
	while (!_kbhit());
	ret = fgetc(stdin);

	switch (ret)
	{
		case VTK_ESCAPE:
			ret = fgetc(stdin);
			switch (ret)
			{
				case -1:
				case VTK_ESCAPE:
					return AKB_ESCAPE;
				break;
				case BRACE:
					ret = fgetc(stdin);
					switch (ret)
					{
						case AKB_UP:
							return KB_UP;
						case AKB_DOWN:
							return KB_DOWN;
						case AKB_LEFT:
							return KB_LEFT;
						case AKB_RIGHT:
							return KB_RIGHT;
						case AKB_HOME:
							return KB_HOME;
						case AKB_END:
							return KB_END;
					}
				break;

				case 'O':
					ret = fgetc(stdin);
					switch (ret)
					{
						case 'P':
							return KB_F1;
						case 'Q':
							return KB_F2;
						case 'R':
							return KB_F3;
						case 'S':
							return KB_F4;
						case 'T':
							return KB_F5;
						case 'U':
							return KB_F6;
						case 'V':
							return KB_F7;
						case 'W':
							return KB_F8;
						case 'X':
							return KB_F9;
						case 'Y':
							return KB_F10;
						case 'Z':
							return KB_F11;
						case '[':
							return KB_F12;
						default: return ret;
					}
				break;

			}
		break;

		case VTK_BACK:
			ret = fgetc(stdin);
			if (ret == VTK_BACK) return KB_BACK;
		break;

		case VTK_TAB:
			return KB_TAB;

		case VTK_RETURN:
			return KB_ENTER;

		case KB_ENTER:
			return KB_ENTER;

		default:
			return ret;
		break;
	}
	/*if (ret==0)
	{
		while (!_kbhit());
		ret=fgetc(stdin);
		ret = 0x100 | ret;
	}*/

/*	int ret = 0;
	if (!feof(stdin)) {
		ret = fgetc(stdin);
		clearerr(stdin);
	}*/
	return ret;

	/*
	while (!_kbhit());

	ch=UDR; //read uart

	if (ch==0)
	{
		while (!_kbhit());
		ch=UDR;
		ch = 0x100 | ch;
	}

	*/
}

extern int _kbhit (void)
{
	int chr = 0;
	chr = fgetc(stdin);
	if (chr == EOF)
	{
		return 0;
	}

	ungetc(chr, stdin);
	clearerr(stdin);

	//if (ret) clearerr(stdin);

	return 1;
}

void _cputs(const char *s)
{
	while (*s != 0)
	_putch(*s++);
}

int16_t _cgets(char *s, uint8_t buf_len)
{
	char len;
	int ch;

	len=0;

	while (buf_len>len)
	{
		ch=_getch();

		if (ch==KB_ENTER)
		break; //enter hit, end of input

		if (ch==KB_ESC) {
			return -1; //cancelled
		}
		else if (ch==KB_BACK)
		{
			if (len>0)
			{
				len--;
				//delete char and go back (if some chars left)
				_putch(KB_BACK);
				_putch(' ');
				_putch(KB_BACK);

			}

			continue;
		}

		if (ch>0x80 || ch <' ') //skip functions keys
		continue;

		_putch((char)0xff&ch); //print back to screen
		s[len]=(char)0xff&ch;
		len++;
	}

	return len;
}

static inline void __header()
{
	_putch('\033');
	_putch('[');
}
//---commit end---

void clrscr(void)
{
  __header();
  _putch('2');
  _putch('J');
}

void clreol(void)
{
  __header();
  _putch('K');
}


void cursoron(void)
{
  __header();
  _putch('?');
  _putch('2');
  _putch('5');
  _putch('h');
}

void cursoroff(void)
{
  __header();
  _putch('?');
  _putch('2');
  _putch('5');
  _putch('l');
}

void textcolor(int color)
{
  __header();
  if (color & 0x8)
	  _putch('1');
  else _putch('2');
  _putch('m');

  __header();
  _putch('3');
  _putch(((color&0x7)%10)+'0');
  _putch('m');

  /*  _putch(color / 10 + 0x30);
    _putch(color % 10 + 0x30);
    _putch('m');*/
}

void textbackground(int color)
{
  __header();
  if (color & 0x8)
	  _putch('5');
  else _putch('6');
  _putch('m');

  __header();
  _putch('4');
  _putch((color&0x7)+'0');
  _putch('m');

  /* _putch(color / 10 + 0x30);
   _putch(color % 10 + 0x30);
   _putch('m');*/
}

void setcolor(int attr)
{
	textattr(ATTR_REST);
	textcolor(attr&0xF);
	textbackground(attr>>4);
}

void textattr(enum ATTR attr)
{
	__header();
	_putch(( attr & 0x0F ) + '0');
	_putch('m');

	return;
}

void gotoxy(char x, char y)
{
  if (x>MAX_X || y>MAX_Y)
    return;

  x--;
  y--;

  __header();
  _putch((y/10)+'0');
  _putch((y%10)+'0');
  _putch(';');
  _putch((x/10)+'0');
  _putch((x%10)+'0');
  _putch('f');
}


