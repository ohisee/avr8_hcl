#ifndef KIOMXX0_1_H_INCLUDED
#define KIOMXX0_1_H_INCLUDED

#if defined(__ATmegaxx1__)
    #define _VECTOR_TABLE_SIZE _VECTORS_SIZE / 4
#else
    #define _VECTOR_TABLE_SIZE _VECTORS_SIZE / 4
#endif // defined


#endif // IOMXX0_1_H_INCLUDED
