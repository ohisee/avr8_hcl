#ifndef HANDLERS_H_INCLUDED
#define HANDLERS_H_INCLUDED

#include "haldevs.h"

struct dev_descr_node;
struct kstream;

typedef void* (*t_hal_message)(struct dev_descr_node *,
                              enum dd_event,
                              void *,
                              size_t *);

typedef void* (*t_hal_kstream)(struct dev_descr_node * ddn,
                                struct kstream * ks);

typedef void* (*t_hal_stdstream)(struct dev_descr_node * ddn,
                                FILE * file);

struct dd_handlers
{
    enum dev_descr_code e_dd_code;
    enum dev_desc_handlers e_dh_code;
    int8_t (*HAL_MESSAGE)(struct dev_descr_node *,
                          enum dd_event,
                          void *,
                          size_t *);
    int8_t (*STDSTREAM_INIT)(struct dev_descr_node * ddn,
                             FILE * file);
    int8_t (*FSTREAM_INIT)(struct dev_descr_node * ddn,
                           struct kstream * ks);
};

const struct dd_handlers dd_hndl_list[] PROGMEM;

#define DD_HNDLR_COUNT 254

#if DD_HNDLR_COUNT > 255
    #define HNDL_CNT uint16_t
#else
    #define HNDL_CNT uint8_t
#endif // DD_HNDLR_COUNT

const struct dd_handlers * handler_search(enum dev_descr_code e_dev_code, enum dev_desc_handlers e_dev_hndl);

#endif // HANDLERS_H_INCLUDED
