/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include "hal_sectioning.h"
#include "hal_private.h"
#include "error.h"
#include "drivers/serial/usart.h"

#define STATIC_BAUD_RATE 38400UL//19200UL
#define INP_BUF_SIZE 20

//fail on loading debug functions
static int uart_putchar(char c, FILE *stream);
static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL,
                                         _FDEV_SETUP_WRITE);
static int
uart_putchar(char c, FILE *stream)
{
  if (c == '\n')
    uart_putchar('\r', stream);
  while ( !(UCSR0A & (1 << UDRE0) ) );
  UDR0 = c;
  return 0;
}

void
hal_run_debug_serial()
{
    usart_init();
        //UDR0 = 'E';
    stdout = &mystdout;
}

int8_t
hal_init_statically()
{
    volatile struct usart_setup_struct uss_setup;
    volatile struct usart_hw_setup def_setup = {0,0,0,0};
	usart_hw_init_helper(&def_setup, CLOCK_POL_RF, CHAR_SIZE_8BIT, STOP_BIT1, PARITY_NONE, MODE_ASYNC);
	usart_hw_init_helper2(&def_setup, USART_ENABLE_TXE|USART_ENABLE_RXE); //<-assynchroneous mode
	usart_hw_init_helper3(&def_setup, STATIC_BAUD_RATE);
	usart_hw_init_helperf(&uss_setup, &def_setup, DEV_USART0, USART_BUFFERED_RX/*USART_NON_BUFFERED*/, USART_BUF_CIRCULAR, INP_BUF_SIZE, 0);
	usart_hw_init_helperio(&uss_setup, PORTS_A, -1, PORTS_A, -1);
    struct dev_descr_node * serial0 = hal_dev_node_create(K_NO_DD, DEV_DESCR_USART, ROOT_DRIVER_DEV, (void *) &uss_setup, sizeof(struct usart_setup_struct));
    if (serial0 == NULL)
    {
        //NO DEVICE WAS CREATED
        hal_run_debug_serial();
        printf_P(PSTR("Error serial0 n = %d\r\n"), getSysErrror());
        exit(0);
        return 1;
    }

    //open standart stream to created device
   /* FILE * serial0_file = hal_dopen(serial0, FOPEN_RW);
    if (serial0_file == NULL)
    {

        //UDR0 = 'E';
        hal_run_debug_serial();
        printf_P(PSTR("Error serial0_file n = %d\r\n"), getSysErrror());
        exit(0);
        return 1;
    }*/
    //printf_P(PSTR("after\r\n"));
    //setting stdin stdout

   /* stdout = serial0_file;
    stdin = serial0_file;*/

    return 0;
}
