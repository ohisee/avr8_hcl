/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include "hal_sectioning.h"
#include "hal_private.h"


#include "../os/kernel/kernel_memory.h"
#include "../os/kernel/kernel_process.h"
#include "../os/kernel/kernel_mutex.h"
#include "../os/kernel/kernel_cs.h"

static int8_t hal_restore_config();

volatile struct dd_list * volatile dd_list_ptr = NULL;          /*fd records*/
volatile struct hw_bind_list  * volatile hw_list_ptr = NULL;    /*hardware bind (only physical ports)*/
volatile t_dev_count dev_amount = 0;                            /*the device counter*/
uint16_t eerom_magic EEMEM;
t_dev_count eerom_dd_recs EEMEM;                                     /*the amount of a fd records*/
struct dev_descr_node_envilope eerom_dd_dev_env[127] EEMEM;     /*the fd_dev records*/
uint8_t eerom_dd_dev_sets[127][EEPROM_DEV_MEM_ALLOC] EEMEM;     /*the additional space*/
struct kproc_mutex * hal_mux = NULL;                            /*hal ctrl mutex GIANT-LOCK*/
struct kproc_mutex * hw_mux = NULL;                             /*hal hardware ctrl mutex*/
struct kproc_mutrx * mtx_giant_msg = NULL;                      /*hal message giant mutex*/

volatile struct kproc_mutex * mtx_giant_io = NULL;

/*watchdog*/
uint8_t mcusr_mirror __attribute__ ((section (".noinit")));
void get_mcusr(void) __attribute__((naked)) __attribute__((section(".init3")));
void get_mcusr(void)
{
	mcusr_mirror = MCUSR;
	MCUSR = 0;
	wdt_disable();
}

ATTRIBUTE_HAL_SECTION
uint8_t
ret_mcusr()
{
    return mcusr_mirror;
}

ATTRIBUTE_HAL_SECTION
void
hal_init()
{
    //reset error list
	sys_errors_reset();

#if KERNEL_HW_USE_RESET_PIN == YES
	//set PIN to high
	KERNEL_HW_RESET_TRIGGER_PORT |= (1 << KERNEL_HW_RESET_TRIGGER_PIN);
	KERNEL_HW_RESET_TRIGGER_DDR |= (1 << KERNEL_HW_RESET_TRIGGER_PIN);
#endif

	//check status of the MCUSR

	switch (mcusr_mirror)
	{
		case (1 << PORF):
			// Power-on Reset Flag
			MCUSR &= ~(1 << PORF);
			// add handler there
			break;

		case (1 << EXTRF):
			//  External Reset Flag (RESET)
			MCUSR &= ~(1 << EXTRF);
#if KERNEL_HW_USE_RESET_FOR_MODE_SWITCHING == YES
			//TODO
#endif
			break;

		case (1 << BORF):
			//  Brown-out Reset Flag
			MCUSR &= ~(1 << BORF);
			// add handler here
			break;

		case (1 << WDRF):
			// Watchdog Reset Flag
			MCUSR &= ~(1 << WDRF);
			// add handler here
			break;

		case (1 << JTRF):
			// JTAG Reset Flag
			MCUSR &= ~(1 << JTRF);
			//add handelr here
			break;
	}

    dd_list_ptr = NULL;
    hw_list_ptr = NULL;
    dev_amount = 0;

    return;
}

ATTRIBUTE_HAL_SECTION
void
hal_postinit()
{
    //initialize mutex
    hal_mux = kernel_mutex_init();
    hw_mux = kernel_mutex_init();
    mtx_giant_io = kernel_mutex_init();
    mtx_giant_msg = kernel_mutex_init();
}

ATTRIBUTE_HAL_SECTION
t_dev_count
hal_get_max_devs()
{
    return (1 << (sizeof(t_dev_id)  * 8 - 1));
}

ATTRIBUTE_HAL_SECTION
struct dev_descr_node *
hal_dev_node_create(t_dev_id tid,
                    enum dev_descr_code e_dev_code,
                    enum dev_desc_handlers e_dev_hndl,
                    void * data0,
                    size_t data0_len)
{
    t_dev_count d_count = hal_fd_get_total();
	t_dev_id sel_dev_id = tid;

    //check if requested device is supported
    const struct dd_handlers * hnldr = handler_search(e_dev_code, e_dev_hndl);
    ASSERT_NULL_ER(hnldr, SYSTEM_DEVICE_NOT_FOUND, NULL);

    //check if free nodes can be allocated
    if (d_count >= hal_get_max_devs())
	{
		//reached max amount of devs, abort function
		setSysError(SYSTEM_DEV_MAX_AMOUNT);
		return NULL;
    }

    //creating node
    struct dev_descr_node * node = (struct dev_descr_node *) kernel_malloc(sizeof(struct dev_descr_node));
    ASSERT_NULL_ER(node, SYSTEM_MALLOC_FAILED, NULL);

    node->instance = NULL;
	node->e_dev_descr = e_dev_code;
	node->e_dh_code = e_dev_hndl;
    node->prgm_handlers = handler_search(e_dev_code, e_dev_hndl);

    //register this node in the list, autosorting by dev_id
    NEW_LIST_ITEM(struct dd_list, item, create_error);
	item->node = node;


    kernel_mutex_lock(hal_mux); //<--- LOCKING MUTEX

    if (dd_list_ptr == NULL)
    {
        dd_list_ptr = item;
        if (sel_dev_id == K_NO_DD)
        {
            sel_dev_id = 0;
        }
    }
    else if (sel_dev_id != K_NO_DD)
    {
        ADD_TO_LIST_MANUAL(volatile struct dd_list, dd_list_ptr)
        {
            IF_LAST_ELEMENT
            {
                INJECT_ELEMENT_AFTER_PREV_IT(item);
                FINISH_SORTING;
            }
            PUT_ELEM_WITH_KNOWN_ORDER_NUM(dd_list_ptr, dev_id, item, sel_dev_id)

            NEXT_ELEMENT;
        }
    }
    else
    {
        //ADD_TO_LIST(struct fd_list, fd_list_ptr, item);
        ADD_TO_LIST_MANUAL(volatile struct dd_list, dd_list_ptr)
        {
            //if the last element in chain list was reached
            IF_LAST_ELEMENT
            {
                sel_dev_id = PREV_IT->node->dev_id + 1;
                INJECT_ELEMENT_AFTER_PREV_IT(item);
                FINISH_SORTING;
            }
            //comare the dev_id fields prev and current for distance 2
            COMPARE_DIFF(dev_id, 2)
            {
                sel_dev_id = PREV_IT->node->dev_id+1;
                INJECT_ELEMENT_BEFORE_IT(item, dd_list_ptr);
                FINISH_SORTING;
            }

            NEXT_ELEMENT;
        }

    }
    //setting for df its device fd number
    node->dev_id = sel_dev_id;

    //increment device counter
    ++dev_amount;
    kernel_mutex_unlock(hal_mux);

    //if no tid provided then the device should be attached, else it will be restored in other function
    if (tid == K_NO_DD)
    {

        if (hal_dev_node_attach(node, data0, data0_len) == K_NO_DD)
        {
            //failed to attach device to the node, abord
            //roll back
            REMOVE_FROM_LIST_2(struct dd_list, dd_list_ptr, si, si->node->dev_id == node->dev_id, nf_create_error);
            goto create_error;
        }
    }

	return node;

nf_create_error:
    setSysError(SYSTEM_ITEM_NOT_FOUND);
create_error:
    kernel_free((void*)item);
    kernel_free((void*)node);

    return NULL;

}

ATTRIBUTE_HAL_SECTION
t_dev_id
hal_dev_node_destroy(struct dev_descr_node * node)
{
    ASSERT_NULL_ER(node, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    t_dev_id res = hal_dev_node_detach(node);

    if (res == K_NO_DD)
    {
        //detach failed
        return K_NO_DD;
    }

    //remove record from list
    MUTEX_BLOCK(hal_mux, MUTEX_ACTION_MANUAL)
    {
        REMOVE_FROM_LIST_2(struct dd_list, dd_list_ptr, item, item->node->dev_id == node->dev_id, nothing_found);
        kernel_free((void *) item->node);
        kernel_free((void *) item);

        --dev_amount;

        return 0;

nothing_found:
        return K_NO_DD;
    }
}

ATTRIBUTE_HAL_SECTION
t_dev_id
hal_dev_node_attach(struct dev_descr_node * node, void * data0, size_t data0_len)
{
    //ASSERT_NULL_ER(node, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    t_hal_message hal_msg = (t_hal_message) pgm_read_word(&node->prgm_handlers->HAL_MESSAGE); //<---- FIX

    MUTEX_BLOCK(mtx_giant_msg, MUTEX_ACTION_MANUAL)
    {
        return hal_msg(node, DD_EVENT_ATTACH, data0, &data0_len);
    }

    return K_NO_DD;
}

ATTRIBUTE_HAL_SECTION
t_dev_id
hal_dev_node_update(struct dev_descr_node * node, void * data0, size_t * data0_len)
{
    ASSERT_NULL_ER(node, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    t_hal_message hal_msg = (t_hal_message) pgm_read_word(&node->prgm_handlers->HAL_MESSAGE); //<---- FIX

    MUTEX_BLOCK(mtx_giant_msg, MUTEX_ACTION_MANUAL)
    {
        return hal_msg(node, DD_EVENT_UPDATE, data0, data0_len);
    }
}

ATTRIBUTE_HAL_SECTION
t_dev_id
hal_dev_node_query(struct dev_descr_node * node, struct hal_query_info * query)
{
    ASSERT_NULL_ER(node, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    t_hal_message hal_msg = (t_hal_message) pgm_read_word(&node->prgm_handlers->HAL_MESSAGE); //<---- FIX

    size_t len = sizeof(struct hal_query_info);

    MUTEX_BLOCK(mtx_giant_msg, MUTEX_ACTION_MANUAL)
    {
        return hal_msg(node, DD_QUERY, (void*)query, &len);
    }
}

ATTRIBUTE_HAL_SECTION
t_dev_id
hal_dev_node_flush(struct dev_descr_node * node)//, struct hal_flush * hf)
{
    ASSERT_NULL_ER(node, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    t_hal_message hal_msg = (t_hal_message) pgm_read_word(&node->prgm_handlers->HAL_MESSAGE); //<---- FIX

    MUTEX_BLOCK(mtx_giant_msg, MUTEX_ACTION_MANUAL)
    {
        //size_t sz = sizeof(struct hal_flush);
        return hal_msg(node, DD_FLUSH, NULL, NULL);
    }

    return -1;
}

ATTRIBUTE_HAL_SECTION
t_dev_id
hal_dev_node_detach(struct dev_descr_node * node)
{
    //ASSERT_NULL_ER(node, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    t_hal_message hal_msg = (t_hal_message) pgm_read_word(&node->prgm_handlers->HAL_MESSAGE); //<---- FIX
    size_t sz = sizeof(struct dev_descr_node);

    MUTEX_BLOCK(mtx_giant_msg, MUTEX_ACTION_MANUAL)
    {
        return hal_msg(node, DD_EVENT_DETACH, (void *) node, &sz);
    }

    return -1;
}

ATTRIBUTE_HAL_SECTION
t_dev_id
hal_dev_node_dump(struct dev_descr_node * node, void * data_out, size_t data_size)
{
    //ASSERT_NULL_ER(node, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    t_hal_message hal_msg = (t_hal_message) pgm_read_word(&node->prgm_handlers->HAL_MESSAGE); //<---- FIX

    MUTEX_BLOCK(mtx_giant_msg, MUTEX_ACTION_MANUAL)
    {
        return hal_msg(node, DD_DUMP, data_out, &data_size);
    }

    return -1;
}

ATTRIBUTE_HAL_SECTION
t_dev_id
hal_dev_node_restore(struct dev_descr_node * node, void * data_in, size_t data_size)
{
    //ASSERT_NULL_ER(node, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    t_hal_message hal_msg = (t_hal_message) pgm_read_word(&node->prgm_handlers->HAL_MESSAGE); //<---- FIX

    MUTEX_BLOCK(mtx_giant_msg, MUTEX_ACTION_MANUAL)
    {
        return hal_msg(node, DD_RESTORE, data_in, &data_size);
    }

    return -1;
}

ATTRIBUTE_HAL_SECTION
struct dev_descr_node *
hal_node_get(t_dev_id dev_id)
{
    MUTEX_BLOCK(mtx_giant_msg, MUTEX_ACTION_MANUAL)
    {
        SEARCH_IN_LIST_2(struct dd_list, dd_list_ptr, item, item->node->dev_id == dev_id, not_found);

        return item->node;

not_found:
        setSysError(SYSTEM_ITEM_NOT_FOUND);
        return NULL;
    }
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_dd_mem_cpy(t_dev_id offs, struct dev_descr_node * fdl)
{
    ASSERT_NULL_ER(fdl, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    ASSERT_NEGATIVE_ER(offs, SYSTEM_FD_INVALID, K_NO_DD);

    MUTEX_BLOCK(hal_mux, MUTEX_ACTION_MANUAL)
    {
        volatile struct dd_list * temp_list = dd_list_ptr;
        while ( (offs > 0) && (temp_list != NULL) )
        {
            temp_list = temp_list->next;
            --offs;
        }

        if (temp_list == NULL)
        {
            return K_NO_DD;
        }
        else
        {
            memmove(fdl, temp_list->node, sizeof(struct dev_descr_node));
            return 0;
        }
    }
    return 0;
}


ATTRIBUTE_HAL_SECTION
t_dev_count
hal_fd_get_total()
{
#if KERNEL_USE_FAST_DEVCOUNT == YES
    return dev_amount;
#else
    t_dev_count cnt = 0;
    COUNT_ELEMENTS_IN_LIST(struct dd_list, dd_list_ptr, cnt)
    return cnt;
#endif // KERNEL_USE_FAST_DEVCOUNT
}

ATTRIBUTE_HAL_SECTION
uint8_t
hal_hw_bind_pin(t_dev_id did, enum ports nport, uint8_t pins)
{
    ASSERT_NEGATIVE_ER(did, SYSTEM_FD_INVALID, 1);
    ASSERT_NULL_ER(pins, SYSTEM_MASK_ZERO, 1);

    MUTEX_BLOCK(hw_mux, MUTEX_ACTION_MANUAL)
    {
        //check if port already busy
        if (hal_hw_check_available(nport, pins) == 1)
        {
            setSysError(SYSTEM_HW_PIN_BUSY);
            return 1;
        }

        struct hw_port_bind_rec * rec = NULL;

        SEARCH_IN_LIST_3(struct hw_bind_list, hw_list_ptr, it, (it->node->idev == did) && (it->node->nport == nport));
        if (it != NULL)
        {
            //insert mask bits
            it->node->pins |= pins;
        }
        else
        {
            rec = (struct hw_port_bind_rec *) kernel_malloc(sizeof(struct hw_port_bind_rec));
            ASSERT_NULL(rec, kernel_hw_bind_pin_error_0);

            rec->idev = did;
            rec->pins = pins;
            rec->nport = nport;

            NEW_LIST_ITEM(struct hw_bind_list, item, kernel_hw_bind_pin_error_1);
            item->node = rec;
            ADD_TO_LIST(struct hw_bind_list, hw_list_ptr, item);
        }

        return 0;

kernel_hw_bind_pin_error_1:
        kernel_free((void *) rec);

kernel_hw_bind_pin_error_0:
        setSysError(SYSTEM_MALLOC_FAILED);
        return 1;
    }
}

ATTRIBUTE_HAL_SECTION
uint8_t
hal_hw_unbind_pin(t_dev_id did, enum ports nport, uint8_t pins)
{
    ASSERT_NULL_ER(hw_list_ptr, SYSTEM_FD_INVALID, 1);

    //i.e DID:0 PORT:A PINS: 0b00110011
    //did 0 binds port a pins 0,1,4,5
    //request to unbind pins with mask 0b00000011
    //then don't remove full record, but clear the mask
    //request to unbind with mask 0b00110011
    //then remove record from chain

    MUTEX_BLOCK(hw_mux, MUTEX_ACTION_MANUAL)
    {
        SEARCH_IN_LIST_2(struct hw_bind_list,
                            hw_list_ptr,
                            item,
                            (item->node->idev == did) &&
                            (item->node->nport == nport) &&
                            (item->node->pins & pins),
                            kernel_hw_unbind_pin_not_found);
        if (item->node->pins == pins)
        {
            //if the same then remove record
            REMOVE_FROM_LIST_2(struct hw_bind_list,
                                hw_list_ptr,
                                ritem,
                                (ritem->node->idev == did) &&
                                (ritem->node->nport == nport) &&
                                (ritem->node->pins & pins),
                                kernel_hw_unbind_pin_not_found);
            kernel_free((void *) ritem->node);
            kernel_free((void *) ritem);
        }
        else
        {
            //clear mask, but leave record
            item->node->pins &= ~pins;
        }


        return 0;

kernel_hw_unbind_pin_not_found:
        setSysError(SYSTEM_ITEM_NOT_FOUND);
        return 1;
    }
}

ATTRIBUTE_HAL_SECTION
uint8_t
hal_hw_unbind_dd_all_pin(t_dev_id did)
{
    uint8_t i = 0;
	ASSERT_NULL_ER(hw_list_ptr, SYSTEM_LIST_EMPTY, i);

    MUTEX_BLOCK(hw_mux, MUTEX_ACTION_MANUAL)
    {
        REMOVE_FROM_LIST_ALL_MATCH(struct hw_bind_list,
                                    hw_list_ptr,
                                    item,
                                    item->node->idev == did,
                                    kernel_free((void *) item->node);kernel_free((void *) item); ++i;);

        return i;
    }
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_hw_check_available(enum ports nport, uint8_t pins)
{
	ASSERT_NULL_ER(hw_list_ptr, SYSTEM_LIST_EMPTY, -1);

    MUTEX_BLOCK(hw_mux, MUTEX_ACTION_MANUAL)
    {
        struct hw_bind_list * item_ret = hw_list_ptr;
        while (item_ret != NULL)
        {
            if ( (item_ret->node->nport == nport) && ( (item_ret->node->pins & pins) > 0))
            {
                break;
            }
            item_ret = item_ret->next;
        }
        if (item_ret == NULL)
        {
            return 0;
        }
        else
        {
            return K_HW_BUSY; //<--found
        }
    }
	return 0;
}

ATTRIBUTE_HAL_SECTION
t_dev_id
hal_hw_pin_owner(enum ports nport, uint8_t pins)
{
    ASSERT_NULL_ER(hw_list_ptr, SYSTEM_LIST_EMPTY, K_NO_DD);

    MUTEX_BLOCK(hw_mux, MUTEX_ACTION_MANUAL)
    {
        SEARCH_IN_LIST_2(struct hw_bind_list,
                        hw_list_ptr,
                        item,
                        (item->node->nport == nport) && ( (item->node->pins & pins) > 0),
                        kernel_hw_pin_owner_error_1);

        return item->node->idev;

kernel_hw_pin_owner_error_1:

        return K_NO_DD;
    }
}

ATTRIBUTE_HAL_SECTION
uint8_t
hal_hw_count_recs()
{
    uint8_t count = 0;
    MUTEX_BLOCK(hw_mux, MUTEX_ACTION_MANUAL)
    {
        COUNT_ELEMENTS_IN_LIST(struct hw_bind_list, hw_list_ptr, count);

        return count;
    }
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_hw_mem_cpy(uint8_t offs, struct hw_port_bind_rec * hwn)
{
    ASSERT_NULL_ER(hwn, SYSTEM_NULL_ARGUMENT, 1);

    MUTEX_BLOCK(hw_mux, MUTEX_ACTION_MANUAL)
    {
        struct hw_bind_list * tmp_list = hw_list_ptr;
        uint8_t hw_off = offs;

        while ( (hw_off > 0) && (tmp_list != NULL))
        {
            --hw_off;
            tmp_list = tmp_list->next;
        }

        if (tmp_list)
        {
            hwn->idev = tmp_list->node->idev;
            hwn->nport = tmp_list->node->nport;
            hwn->pins = tmp_list->node->pins;
            return 0;
        }
        else
        {
            return 1;
        }
    }
}

ATTRIBUTE_HAL_SECTION
void
hal_write_reg8(uint16_t addr, uint8_t data)
{
	if (addr > SFR_IO_FINISH)
	{
		_SFR_MEM8(addr) = data;
	}
	else
	{
		_SFR_IO8(addr) = data;
	}

	return;
}

ATTRIBUTE_HAL_SECTION
void
hal_write_reg16(uint16_t addr, uint16_t data)
{
	if (addr > SFR_IO_FINISH)
	{
		_SFR_MEM16(addr) = data;
	}
	else
	{
		_SFR_IO16(addr) = data;
	}

	return;
}

ATTRIBUTE_HAL_SECTION
uint8_t
hal_read_reg8(uint16_t addr)
{
	if (addr > SFR_IO_FINISH)
	{
		return _SFR_MEM8(addr);
	}
	else
	{
		return _SFR_IO8(addr);
	}
}

ATTRIBUTE_HAL_SECTION
uint16_t
hal_read_reg16(uint16_t addr)
{
	if (addr > SFR_IO_FINISH)
	{
		return _SFR_MEM16(addr);
	}
	else
	{
		return _SFR_IO16(addr);
	}
}

ATTRIBUTE_HAL_SECTION
uint16_t
hal_hw_get_addr_ddr(enum ports port)
{
    return pgm_read_word(&(ddr_addresses[(uint8_t) port]));
}

ATTRIBUTE_HAL_SECTION
uint16_t
hal_hw_get_addr_pin(enum ports port)
{
    return pgm_read_word(&(pin_addresses[(uint8_t) port]));
}

ATTRIBUTE_HAL_SECTION
uint16_t
hal_hw_get_addr_port(enum ports port)
{
    return pgm_read_word(&(port_addresses[(uint8_t) port]));
}

//--------------------------------

ATTRIBUTE_HAL_SECTION
void
hal_clean_eeprom()
{
    uint16_t magic = 0;

    eeprom_busy_wait();

    CRITICAL_SECTION()
    {
        magic = eeprom_read_word(&eerom_magic);
    }

	if (magic == EEPROM_MAGIC)
	{
	    eeprom_busy_wait();
	    CRITICAL_SECTION()
        {
            eeprom_update_word(&eerom_magic, 0);
        }
	}

	return;
}



ATTRIBUTE_HAL_SECTION
int8_t
hal_has_configuration()
{
    return 0;//__eeprom_check_recs(EEPROM_MAGIC);
}

ATTRIBUTE_HAL_SECTION
uint16_t hal_eeprom_read_magic()
{
    return eeprom_read_word(&eerom_magic);
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_load_config()
{
     //load configuration
    if (eeprom_read_word(&eerom_magic) == EEPROM_MAGIC)
    {
        //load from eeprom
        hal_restore_config();
       //hal_init_statically();
    }
    else
    {
        hal_init_statically();
    }
}

ATTRIBUTE_HAL_SECTION
t_dev_count
hal_read_eeprom_recs_am()
{
    t_dev_count cnt = 0;
    //get amount of the records
    eeprom_busy_wait();

    CRITICAL_SECTION()
    {
        eeprom_read_block((void *)&cnt, (void*)&eerom_dd_recs, sizeof(t_dev_count));
      //  cnt = eeprom_read_byte((void*)&eerom_dd_recs);
    }
    return cnt;
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_read_eeprom_offset(t_dev_count offset, struct dev_descr_node_envilope * envlp)
{
    ASSERT_NULL_ER(envlp, SYSTEM_NULL_ARGUMENT, -1);

    t_dev_count cnt = hal_read_eeprom_recs_am();

    if (cnt == 0)
    {
        return -1;
    }
    else if (offset > (cnt-1))
    {
        return -1;
    }

    //read node information
    eeprom_busy_wait();

    CRITICAL_SECTION()
    {
        eeprom_read_struct((const void *)&eerom_dd_dev_env[offset], (void*) envlp, sizeof(struct dev_descr_node_envilope));
    }

    return 0;
}

ATTRIBUTE_HAL_SECTION
static int8_t
hal_restore_config()
{
    struct dev_descr_node_envilope envlp = {0};
    t_dev_id ret = 0;
    t_dev_count cnt = 0;
    t_dev_count i = 0;
    uint8_t dev_data[EEPROM_DEV_MEM_ALLOC];

    //get amount of the records
    eeprom_busy_wait();
    //eeprom_read_block((void *)&cnt, (void*)&eerom_dd_recs, sizeof(t_dev_count));
    CRITICAL_SECTION()
    {
        cnt = hal_read_eeprom_recs_am();
    }

#if DEBUG_MESSAGES == YES
    printf_P(PSTR("read %d\r\n"), cnt);
#endif // DEBUG_MESSAGES

    //retrieve records
    while (cnt != i)
    {
        memset(dev_data, 0, EEPROM_DEV_MEM_ALLOC);

        //read node information
        eeprom_busy_wait();
        CRITICAL_SECTION()
        {
            eeprom_read_struct((const void *)&eerom_dd_dev_env[i], (void*) &envlp, sizeof(struct dev_descr_node_envilope));
        }

#if DEBUG_MESSAGES == YES
         printf_P(PSTR("read envilope\r\n"));
#endif // DEBUG_MESSAGES

        //create node
        struct dev_descr_node * node = hal_dev_node_create(envlp.dev_id, envlp.e_dev_descr, envlp.e_dh_code, NULL, 0);
#if DEBUG_MESSAGES == YES
        if (node == NULL)
        {
            printf_P(PSTR("node create error\r\n"));
        }
#endif // DEBUG_MESSAGES

        //read configuration from eeprom
        eeprom_busy_wait();

        CRITICAL_SECTION()
        {
            eeprom_read_block((void*) dev_data, (const void *)&eerom_dd_dev_sets[i], EEPROM_DEV_MEM_ALLOC);
        }

#if DEBUG_MESSAGES == YES
         printf_P(PSTR("read data\r\n"));
#endif // DEBUG_MESSAGES

        //send signal to node to restore
        ret = hal_dev_node_restore(node, dev_data, EEPROM_DEV_MEM_ALLOC);
        if (ret == K_NO_DD)
        {
            //restore failed!
            //ToDO
#if DEBUG_MESSAGES == YES
            printf_P(PSTR("restore failed!\r\n"));
#endif // DEBUG_MESSAGES

        }

        i++;
    }

    hal_external_config_restore();

    return 0;

}

ATTRIBUTE_HAL_SECTION
int8_t
hal_dump_config(FILE * file,
                void (*CALLBACK(FILE *, t_dev_count)),
                void (*CALLBACK_ERROR(FILE *, t_dev_count)))
{
    struct dev_descr_node_envilope envlp = {0};
    uint8_t ret = 0;
    t_dev_count i = 0;
    uint8_t dev_data[EEPROM_DEV_MEM_ALLOC];

    ITERATE_BLOCK_DIRECT_MAN(struct dd_list, dd_list_ptr, it)
    {
        if ( (file != NULL) && (CALLBACK != NULL) )
        {
            CALLBACK(file, i);
        }
        memset(dev_data, 0, EEPROM_DEV_MEM_ALLOC);

        //prepare envilope for each dd eerom_dd_dev_env
        envlp.e_dev_descr = it->node->e_dev_descr;
        envlp.e_dh_code = it->node->e_dh_code;
        envlp.dev_id = it->node->dev_id;

        //dump to eeprom
        eeprom_busy_wait();

        CRITICAL_SECTION()
        {
            eeprom_write_struct(&eerom_dd_dev_env[i], &envlp, sizeof(struct dev_descr_node_envilope));
        }

        //request the device to dump its config
        ret = hal_dev_node_dump(it->node, &dev_data, EEPROM_DEV_MEM_ALLOC);
        if (ret == K_NO_DD)
        {
            //dump failed!
            //ToDo
            if ((file != NULL) && (CALLBACK_ERROR != NULL))
            {
                CALLBACK_ERROR(file, i);
            }
        }
        else
        {
            eeprom_busy_wait();
            CRITICAL_SECTION()
            {
                eeprom_update_block((const void *)dev_data, (void*)&eerom_dd_dev_sets[i], EEPROM_DEV_MEM_ALLOC);
            }
        }
        i++;
        ITERATE_NEXT(it);
    }

    //write down the amount of the records
    //lock hal mutex
    kernel_mutex_lock(hal_mux);
    eeprom_busy_wait();

    CRITICAL_SECTION()
    {
        //eeprom_update_byte((void*)&eerom_dd_recs, i);
        eeprom_update_block((const void *) &i,(void*)&eerom_dd_recs, sizeof(t_dev_count));
    }
    kernel_mutex_unlock(hal_mux);

    //write external config
    hal_external_config_dump();

    //WRITE MAGIC
    CRITICAL_SECTION()
    {
        eeprom_update_word(&eerom_magic, EEPROM_MAGIC);
    }

    return 0;
}
