/*-
 *  ATTENTION!: the initial version of the DHCP logic was taken from:
 *		link:http://easyelectronics.ru/files/WE/redsh/eth_232.zip
 *		link2:http://we.easyelectronics.ru/electro-and-pc/podklyuchenie-mikrokontrollera-k-lokalnoy-seti-udp-server.html
 *		License: WTFPL
 *
 *	--------APPLICABLE ONLY FOR DSC code, not ENC28J60 logic/routnies------
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

 /* ToDo:
  * [] Add settings selection on init and mode switch
  * [] Add retransmit functionality for TCP/IP support
  */

#include "../../build_defs.h"
#include <avr/io.h>
#include <util/delay.h>

#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>

#include "../../hal.h"
#include "../spi/spi.h"
#include "enc28j60.h"

static uint8_t enc28j60_read_op(struct enc28_state * enc28, uint8_t cmd, uint8_t adr);
static void enc28j60_write_op(struct enc28_state * enc28, uint8_t cmd, uint8_t adr, uint8_t data);
static void enc28j60_soft_reset(struct enc28_state * enc28);
static void enc28j60_set_bank(struct enc28_state * enc28, uint8_t adr);



#define enc28j60_select(_SPI_, _CS_SEL_) spi_run_session(_SPI_, _CS_SEL_)
#define enc28j60_release(_SPI_, _CS_SEL_) spi_finish_session(_SPI_, _CS_SEL_)
#define enc28j60_rx(_SPI_) spi_transfer(_SPI_, 0xff)
#define enc28j60_tx(_SPI_, _data_) spi_transfer(_SPI_, _data_)

// Generic SPI read command
static uint8_t enc28j60_read_op(struct enc28_state * enc28, uint8_t cmd, uint8_t adr)
{
    ASSERT_NULL_E(enc28, SYSTEM_NULL_ARGUMENT, error);
	uint8_t data;

	enc28j60_select(enc28->spi, enc28->spi_cs_sel);
	enc28j60_tx(enc28->spi, cmd | (adr & ENC28J60_ADDR_MASK));
	if(adr & 0x80) // throw out dummy byte
		enc28j60_rx(enc28->spi); // when reading MII/MAC register
	data = enc28j60_rx(enc28->spi);
	enc28j60_release(enc28->spi, enc28->spi_cs_sel);

	return data;

error:
    return 0;
}

// Generic SPI write command
static void enc28j60_write_op(struct enc28_state * enc28, uint8_t cmd, uint8_t adr, uint8_t data)
{
	enc28j60_select(enc28->spi, enc28->spi_cs_sel);
	enc28j60_tx(enc28->spi, cmd | (adr & ENC28J60_ADDR_MASK));
	enc28j60_tx(enc28->spi, data);
	enc28j60_release(enc28->spi, enc28->spi_cs_sel);

    return;
}

// Initiate software reset
static void enc28j60_soft_reset(struct enc28_state * enc28)
{

	enc28j60_select(enc28->spi, enc28->spi_cs_sel);
	enc28j60_tx(enc28->spi, ENC28J60_SPI_SC);
	enc28j60_release(enc28->spi, enc28->spi_cs_sel);
	enc28->enc28j60_current_bank = 0;
	_delay_ms(60); // Wait until device initializes

	return;
}


/*
 * Memory access
 */

// Set register bank
static void enc28j60_set_bank(struct enc28_state * enc28, uint8_t adr)
{
	/*uint8_t bank;

	if( (adr & ENC28J60_ADDR_MASK) < ENC28J60_COMMON_CR )
	{
		bank = (adr >> 5) & 0x03; //BSEL1|BSEL0=0x03
		if(bank != enc28->enc28j60_current_bank)
		{
			enc28j60_write_op(enc28, ENC28J60_SPI_BFC, ECON1, 0x03);
			enc28j60_write_op(enc28, ENC28J60_SPI_BFS, ECON1, bank);
			enc28->enc28j60_current_bank = bank;
		}
	}*/

	 if((adr & BANK_MASK) != enc28->enc28j60_current_bank)
        {
                // set the bank
                enc28j60_write_op(enc28, ENC28J60_SPI_BFC, ECON1, (ECON1_BSEL1|ECON1_BSEL0));
                enc28j60_write_op(enc28, ENC28J60_SPI_BFS, ECON1, (adr & BANK_MASK)>>5);
                enc28->enc28j60_current_bank = (adr & BANK_MASK);
        }

	return;
}

static void enc28j60_mem_read(struct enc28_state * enc28, uint16_t addr, uint16_t len, uint8_t *data)
{
	enc28j60_wcr16(enc28, ERDPTL, addr);

	enc28j60_read_buffer(enc28, data, len);

}

// Read register
uint8_t enc28j60_rcr(struct enc28_state * enc28, uint8_t adr)
{
	ASSERT_NULL_E(enc28, SYSTEM_NULL_ARGUMENT, error);

	enc28j60_set_bank(enc28, adr);
	return enc28j60_read_op(enc28, ENC28J60_SPI_RCR, adr);

error:
	return 0;
}

// Read register pair
uint16_t enc28j60_rcr16(struct enc28_state * enc28, uint8_t adr)
{
	ASSERT_NULL_E(enc28, SYSTEM_NULL_ARGUMENT, error);

	enc28j60_set_bank(enc28, adr);
	return enc28j60_read_op(enc28, ENC28J60_SPI_RCR, adr) |
		(enc28j60_read_op(enc28, ENC28J60_SPI_RCR, adr+1) << 8);

error:
	return 0;
}

// Write register
void enc28j60_wcr(struct enc28_state * enc28, uint8_t adr, uint8_t arg)
{
	ASSERT_NULL_E(enc28, SYSTEM_NULL_ARGUMENT, error);

	enc28j60_set_bank(enc28, adr);
	enc28j60_write_op(enc28, ENC28J60_SPI_WCR, adr, arg);

error:
	return;
}

// Write register pair
void enc28j60_wcr16(struct enc28_state * enc28, uint8_t adr, uint16_t arg)
{
	ASSERT_NULL_E(enc28, SYSTEM_NULL_ARGUMENT, error);
	enc28j60_set_bank(enc28, adr);
	enc28j60_write_op(enc28, ENC28J60_SPI_WCR, adr, arg);
	enc28j60_write_op(enc28, ENC28J60_SPI_WCR, adr+1, arg>>8);

error:
	return;
}

// Clear bits in register (reg &= ~mask)
void enc28j60_bfc(struct enc28_state * enc28, uint8_t adr, uint8_t mask)
{
	ASSERT_NULL_E(enc28, SYSTEM_NULL_ARGUMENT, error);

	enc28j60_set_bank(enc28, adr);
	enc28j60_write_op(enc28, ENC28J60_SPI_BFC, adr, mask);

error:
	return;
}

// Set bits in register (reg |= mask)
void enc28j60_bfs(struct enc28_state * enc28, uint8_t adr, uint8_t mask)
{
	ASSERT_NULL_E(enc28, SYSTEM_NULL_ARGUMENT, error);

	enc28j60_set_bank(enc28, adr);
	enc28j60_write_op(enc28, ENC28J60_SPI_BFS, adr, mask);

error:
	return;
}

// Read Rx/Tx buffer (at ERDPT)
void enc28j60_read_buffer(struct enc28_state * enc28, uint8_t *buf, uint16_t len)
{
    ASSERT_NULL_E(enc28, SYSTEM_NULL_ARGUMENT, error);
    ASSERT_NULL_E(buf, SYSTEM_NULL_ARGUMENT, error);

	enc28j60_select(enc28->spi, enc28->spi_cs_sel);
	enc28j60_tx(enc28->spi, ENC28J60_SPI_RBM);
	while(len--)
	{
		*(buf++) = enc28j60_rx(enc28->spi);
	}
	enc28j60_release(enc28->spi, enc28->spi_cs_sel);

error:
    return;
}

// Write Rx/Tx buffer (at EWRPT)
void enc28j60_write_buffer(struct enc28_state * enc28, uint8_t *buf, uint16_t len)
{
    ASSERT_NULL_E(enc28, SYSTEM_NULL_ARGUMENT, error);
    ASSERT_NULL_E(buf, SYSTEM_NULL_ARGUMENT, error);

	enc28j60_select(enc28->spi, enc28->spi_cs_sel);
	enc28j60_tx(enc28->spi, ENC28J60_SPI_WBM);
	while(len--)
	{
		enc28j60_tx(enc28->spi, *buf++);
	}

	enc28j60_release(enc28->spi, enc28->spi_cs_sel);

error:
    return;
}

// Read PHY register
uint16_t enc28j60_read_phy(struct enc28_state * enc28, uint8_t adr)
{
	ASSERT_NULL_E(enc28, SYSTEM_NULL_ARGUMENT, error);

	enc28j60_wcr(enc28, MIREGADR, adr);
	enc28j60_wcr(enc28, MICMD, MICMD_MIIRD);//enc28j60_bfs(enc28, MICMD, MICMD_MIIRD);

	while(enc28j60_rcr(enc28, MISTAT) & MISTAT_BUSY);

	enc28j60_wcr(enc28, MICMD, 0);

	//enc28j60_bfc(enc28, MICMD, MICMD_MIIRD);
	return enc28j60_rcr16(enc28, MIRD);

error:
	return 0;
}

// Write PHY register
void enc28j60_write_phy(struct enc28_state * enc28, uint8_t adr, uint16_t data)
{
	ASSERT_NULL_E(enc28, SYSTEM_NULL_ARGUMENT, error);

	enc28j60_wcr(enc28, MIREGADR, adr);
	enc28j60_wcr16(enc28, MIWR, data);
	while(enc28j60_rcr(enc28, MISTAT) & MISTAT_BUSY);

error:
	return;
}


/*
 * Init & packet Rx/Tx
 */

int8_t enc28j60_init(struct enc28_state * enc28_inst)
{
    ASSERT_NULL_E(enc28_inst, SYSTEM_NULL_ARGUMENT, exit_error);

	// Reset ENC28J60
	enc28j60_soft_reset(enc28_inst);

	// Setup Rx/Tx buffer
	enc28_inst->enc28j60_rxrdpt = RXSTART_INIT;

	enc28j60_wcr16(enc28_inst, ERXST, RXSTART_INIT);

	uint16_t erxrdpt;
	if ((enc28_inst->enc28j60_rxrdpt - 1 < RXSTART_INIT) || (enc28_inst->enc28j60_rxrdpt - 1 > RXEND_INIT))
		erxrdpt = RXEND_INIT;
	else
		erxrdpt = enc28_inst->enc28j60_rxrdpt - 1;


	enc28j60_wcr16(enc28_inst, ERXRDPT, erxrdpt);
	enc28j60_wcr16(enc28_inst, ERXND, RXEND_INIT);
	/* set transmit buffer start + end */
	enc28j60_wcr16(enc28_inst, ETXST, TXSTART_INIT);
	enc28j60_wcr16(enc28_inst, ETXND, TXEND_INIT);

	enc28j60_wcr(enc28_inst, ERXFCON, 0);
	enc28j60_wcr(enc28_inst, EPMM0, 0);
	enc28j60_wcr(enc28_inst, EPMM1, 0);
	enc28j60_wcr(enc28_inst, EPMCSL, 0);
	enc28j60_wcr(enc28_inst, EPMCSH, 0);

	// Setup MAC
	enc28j60_wcr(enc28_inst, MACON1, MACON1_TXPAUS| // Enable flow control
		MACON1_RXPAUS|MACON1_MARXEN); // Enable MAC Rx
	enc28j60_wcr(enc28_inst, MACON2, 0); // Clear reset
	/*enc28j60_wcr(enc28_inst, MACON3, MACON3_PADCFG0| // Enable padding,
		MACON3_TXCRCEN|MACON3_FRMLNEN|MACON3_FULDPX);*/ // Enable crc & frame len chk
	enc28j60_bfs(enc28_inst, MACON3, MACON3_PADCFG0| // Enable padding,
		MACON3_TXCRCEN|MACON3_FRMLNEN|MACON3_FULDPX);

	enc28j60_wcr(enc28_inst, MABBIPG, 0x12/*0x15*/); // Set inter-frame gap
	enc28j60_wcr(enc28_inst, MAIPGL, 0x12);
	enc28j60_wcr(enc28_inst, MAIPGH, 0x0C);
	enc28j60_wcr16(enc28_inst, MAMXFL, MAX_FRAMELEN);//ENC28J60_MAXFRAME);

	enc28j60_wcr(enc28_inst, MAADR5, enc28_inst->mac[0]); // Set MAC address
	enc28j60_wcr(enc28_inst, MAADR4, enc28_inst->mac[1]);
	enc28j60_wcr(enc28_inst, MAADR3, enc28_inst->mac[2]);
	enc28j60_wcr(enc28_inst, MAADR2, enc28_inst->mac[3]);
	enc28j60_wcr(enc28_inst, MAADR1, enc28_inst->mac[4]);
	enc28j60_wcr(enc28_inst, MAADR0, enc28_inst->mac[5]);

	// Setup PHY
	enc28j60_write_phy(enc28_inst, PHCON1, PHCON1_PDPXMD); // Force full-duplex mode

	enc28j60_write_phy(enc28_inst, PHCON2, PHCON2_HDLDIS); // Disable loopback
	enc28j60_write_phy(enc28_inst, PHLCON, PHLCON_LACFG2| // Configure LED ctrl
		PHLCON_LBCFG2|PHLCON_LBCFG1|PHLCON_LBCFG0|
		PHLCON_LFRQ0|PHLCON_STRCH);
//enc28j60_bfs(enc28_inst,EIE,(EIE_INTIE|EIE_PKTIE));
	// Enable Rx packets
	enc28j60_bfs(enc28_inst, ECON1, ECON1_RXEN);

    return 0;

exit_error:
    return 1;
}

int32_t enc28j60_send_packet(struct enc28_state * enc28_inst, uint8_t *data, uint16_t len)
{
    ASSERT_NULL_E(enc28_inst, SYSTEM_NULL_ARGUMENT, error);
    ASSERT_NULL_E(data, SYSTEM_NULL_ARGUMENT, error);


	#if (DEBUG == YES)
		uint8_t ECON1_R = enc28j60_rcr(enc28_inst, ECON1);
		printf_P(PSTR("S1-->ECON1=%d EIR=%d\r\n"), ECON1_R, enc28j60_rcr(enc28_inst, EIR));
	#endif

	//---!fix outgoing error
	if (enc28j60_rcr(enc28_inst, ECON1) & ECON1_BSEL0)
	{
		enc28j60_bfc(enc28_inst, ECON1, ECON1_BSEL0);
	}
	// cancel previous transmission if stuck
	while(enc28j60_rcr(enc28_inst, ECON1) & ECON1_TXRTS)
	{
		enc28j60_bfc(enc28_inst, ECON1, ECON1_TXRTS);
		enc28j60_bfs(enc28_inst, ECON1, ECON1_TXRST);
		enc28j60_bfc(enc28_inst, ECON1, ECON1_TXRST);
	}

	if( (enc28j60_rcr(enc28_inst, EIR) & EIR_TXERIF))
	{
		enc28j60_bfc(enc28_inst, EIR, EIR_TXERIF|EIR_TXIF);
	}

	enc28j60_wcr16(enc28_inst, ETXST, TXSTART_INIT);
	enc28j60_wcr16(enc28_inst, EWRPT, TXSTART_INIT);

	enc28j60_write_op(enc28_inst, ENC28J60_SPI_WBM, 0, 0x00);
	enc28j60_write_buffer(enc28_inst, data, len);



	enc28j60_wcr16(enc28_inst, ETXND, TXSTART_INIT + len);
	uint16_t ERWPT_R = enc28j60_rcr16(enc28_inst, EWRPT);
	uint16_t ETXND_R = enc28j60_rcr16(enc28_inst, ETXND);

	#if (DEBUG == YES)
	printf_P(PSTR("-->ETXST: %x ERWPT:%x, ETXND:%x len=%d P=%p ECON1=%d EIR=%d\r\n"), enc28j60_rcr16(enc28_inst, ETXST), ERWPT_R, ETXND_R, len, data, enc28j60_rcr(enc28_inst, ECON1), enc28j60_rcr(enc28_inst, EIR));
	#endif
	//enc28j60_wcr16(enc28_inst, ETXST, ENC28J60_TXSTART);

	if ((ETXND_R+1) != ERWPT_R)
	{
	    #if (DEBUG == YES)
		printf_P(PSTR("Dropped: %p len: %u!\r\n"), data, len);
		#endif
		setSysError(SYSTEM_NET_INTERFACE_INTERNAL_ERR);
		return -1;
	}

	/*enc28j60_mem_read(enc28_inst, TXSTART_INIT + 1, test_len, test_buf);
	uint8_t testf = 0;
		for (uint16_t k = 0; k < test_len; k++)
		{
			if (data[k] != test_buf[k])
			{
				testf = 1;
				printf_P(PSTR("Error, %d location differ: 0x%02x-0x%02x\r\n"), k,
					 data[k], test_buf[k]);
			}
		}
		if (testf == 1)
		{
		for (uint16_t k = 0; k < test_len; k++)
		{

			printf_P(PSTR("%x|"), test_buf[k]);

		}
		printf_P(PSTR("\r\n"));
		}*/

	#if (DEBUG == YES)
	printf_P(PSTR("S2-->ECON1=%d EIR=%d\r\n"), enc28j60_rcr(enc28_inst, ECON1), enc28j60_rcr(enc28_inst, EIR));
	#endif

	enc28j60_bfs(enc28_inst, ECON1, ECON1_TXRTS); // Request packet send

	#if (DEBUG == YES)
	printf_P(PSTR("S3-->ECON1=%d EIR=%d\r\n"), enc28j60_rcr(enc28_inst, ECON1), enc28j60_rcr(enc28_inst, EIR));
	#endif

	return len;

error:
	return -1;
}

int32_t enc28j60_recv_packet(struct enc28_state * enc28_inst, uint8_t *buf, uint16_t buflen)
{
    ASSERT_NULL_E(enc28_inst, SYSTEM_NULL_ARGUMENT, error);
    ASSERT_NULL_E(buf, SYSTEM_NULL_ARGUMENT, error);

	uint16_t len = 0, rxlen, status, temp;

	if( (enc28j60_rcr(enc28_inst, EIR) & EIR_LINKIF))
	{

		//read PHIR to clear
		uint16_t phir = enc28j60_read_phy(enc28_inst, PHIR);
		#if (DEBUG == YES)
		printf_P(PSTR("PHIR:%x\r\n"), PHIR);
		#endif
	}

	if( (enc28j60_rcr(enc28_inst, EIR) & EIR_RXERIF))
	{
		enc28j60_bfc(enc28_inst, EIR, EIR_RXERIF);
		#if (DEBUG == YES)
		printf_P(PSTR("Receive Error Interrupt %x\r\n"), PHIR);
		#endif
		enc28j60_init(enc28_inst);
		return -1;
	}

	if( enc28j60_rcr(enc28_inst, ECON1) == 0)
	{
	    #if (DEBUG == YES)
		printf_P(PSTR("Something happened ECON1 == 0\r\n"));
		#endif
		enc28j60_init(enc28_inst);
		return -1;
	}

	if(enc28j60_rcr(enc28_inst, EPKTCNT))
	{
		/*printf_P(PSTR("PKT received!\r\n"));*/
		enc28j60_wcr16(enc28_inst, ERDPT, enc28_inst->enc28j60_rxrdpt);

		enc28j60_read_buffer(enc28_inst, (void*)&enc28_inst->enc28j60_rxrdpt, sizeof(enc28_inst->enc28j60_rxrdpt));
		enc28j60_read_buffer(enc28_inst, (void*)&rxlen, sizeof(rxlen));
		enc28j60_read_buffer(enc28_inst, (void*)&status, sizeof(status));

		if(status & 0x80) //success
		{
			len = rxlen - 4; //throw out crc
			if(len > buflen) len = buflen;
			enc28j60_read_buffer(enc28_inst, buf, len);
		}
		else
		{
		    #if (DEBUG == YES)
            printf_P(PSTR("enc28j60: recv_packet FAILED\r\n"));
		    #endif
		}

		// Set Rx read pointer to next packet
		temp = (enc28_inst->enc28j60_rxrdpt - 1) & RXEND_INIT;//ENC28J60_BUFEND;
		enc28j60_wcr16(enc28_inst, ERXRDPT, temp);

		// Decrement packet counter
		enc28j60_bfs(enc28_inst, ECON2, ECON2_PKTDEC);
	}

	return len;

error:
    return -1;
}
