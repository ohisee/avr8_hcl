#ifndef ENC28_H_INCLUDED
#define ENC28_H_INCLUDED

#include "./enc28j60.h"

#define ENC28_REVISION 1

struct enc28_config
{
    enum ports e_port;
    uint8_t p_mask;
    t_dev_id spi_dev_id;
    uint8_t mac_addr[6];
};

struct enc28_record
{
    struct dev_descr_node * ddn;                        //FD instance
    struct knetdevice * k_net_dev;                      //pointer to network record
    struct enc28_state hw_state;                        //enc28 specific
    FILE * file_rxed;                                  //file has rxed current state
};

struct enc28_envilope
{
    enum ports e_port;
    uint8_t p_mask;
    t_dev_id spi_dev_id;
    uint8_t mac[6];
};

/*
 *   Standart module functions
 */
int8_t enc28_hal_message(struct dev_descr_node * ddn,
                          enum dd_event e_event,
                          void * data0,
                          size_t * dsize0);

int8_t enc28_kstream_init(struct dev_descr_node * ddn,
                           struct kstream * ks);

int8_t enc28_stream_init(struct dev_descr_node * ddn,
                             FILE * file);




#endif // ENC_HANDLER_H_INCLUDED
