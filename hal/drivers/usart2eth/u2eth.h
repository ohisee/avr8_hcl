#ifndef U2ETH_H_INCLUDED
#define U2ETH_H_INCLUDED

#define U2ETH_REVISION 1

#include "../../../os/kernel/kernel_process.h"

struct u2eth_instance
{
    k_pid pid;
    struct dev_descr_node * ddn;
    struct u2eth_route * u2eth_list_ptr;
    struct kproc_mutex * m_u2e;
};

struct u2eth_route
{
    struct dev_descr_node * from;
    uint32_t ip_to;
    uint16_t port_to;
    t_netdev_id netdev;

    struct u2eth_route * next;
};

struct u2eth_envelope
{
    t_dev_id serial_did;
    uint32_t ip_to;
    uint16_t port_to;
    t_netdev_id netdev;
};

/*
 *   Standart module functions
 */
int8_t u2eth_hal_message(struct dev_descr_node * ddn,
                          enum dd_event e_event,
                          void * data0,
                          size_t * dsize0);

int8_t u2eth_kstream_init(struct dev_descr_node * ddn,
                           struct kstream * ks);

int8_t u2eth_stream_init(struct dev_descr_node * ddn,
                             FILE * file);

int8_t u2eth_add(struct dev_descr_node * ddn, t_dev_id did, uint32_t ip, uint16_t port, t_netdev_id tnid);

#endif // U2ETH_H_INCLUDED
