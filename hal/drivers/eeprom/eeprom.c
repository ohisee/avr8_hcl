/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <util/atomic.h>
#include <string.h>
#include <stdint.h>
#include "../../device/dev.h"
#include "eeprom.h"



void eeprom_clear()
{
	ATOMIC_BLOCK (ATOMIC_RESTORESTATE){
		uint16_t addr = 0;
		uint8_t eecr = EECR;
		EECR = 0;
		for (;addr < E2END; addr++)
		{
			eeprom_busy_wait();
			EEAR = addr;
			EEDR = 0x00;
			EECR |= (1<<EEMPE);
			EECR |= (1<<EEPE);
		}

		//restore back EEPROM EECR
		EECR = eecr;
	}

	return;
}

void eeprom_write_double(const double *addr, double inp)
{
	eeprom_busy_wait();
	eeprom_update_block((const void*)&inp, (void*)addr, sizeof(double));

	return;
}

void eeprom_write_string(const uint8_t *addr, char * inp)
{
	eeprom_busy_wait();
	eeprom_update_block((const void*)inp, (void*)addr, strlen(inp));

	return;
}

void eeprom_write_struct(const void *addr, const void * inp, size_t st_size)
{
	eeprom_busy_wait();
	eeprom_update_block(inp, (void*)addr, st_size);
}

void eeprom_update_double(const double *addr, double inp)
{
	eeprom_busy_wait();
	eeprom_update_block((const void*)&inp, (void*)addr, sizeof(double));

	return;
}

void eeprom_update_string(const uint8_t *addr, char * inp)
{
	eeprom_busy_wait();
	eeprom_update_block((const void*)inp, (void*)addr, strlen(inp));

	return;
}

void eeprom_update_struct(const void *addr, const void * inp, size_t st_size)
{
	eeprom_busy_wait();
	eeprom_update_block(inp, (void*)addr, st_size);
}

void eeprom_read_double(const double * addr, double * inp)
{
	eeprom_busy_wait();
	eeprom_read_block((void *)inp, (const void*)addr, sizeof(double));

	return;
}

void eeprom_read_string(const uint8_t *addr, uint8_t * inp)
{
	eeprom_busy_wait();
	eeprom_read_block((void *)inp, (const void*)addr, sizeof(inp));

	return;
}

void eeprom_read_struct(const void *addr, void * inp, size_t st_size)
{
	eeprom_busy_wait();
	eeprom_read_block((void *)inp, (const void*)addr, st_size);

	return;
}
