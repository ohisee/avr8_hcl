/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef EEPROM_H_
#define EEPROM_H_

#include <avr/eeprom.h>

void eeprom_clear();
uint16_t eeprom_left_space();

void eeprom_write_double(const double *addr, double inp);
void eeprom_write_string(const uint8_t *addr, char * inp);
void eeprom_write_struct(const void *addr, const void * inp, size_t st_size);

void eeprom_update_double(const double *addr, double inp);
void eeprom_update_string(const uint8_t *addr, char * inp);
void eeprom_update_struct(const void *addr, const void * inp, size_t st_size);

void eeprom_read_double(const double * addr, double * inp);
void eeprom_read_string(const uint8_t *addr, uint8_t * inp);
void eeprom_read_struct(const void *addr, void * inp, size_t st_size);



#endif /* EEPROM_H_ */
