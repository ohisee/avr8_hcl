/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#ifndef TIMER_H_INCLUDED
#define TIMER_H_INCLUDED

#define TIMER_DRIVER_VERSION 3

#define FOREACH_PWM_OPERA(DV)\
    DV(OCR_pwm_normalA, (uint8_t) 0)\
    DV(OCR_pwm_A0, (uint8_t) 64)\
    DV(OCR_pwm_A1, (uint8_t) 128)\
    DV(OCR_pwm_A1A0, (uint8_t) 192)

enum OCR_pwm_operA
{
    FOREACH_PWM_OPERA(GENERATE_ENUM_VAL)
};

#define FOREACH_PWM_OPERB(DV)\
    DV(OCR_pwm_normalB, (uint8_t) 0)\
    DV(OCR_pwm_B0, (uint8_t) 16)\
    DV(OCR_pwm_B1, (uint8_t) 32)\
    DV(OCR_pwm_B1B0, (uint8_t) 48)

enum OCR_pwm_operB
{
    FOREACH_PWM_OPERB(GENERATE_ENUM_VAL)
};

/*
8-BIT TIMER
Mode      WGM2     WGM1	    WGM0	Timer/Counter Mode of Operation		TOP	    Update of OCRx at	TOV Flag Set on (1)(2)
0	        0        0        0			        Normal                  0xFF         Immediate                MAX
1	        0	    0	     1      	    PWM, Phase Correct          0xFF            TOP           		BOTTOM
2	        0	    1	     0                  CTC                 	OCRA	     Immediate  		      MAX
3	        0	    1	     1                  Fast PWM                0xFF            TOP                   MAX
4	        1	    0	     0                  Reserved                 –       	    –            		    –
5	        1	    0	     1      		PWM, Phase Correct          OCRA            TOP                 BOTTOM
6	        1	    1	     0                	Reserved                 –       	    –            		    –
7	        1	    1	     1               	Fast PWM			    OCRA		    BOTTOM			    TOP
*/

/*
16-BIT TIMERS
Mode	WGMn3	WGMn2(CTCn)	WGMn1(PWMn1)	WGMn0(PWMn0)	Timer/CounterMode of Operation		TOP         Update of OCRnx at	TOVn Flag Set on
0	    0   	0   		0   		    0		        Normal                       		0xFFFF        Immediate             MAX
1	    0   	0   		0   		    1		        PWM, Phase Correct, 8-bit       	0x00FF           TOP              BOTTOM
2	    0   	0   		1   		    0		        PWM, Phase Correct, 9-bit       	0x01FF           TOP              BOTTOM
3	    0   	0   		1   		    1		        PWM, Phase Correct, 10-bit      	0x03FF           TOP              BOTTOM
4       0       1           0               0               CTC					                OCRnA         Immediate             MAX
5	    0   	1   		0   		    1               Fast PWM, 8-bit                 	0x00FF         BOTTOM               TOP
6	    0   	1   		1   		    0               Fast PWM, 9-bit                		0x01FF         BOTTOM               TOP
7	    0   	1   		1   		    1           	Fast PWM, 10-bit               		0x03FF         BOTTOM               TOP
8	    1   	0   		0   		    0		        PWM, Phase and Frequency Correct 	ICRn           BOTTOM          	  BOTTOM
9	    1   	0   		0   		    1		        PWM,Phase and Frequency Correct 	OCRnA          BOTTOM          	  BOTTOM
10	    1	    0		    1		        0		        PWM, Phase Correct              	ICRn             TOP		      BOTTOM
11	    1	    0		    1		        1		        PWM, Phase Correct            		OCRnA            TOP		      BOTTOM
12      1       1           0               0               CTC                           		ICRn          Immediate             MAX
13	    1	    1		    0	            1        	    (Reserved)       			        –  		        –     		        –
14	    1	    1		    1		        0         	    Fast PWM				            ICRn		   BOTTOM		        TOP
15	    1	    1		    1		        1         	    Fast PWM                     		OCRnA          BOTTOM               TOP
*/

#define FOREACH_WAVEFORM_MODE(DV)\
    DV(WAVEFORM_MODE_0, (uint8_t) 0)\
    DV(WAVEFORM_MODE_1, (uint8_t) 1)\
    DV(WAVEFORM_MODE_2, (uint8_t) 2)\
    DV(WAVEFORM_MODE_3, (uint8_t) 3)\
    DV(WAVEFORM_MODE_4, (uint8_t) 4)\
    DV(WAVEFORM_MODE_5, (uint8_t) 5)\
    DV(WAVEFORM_MODE_6, (uint8_t) 6)\
    DV(WAVEFORM_MODE_7, (uint8_t) 7)\
    DV(WAVEFORM_MODE_8, (uint8_t) 8)\
    DV(WAVEFORM_MODE_9, (uint8_t) 9)\
    DV(WAVEFORM_MODE_10, (uint8_t) 10)\
    DV(WAVEFORM_MODE_11, (uint8_t) 11)\
    DV(WAVEFORM_MODE_12, (uint8_t) 12)\
    DV(WAVEFORM_MODE_13, (uint8_t) 13)\
    DV(WAVEFORM_MODE_14, (uint8_t) 14)\
    DV(WAVEFORM_MODE_15, (uint8_t) 15)

enum waveform_mode
{
    FOREACH_WAVEFORM_MODE(GENERATE_ENUM_VAL)
};

struct waveform_mode_gen
{
    uint8_t tccra; //wgm1,0
    uint8_t tccrb; //wgm2,3
};

extern const struct waveform_mode_gen waveform_gens[] PROGMEM;

#define FOREACH_TIMER_CLOCK(DV)\
    DV(TIMER_CLOCK_DISABLED, (uint8_t) 0)\
    DV(TIMER_CLOCK_NO_PRES, (uint8_t) 1)\
    DV(TIMER_CLOCK_PRESC_8, (uint8_t) 2)\
    DV(TIMER_CLOCK_PRESC_64, (uint8_t) 3)\
    DV(TIMER_CLOCK_PRESC_256, (uint8_t) 4)\
    DV(TIMER_CLOCK_PRESC_1024, (uint8_t) 5)\
    DV(TIMER_CLOCK_EXTERN_FALLING, (uint8_t) 6)\
    DV(TIMER_CLOCK_EXTERN_RISING, (uint8_t) 7)

enum timer_clock
{
    FOREACH_TIMER_CLOCK(GENERATE_ENUM_VAL)
};

//-------------PROTOCOL---------
/*#define FOREACH_TIMER_CMDS(DV)\
    DV(TIMER_CMD_SET_MODE, (uint8_t) 0)\
    DV(TIMER_CMD_REQUEST_INFO, (uint8_t) 1)\
    DV(TIMER_CMD_REQUEST_REG_INFO, (uint8_t) 2)\
    DV(TIMER_CMD_SET_OCR_REGISTER, (uint8_t) 10)\
    DV(TIMER_CMD_OPERATION_NOT_SUP, (uint8_t) 0xFF)

enum timer_cmds
{
    FOREACH_TIMER_CMDS(GENERATE_ENUM_VAL)
};
*/

enum timer_ocr_registers
{
    OCR_REG_A = (uint8_t) 0,
    OCR_REG_B = (uint8_t) 1,
    OCR_REG_C = (uint8_t) 2
};

union reg_bi_sized
{
    uint8_t ocr8;
    uint16_t ocr16;
};

struct timer_rec
{
    struct dev_descr_node * ddn;
    uint8_t counter_bit_width;

    enum timers_id e_timer;
    enum OCR_pwm_operA e_operA;
    enum OCR_pwm_operB e_operB;
    enum waveform_mode e_wave_mode;
    enum timer_clock e_clock;
    union reg_bi_sized ocra;
    union reg_bi_sized ocrb;
    union reg_bi_sized ocrc;
    union reg_bi_sized icr;
};

struct timer_setup
{
    uint8_t e_timer;    // enum timers_id
    uint8_t e_operA;        //enum OCR_pwm_operA
    uint8_t e_operB;        //enum OCR_pwm_operB
    uint8_t e_wave_mode;    //enum waveform_mode
    uint8_t e_clock; //enum timer_clock
    uint16_t ocra;
    uint16_t ocrb;
    uint16_t ocrc;
    uint16_t icr;
};

struct timer_envelope
{
    uint8_t e_timer;    // enum timers_id
    uint8_t e_operA;        //enum OCR_pwm_operA
    uint8_t e_operB;        //enum OCR_pwm_operB
    uint8_t e_wave_mode;    //enum waveform_mode
    uint8_t e_clock; //enum timer_clock
    uint16_t ocra;
    uint16_t ocrb;
    uint16_t ocrc;
    uint16_t icr;
};


/*
 *   Standart module functions
 */
int8_t timer_hal_message(struct dev_descr_node * ddn,
                          enum dd_event e_event,
                          void * data0,
                          size_t * dsize0);

int8_t timer_kstream_init(struct dev_descr_node * ddn,
                           struct kstream * ks);

int8_t timer_stream_init(struct dev_descr_node * ddn,
                             FILE * file);


struct timer_rec * timer_get_by_did(t_dev_id did);
int8_t timer_is_system(enum timers_id e_timer);
int8_t timer_mode_setting(struct dev_descr_node * ddn, struct timer_setup * tsp);
int8_t timer_update_single_ocr(struct dev_descr_node * dss, enum timer_ocr_registers e_reg, uint16_t ocr_val);
int8_t timer_update_ocrs(struct timer_rec * tmr, uint16_t ocra, uint16_t ocrb, uint16_t ocrc);

#endif // TIMER_H_INCLUDED
