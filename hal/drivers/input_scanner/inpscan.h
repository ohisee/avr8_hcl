#ifndef INPSCAN_H_INCLUDED
#define INPSCAN_H_INCLUDED

#define INPSCAN_REVISION 1

#include "../../../os/kernel/kernel_types.h"

struct dim_rec
{
    struct io_null_dev * ctrld;
    uint8_t scanres;

    struct dim_rec * next;
};

struct dim_instance
{
    struct dev_descr_node * dd;
    k_pid pid;
    struct kproc_mutex * m_isc;
    uint32_t ip_to;
    uint16_t port_to;
    struct knetdevice * kndev;
    struct dim_rec * dims;
};

struct dim_instance_config
{
    int32_t ip_to;
    uint16_t port_to;
    t_netdev_id netdev;
};

struct dim_instance_envelope
{
    int32_t ip_to;
    uint16_t port_to;
    t_netdev_id netdev;
    uint8_t resc[];
};

struct dim_rec_envelope
{
    t_dev_id io_did;
    uint8_t next[];
};

int8_t inpscan_hal_message(struct dev_descr_node * ddn,
                        enum dd_event e_event,
                        void * data0,
                        size_t * dsize0);
int8_t inpscan_kstream_init(struct dev_descr_node * ddn, struct kstream * ks);
int8_t inpscan_stream_init(struct dev_descr_node * ddn, FILE * file);

int8_t inpscan_listen(struct dev_descr_node * ddn, t_dev_id iodid);
int8_t inpscan_get_mem(struct dev_descr_node * ddn, uint8_t offset, struct dim_rec * ret);

#endif // INPSCAN_H_INCLUDED
