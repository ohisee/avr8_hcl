/*-
 *	Copyright (c) 2014-2016	Alexander Morozov www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"

#if BUILD_WITH_INPUT_SCANNER == YES

#include <avr/io.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <inttypes.h>
#include <avr/interrupt.h>
#include <avr/sfr_defs.h>
#include <util/atomic.h>
#include <util/delay.h>
#include "../../hal.h"
#include "../io/io.h"

#include "../../../os/kernel/kernel_memory.h"
#include "../../../os/kernel/kernel_process.h"
#include "../../../os/kernel/kernel_mutex.h"
#include "./inpscan.h"


#include "../../../processes/tabita/tabita.h"
#include "../../../processes/tabita/devices/dev_inpscan.h"

static int8_t inpscan_attach(struct dev_descr_node * ddn, struct dim_instance_config * din);
static int8_t inpscan_query(struct dev_descr_node * ddn, struct hal_query_info * data0);
static int8_t inpscan_detach(struct dev_descr_node * ddn);
static int8_t inpscan_update(struct dev_descr_node * ddn, struct dim_instance_config * ios);
static int8_t inpscan_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static int8_t inpscan_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0);


int8_t inpscan_hal_message(struct dev_descr_node * ddn,
                        enum dd_event e_event,
                        void * data0,
                        size_t * dsize0)
{
    int8_t ret = 0;

    switch (e_event)
    {
        case DD_EVENT_ATTACH:
            ret = inpscan_attach(ddn, (struct dim_instance_config *) data0);
        break;

        case DD_EVENT_DETACH:
            ret = inpscan_detach(ddn);
        break;

        case DD_EVENT_UPDATE:
            ret = inpscan_update(ddn, (struct dim_instance_config *) data0);
        break;

        case DD_DUMP:
            ret = inpscan_dump(ddn, data0, dsize0);
        break;

        case DD_RESTORE:
            ret = inpscan_restore(ddn, data0, dsize0);
        break;

        case DD_QUERY:
            ret = inpscan_query(ddn, (struct hal_query_info *) data0);
        break;

        case DD_ISRSIGNAL:
            ret = K_MSG_ERROR;
        break;

        case DD_RESET:
            ret = K_MSG_ERROR;
        break;

        case DD_DROP:
            ret = K_MSG_ERROR;
        break;

        case DD_FLUSH:
            ret = K_MSG_ERROR;
        break;
    }

    return ret;
}

int8_t
inpscan_kstream_init(struct dev_descr_node * ddn, struct kstream * ks)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

int8_t
inpscan_stream_init(struct dev_descr_node * ddn, FILE * file)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}
static void scan_proc(void * arg);

static void
scan_proc(void * arg)
{
    struct dim_instance * di = ( struct dim_instance *) arg;
    uint8_t res = 0;
    uint8_t ret = 0;
    while (1)
    {
        kernel_mutex_lock(di->m_isc);

        ITERATE_BLOCK_DIRECT_MAN( struct dim_rec, di->dims, _it)
        {
            ret = io_read8(_it->ctrld, &res);
            if (ret == 0)
            {
                if (res != _it->scanres)
                {
                    //inform host about changes
                    uint8_t * buf = hal_net_grab_tx_buffer(di->kndev, KNET_UDP);
                    struct tabita_request_ans * rans = tabita_prepare_header(buf, TAB_PKT_OPER_REQUEST_DD);
                    rans->did = di->dd->dev_id;
                    rans->opcode = INPSCAN_CMD_MESSAGE;
                    rans->error_no = 0;
                    struct proto_inpscan_message * buf_data = rans->data;

                    buf_data->io_did = _it->ctrld->ddn->dev_id;
                    buf_data->changedmask = res;

                    buf = tabita_cound_crc16(sizeof(struct tabita_request_ans)+sizeof(struct proto_inpscan_message), buf);

                    if (hal_send_dgram(di->kndev,
                                   di->ip_to,
                                   di->port_to,
                                   TABITA_PORT,
                                   KNET_UDP,
                                   di->kndev->tx_buf,
                                   ((struct tabita_header*)buf)->payload_len ) != 0)
                    {
                        setSysError(SYSTEM_NET_SEND_UDP_FAILED);
                    }

                    hal_net_relese_tx_buffer(di->kndev);

                    //update res
                    _it->scanres = res;
                }
            }

            ITERATE_NEXT(_it);
        }

        kernel_mutex_unlock(di->m_isc);

        kernel_process_usleep(100000, 0);
    }

    return;
}

static int8_t
inpscan_attach(struct dev_descr_node * ddn, struct dim_instance_config * din)
{
    clearSysError();
    ASSERT_NULL_ER(din, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    struct dim_instance * di = (struct dim_instance *) kernel_malloc(sizeof(struct dim_instance));
    ASSERT_NULL_ER(di, SYSTEM_MALLOC_FAILED, K_NO_DD);

    di->kndev = hal_get_knetdev(din->netdev);
    ASSERT_NULL(di->kndev, error);

    di->dd = ddn;
    di->dims = NULL;
    di->ip_to = din->ip_to;
    di->port_to = din->port_to;
    di->m_isc = kernel_mutex_init();
    di->pid = kernel_create_process(PSTR("[IO_SCAN]"), KPROC_TYPE_SYSTEM, 200, scan_proc, (void *)di);
    if (di->pid == K_NO_PID)
    {
        setSysError(SYSTEM_KERNEL_PROCESS_CREATE_FAILED);
        kernel_free((void*) di);
        return K_NO_DD;
    }

    ddn->instance = (void *) di;

    return 0;

error:
    return K_NO_DD;
}

static int8_t
inpscan_query(struct dev_descr_node * ddn, struct hal_query_info * data0)
{
    FILE * strm = (FILE *)data0->data0;
#if INPUT_SCANNER_BUILD_WITH_TEXT_QUERY == YES

    fprintf_P(strm, PSTR("INPSCAN REVISION: %u\r\n"), INPSCAN_REVISION);
    struct dim_instance * di = ddn->instance;
    if (di == NULL)
    {
        fprintf_P(strm, PSTR("Internal Error!dim_instance record missing!\r\n"));
    }
    else
    {
        fprintf_P(strm, PSTR("NETDEV [%d] SENDING TO: %"PRIu32":%"PRIu16"\r\n"),
                    di->kndev->net_dev_id, di->ip_to, di->port_to);
        ITERATE_BLOCK_DIRECT_MAN( struct dim_rec, di->dims, _it)
        {
            fprintf_P(strm,
                    PSTR("io_did [%d] scanres [%u]\r\n"),
                    _it->ctrld->ddn->dev_id,
                    _it->scanres
                    );

            ITERATE_NEXT(_it);
        }
    }

    return 0;
#else
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    fprintf_P(strm, PSTR("Inputscan was built without query! \r\n"));
    return K_MSG_ERROR;
#endif // IO_BUILD_WITH_TEXT_QUERY

}

static int8_t
inpscan_detach(struct dev_descr_node * ddn)
{
    ASSERT_NULL_ER(ddn, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    struct dim_instance * di = (struct dim_instance *) ddn->instance;

    kernel_mutex_lock(di->m_isc);
    kernel_process_suspend(di->pid);
    kernel_process_remove(di->pid);
    kernel_mutex_unlock(di->m_isc);

    ITERATE_BLOCK_DIRECT_MAN( struct dim_rec, di->dims, _it)
    {
        struct dim_rec * temp = _it;
        ITERATE_NEXT(_it);
        free((void*) temp);
    }
    free((void*) di->m_isc);
    free((void*) di);

    return 0;
}

static int8_t
inpscan_update(struct dev_descr_node * ddn, struct dim_instance_config * ios)
{
    ASSERT_NULL_ER(ios, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    ASSERT_NULL_ER(ddn->instance, SYSTEM_NODE_INSTANCE_NULL, K_NO_DD);

    return K_NO_DD;
}

static int8_t
inpscan_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    ASSERT_NULL_ER(ddn->instance, SYSTEM_NODE_INSTANCE_NULL, K_NO_DD);

    struct dim_instance * di = (struct dim_instance *) ddn->instance;

    kernel_mutex_lock(di->m_isc);

    struct dim_instance_envelope * inst = (struct dim_instance_envelope*) data0;
    inst->ip_to = di->ip_to;
    inst->port_to = di->port_to;
    inst->netdev = di->kndev->net_dev_id;

    struct dim_rec_envelope * envlp = inst->resc;
    ITERATE_BLOCK_DIRECT_MAN( struct dim_rec, di->dims, _it)
    {
        envlp->io_did = _it->ctrld->ddn->dev_id;
        envlp = envlp->next;
        ITERATE_NEXT(_it);
    }
    envlp->io_did = K_NO_DD;

    kernel_mutex_unlock(di->m_isc);

    return 0;
}

static int8_t
inpscan_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    struct dim_instance_config * cfg = (struct dim_instance_config*) data0;

    if (inpscan_attach(ddn, cfg) == 0)
    {
        //restore controls
        struct dim_rec_envelope * envlp = ((struct dim_instance_envelope*) data0)->resc;
        while (envlp->io_did != K_NO_DD)
        {
            inpscan_listen(ddn, envlp->io_did);
            envlp = envlp->next;
        }
    }
    else
    {
        return K_NO_DD;
    }

    return 0;
}

int8_t
inpscan_listen(struct dev_descr_node * ddn, t_dev_id iodid)
{
    ASSERT_NULL_ER(ddn, SYSTEM_NULL_ARGUMENT, 1);

    struct dev_descr_node * iod = hal_node_get(iodid);
    ASSERT_NULL_R(iod, 1);

    if ( (iod->e_dev_descr != DEV_DESCR_IO) || (iod->e_dh_code != ROOT_DRIVER_DEV) )
    {
        setSysError(SYSTEM_DEVICE_NOT_COMPATIABLE);
        return 1;
    }

    struct dim_rec * rec = (struct dim_rec *) kernel_malloc(sizeof(struct dim_rec));
    ASSERT_NULL_ER(rec, SYSTEM_MALLOC_FAILED, 1);

    rec->ctrld = iod->instance;
    (void)io_read8(iod, &rec->scanres);
    rec->next = NULL;

    struct dim_instance * di = (struct dim_instance *) ddn->instance;

    kernel_mutex_lock(di->m_isc);
    ADD_TO_LIST(struct dim_rec, di->dims, rec);
    kernel_mutex_unlock(di->m_isc);

    return 0;
}

int8_t
inpscan_get_mem(struct dev_descr_node * ddn, uint8_t offset, struct dim_rec * ret)
{
    ASSERT_NULL_ER(ddn, SYSTEM_NULL_ARGUMENT, 1);
    ASSERT_NULL_ER(ret, SYSTEM_NULL_ARGUMENT, 1);
    struct dim_instance * inst = (struct dim_instance *) ddn->instance;

    kernel_mutex_lock(inst->m_isc);

    struct dim_rec * temp = inst->dims;

    while ((temp != NULL) && (offset > 0))
    {
        temp = temp->next;
        offset--;
    }

    kernel_mutex_unlock(inst->m_isc);

    if (temp == NULL)
    {
        ret->next = NULL;
        ret->ctrld = NULL;
        ret->scanres = 0;
        return -1;
    }
    else
    {
        ret->ctrld = temp->ctrld;
        ret->next = NULL;
        ret->scanres = temp->scanres;
        return 0;
    }

    return 1;
}

#endif // BUILD_WITH_INPUT_SCANNER
