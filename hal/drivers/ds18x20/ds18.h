/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#ifndef DS18_H_
#define DS18_H_

#define DS18_REVISION 10

#define DS18_FLAG_ONLINE        1
#define DS18_FLAG_PARASITE      2
#define DS18_FLAG_FAM_DS18S20   4
#define DS18_FLAG_FAM_DS18B20   8
#define DS18_FLAG_FAM_DS1822    16
#define DS18_FLAG_MEASURING     32
#define DS18_FLAG_ERROR         128

#include "local/onewire.h"

typedef int8_t ds18_id;
typedef int8_t ds18_sens_id;

/*#define FOREACH_DS18_CMD(DV)\
	DV(DS18_CMD_ADD, (uint8_t) 0)\
	DV(DS18_CMD_REMOVE, (uint8_t) 1)\
	DV(DS18_CMD_SEARCH_ROM, (uint8_t) 2)\
	DV(DS18_CMD_LIST_BUS, (uint8_t) 3)\
	DV(DS18_CMD_LIST_BUS_ROMS, (uint8_t) 4)\
	DV(DS18_CMD_READ_BUS_SINGLE_ROM, (uint8_t) 5)\
	DV(DS18_CMD_READ_BUS_MULTIPLE, (uint8_t) 6)\
	DV(DS18_CMD_OPER_NOT_SUP, (uint8_t) 0xFF)

enum ds18_commands
{
	FOREACH_DS18_CMD(GENERATE_ENUM_VAL)
};*/

struct ds18_setup
{
    enum ports e_port;
    uint8_t pin_mask;
};

struct ds18_bus
{
	ds18_id id;
	enum ports e_port;
	/*uint16_t ddr;
	uint16_t port;
	uint16_t pin;
	uint8_t pin_mask;*/
	struct ow_bus_rec ow_rec;
	struct ds18_bus_dev * ow_bus;
	uint8_t ow_bus_count;
	struct kproc_mutex * mux;
};

struct ds18_bus_dev
{
	ds18_sens_id sid;
	uint8_t rom_code[OW_ROMCODE_SIZE];
	uint8_t flags;
	uint32_t result;

	struct ds18_bus_dev * next;
};

/*
 *   Standart module functions
 */
int8_t ds18x20_hal_message(struct dev_descr_node * ddn,
                          enum dd_event e_event,
                          void * data0,
                          size_t * dsize0);

int8_t ds18x20_kstream_init(struct dev_descr_node * ddn,
                           struct kstream * ks);

int8_t ds18x20_stream_init(struct dev_descr_node * ddn,
                             FILE * file);



int8_t ds18_probe_dev_supply(struct ds18_bus * ds_bus, ds18_sens_id sens_id);
uint8_t ds18_read_all_blocking(struct ds18_bus * ds_bus);
uint8_t ds18_read_blocking(struct ds18_bus * ds_bus, ds18_sens_id sens_id, uint32_t * decel);
int8_t ds18_search_sensors(struct ds18_bus * ds_bus);
int8_t ds18_rescan_bus(struct ds18_bus * ds_bus);
int8_t ds18_get_decel(struct ds18_bus * ds_bus, ds18_sens_id sens_id, uint32_t * decel);


#endif /* DS18_H_ */
