/*-
 *	Copyright (c) 2014-2016	Alexander Morozov www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"

#if BUILD_WITH_ANON_PANEL == YES

#include <avr/io.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <avr/interrupt.h>
#include <avr/sfr_defs.h>
#include <util/atomic.h>
#include <util/delay.h>
#include "../../hal.h"
#include "../io/io.h"
#include "./a_panel.h"

#include "../../../os/kernel/kernel_memory.h"

#define PHIGH 1
#define PLOW 0
#define X_POLARITY PLOW


static void anon_panel_yell(void * arg0);
static void __anon_panel_reset_xy(struct anon_panel * ap);

static int8_t anon_panel_attach(struct dev_descr_node * ddn, struct anon_panel_setup * aps);
static int8_t anon_panel_query(struct dev_descr_node * ddn, struct hal_query_info * data0);
static int8_t anon_panel_detach(struct dev_descr_node * ddn);
static int8_t anon_panel_update(struct dev_descr_node * ddn, struct anon_panel_setup * aps);
static int8_t anon_panel_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static int8_t anon_panel_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0);

static void anon_panel_yell(void * arg0)
{
    struct anon_panel * ap = (struct anon_panel *) arg0;
	uint64_t ms = 0;
	uint64_t ms_next = 0;
	uint8_t y = 0;
	uint8_t mask = 0;

    while (1)
    {
        if (ap->io_y == NULL)
        {

            if (ap->panel[0].blink_flags != 0)
            {
                // register uint64_t cur_ms = kernel_timer_get_milis();
                //if (ms <= cur_ms)
                if (ap->panel[0].ticks <= kernel_get_seconds())
                {

                    ap->panel[0].state_flags ^= ap->panel[0].blink_flags;

                    ap->panel[0].ticks = kernel_get_seconds()+1;//get_up_time()+1;
                }
            }

            io_write8_direct(ap->io_x, io_mask_prepare(ap->io_x,  ap->panel[0].state_flags));

        }
        else
        {
            // register uint64_t cur_ms = kernel_timer_get_milis();
            // ms_next = cur_ms + 100;
            //set y to 0 (low) 0b11111110))

            #if (X_POLARITY == PLOW)
            io_write8_direct(ap->io_x, io_mask_prepare(ap->io_x, 0xFF));
            #else
            io_write8_direct(ap->io_x, io_mask_prepare(ap->io_x, 0));
            #endif

            if (ap->panel[y].ticks <= kernel_get_seconds())
            {
                ap->panel[y].state_flags ^= ap->panel[y].blink_flags;
                ap->panel[y].ticks = kernel_get_seconds()+1;
            }

            //__anon_panel_reset_xy(ap);
            //_delay_us(12);   //optopair restore time (10us) 8+__anon_panel_reset_xy

            io_write8_direct(ap->io_y, ap->panel[y].y_mask); //io_mask_prepare(__it->io_y, ~io_find_mask_by_selector(__it->io_y, y+1)));
            io_write8_direct(ap->io_x, io_mask_prepare(ap->io_x, ap->panel[y].state_flags) );


            ++y;
            if (y >= ap->y_mask_len)
            {
                y = 0;
            }

        }

        _delay_us(400);

    }
}

int8_t anon_panel_hal_message(struct dev_descr_node * ddn,
                              enum dd_event e_event,
                              void * data0,
                              size_t * dsize0)
{
    int8_t ret = 0;

    switch (e_event)
    {
        case DD_EVENT_ATTACH:
            ret = anon_panel_attach(ddn, (struct anon_panel_setup *) data0);
        break;

        case DD_EVENT_DETACH:
            ret = anon_panel_detach(ddn);
        break;

        case DD_EVENT_UPDATE:
            ret = anon_panel_update(ddn, (struct anon_panel_setup *) data0);
        break;

        case DD_DUMP:
            ret = anon_panel_dump(ddn, data0, dsize0);
        break;

        case DD_RESTORE:
            ret = anon_panel_restore(ddn, data0, dsize0);
        break;

        case DD_QUERY:
            ret = anon_panel_query(ddn, (struct anon_panel_setup *) data0);
        break;

        case DD_ISRSIGNAL:
            ret = K_MSG_ERROR;
        break;

        case DD_RESET:
            ret = K_MSG_ERROR;
        break;

        case DD_DROP:
            ret = K_MSG_ERROR;
        break;

        case DD_FLUSH:
            ret = K_MSG_ERROR;
        break;
    }

    return ret;
}

int8_t anon_panel_kstream_init(struct dev_descr_node * ddn,
                           struct kstream * ks)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

int8_t anon_panel_stream_init(struct dev_descr_node * ddn,
                             FILE * file)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

static int8_t
anon_panel_attach(struct dev_descr_node * ddn, struct anon_panel_setup * aps)
{
    ASSERT_NULL_ER(aps, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    struct dev_descr_node * temp = hal_node_get(aps->io_x_id);
    ASSERT_NULL_R(temp, K_NO_DD);
    struct io_null_dev * io_x = (struct io_null_dev *) temp->instance;
    struct io_null_dev * io_y = NULL;

    if(aps->io_y_id != -1)
    {
        temp = hal_node_get(aps->io_y_id);
        io_y = (struct io_null_dev *) temp->instance;
        ASSERT_NULL_R(io_y, K_NO_DD);
        ASSERT_DATA_ER(io_y->e_io_dir != IO_PORT_DIR_OUT, SYSTEM_IO_WRONG_DIRECTION, K_NO_DD);
    }

    //check io port directions
	ASSERT_DATA_ER(io_x->e_io_dir != IO_PORT_DIR_OUT, SYSTEM_IO_WRONG_DIRECTION, K_NO_DD);

    //creating new record
	struct anon_panel * apan = (struct anon_panel *) kernel_malloc(sizeof(struct anon_panel));
	ASSERT_NULL_ER(apan, SYSTEM_MALLOC_FAILED, K_NO_DD);

	apan->ddn = ddn;
	apan->io_x = io_x;
	apan->io_y = io_y;
//	apan->rounds = 0;

    //count ones in io_* masks
	apan->x_mask_len = io_count_ones(io_x);
	if (io_y != NULL)
	{
		apan->y_mask_len = io_count_ones(io_y);
		//tot_xy = apan->x_mask_len * apan->y_mask_len; //count total
	}
	else
	{
		apan->y_mask_len = 1;
		//tot_xy = apan->x_mask_len; //count total without y
	}

	apan->panel = kernel_malloc(apan->y_mask_len * sizeof(struct a_panel_row));
	ASSERT_NULL_E(apan->panel, SYSTEM_MALLOC_FAILED, malloc_failed);

	for (uint8_t i = 0; i < apan->y_mask_len; i++)
	{
	    apan->panel[i].ticks = 0;
		apan->panel[i].blink_flags = 0; //reset values

		#if (X_POLARITY == PLOW)
		apan->panel[i].state_flags = 0xFF;	//x
		apan->panel[i].y_mask = io_mask_prepare(apan->io_y, io_find_mask_by_selector(apan->io_y, i+1));
		#else
		apan->panel[i].state_flags = 0;	//x
		apan->panel[i].y_mask = io_mask_prepare(apan->io_y, ~io_find_mask_by_selector(apan->io_y, i+1));
		#endif

	}

	//reset io

	__anon_panel_reset_xy(apan);

	//set local instance
	ddn->instance = (void*)apan;

	//create process
	apan->pid = kernel_create_process(PSTR("ANON_PANEL_YELL"), KPROC_TYPE_SYSTEM, 45, anon_panel_yell, (void*)apan);
	if (apan->pid == K_NO_PID)
    {
        //create of the process failed!
        setSysError(SYSTEM_KERNEL_PROCESS_CREATE_FAILED);
        goto proc_create_failed;
    }

    return 0;

proc_create_failed:
    kernel_free((void*) apan->panel);
    ddn->instance = NULL;
malloc_failed:
    kernel_free((void*) apan);
    return K_NO_DD;
}

static int8_t
anon_panel_query(struct dev_descr_node * ddn, struct hal_query_info * data0)
{
 FILE * strm = (FILE *)data0->data0;
#if ANON_PANEL_BUILD_WITH_TEXT_QUERY == YES

    fprintf_P(strm, PSTR("ANAUNCTUATOR PANEL 8-bit REVISION %d\r\n"), ANON_PANEL_REVISION);

    struct anon_panel * ap = (struct anon_panel *) ddn->instance;

    fprintf_P(strm, PSTR("Linked to x->%d y->%d\r\n"), ap->io_x->ddn->dev_id, ap->io_y->ddn->dev_id);
    fprintf_P(strm, PSTR("x-> %x:%x y->%x:%x\r\n"), ap->io_x->e_port, ap->io_x->bit_mask,
                                                    ap->io_y->e_port, ap->io_y->bit_mask);

    for (uint8_t i = 0; i < ap->y_mask_len; i++)
	{
        fprintf_P(strm, PSTR("row: %d state: %x blink: %x mask: %x\r\n"), i, ap->panel[i].state_flags, ap->panel[i].blink_flags, ap->panel[i].y_mask);
	}

	return 0;
#else
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    fprintf_P(strm, PSTR("Anon panel was built without query! \r\n"));
    return K_MSG_ERROR;
#endif
}

static int8_t
anon_panel_detach(struct dev_descr_node * ddn)
{
    struct anon_panel * ap = (struct anon_panel *) ddn->instance;

    kernel_process_suspend(ap->pid);
    kernel_process_remove(ap->pid);
    __anon_panel_reset_xy(ap);

    kernel_free((void*)ap->panel);
    kernel_free((void*)ap);

    return 0;
}

static int8_t
anon_panel_update(struct dev_descr_node * ddn, struct anon_panel_setup * aps)
{
    ASSERT_NULL_ER(aps, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    struct anon_panel * ap = (struct anon_panel *) ddn->instance;

    //update io x y
    struct dev_descr_node * temp = hal_node_get(aps->io_x_id);
    ASSERT_NULL_R(temp, K_NO_DD);
    struct io_null_dev * io_x = (struct io_null_dev *) temp->instance;
    struct io_null_dev * io_y = NULL;


    if(aps->io_y_id != -1)
    {
        temp = hal_node_get(aps->io_y_id);
        io_y = (struct io_null_dev *) temp->instance;
        ASSERT_NULL_R(io_y, K_NO_DD);
        ASSERT_DATA_ER(io_y->e_io_dir != IO_PORT_DIR_OUT, SYSTEM_IO_WRONG_DIRECTION, K_NO_DD);
    }

    //check io port directions
	ASSERT_DATA_ER(io_x->e_io_dir != IO_PORT_DIR_OUT, SYSTEM_IO_WRONG_DIRECTION, K_NO_DD);

    kernel_process_suspend(ap->pid);

	__anon_panel_reset_xy(ap);

	ap->io_x = io_x;
	ap->io_y = io_y;

	//count ones in io_* masks
	ap->x_mask_len = io_count_ones(io_x);
	if (io_y != NULL)
	{
		ap->y_mask_len = io_count_ones(io_y);
		//tot_xy = apan->x_mask_len * apan->y_mask_len; //count total
	}
	else
	{
		ap->y_mask_len = 1;
		//tot_xy = apan->x_mask_len; //count total without y
	}

	kernel_process_restart(ap->pid, anon_panel_yell, (void*)ap);

    return 0;
}

static int8_t
anon_panel_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    struct anon_panel_setup * aps = (struct anon_panel_setup*) data0;
    struct anon_panel * ap = (struct anon_panel *) ddn->instance;

    aps->io_x_id = ap->io_x->ddn->dev_id;
    aps->io_y_id = ap->io_y->ddn->dev_id;

    return 0;
}

static int8_t
anon_panel_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    return anon_panel_attach(ddn, (struct anon_panel_setup*) data0);
}

static void __anon_panel_reset_xy(struct anon_panel * ap)
{
    if (ap->io_y != NULL)
	{
		#if (X_POLARITY == PLOW)
		io_write8_direct(ap->io_y, io_mask_prepare(ap->io_y, 0));
		#else
		io_write8_direct(ap->io_y, io_mask_prepare(ap->io_y, 0xFF));
		#endif
	}

	#if (X_POLARITY == PLOW)
	io_write8_direct(ap->io_x, io_mask_prepare(ap->io_x, 0xFF));
	#else
	io_write8_direct(ap->io_x, io_mask_prepare(ap->io_x, 0));
	#endif
}

int8_t a_panel_turn_on_single(struct dev_descr_node * node, uint8_t cell_id)
{
    ASSERT_NULL_ER(node, SYSTEM_NULL_ARGUMENT, 1);
	struct anon_panel * ap = (struct anon_panel *)node->instance;

	uint8_t tot = ap->x_mask_len * ap->y_mask_len;
	ASSERT_LEN(tot, cell_id, a_panel_turn_on_single_incorrect_cell);

	//turn blink flag off
	ap->panel[a_panel_get_row(ap->x_mask_len, cell_id)].blink_flags &= ~(1 << a_panel_get_pin(ap->x_mask_len, cell_id));
	//convert cell_id to mask

	#if (X_POLARITY == PLOW)
	ap->panel[a_panel_get_row(ap->x_mask_len, cell_id)].state_flags &= ~(1 << a_panel_get_pin(ap->x_mask_len, cell_id));
	#else
	ap->panel[a_panel_get_row(ap->x_mask_len, cell_id)].state_flags |= (1 << a_panel_get_pin(ap->x_mask_len, cell_id));
	#endif
	return 0;

a_panel_turn_on_single_incorrect_cell:
	setSysError(SYSTEM_LENGTH_ASSERT);
	return 1;
}

int8_t a_panel_turn_off_single(struct dev_descr_node * node, uint8_t cell_id)
{
    ASSERT_NULL_ER(node, SYSTEM_NULL_ARGUMENT, 1);
	struct anon_panel * ap = (struct anon_panel *)node->instance;

	uint8_t tot = ap->x_mask_len * ap->y_mask_len;
	ASSERT_LEN(tot, cell_id, error_cell);

	uint8_t aprow = a_panel_get_row(ap->x_mask_len, cell_id);
	uint8_t appin = a_panel_get_pin(ap->x_mask_len, cell_id);

	ap->panel[aprow].blink_flags &= ~(1 << appin);

	//convert cell_id to mask
	#if (X_POLARITY == PLOW)
	ap->panel[aprow].state_flags |= (1 << appin);
	#else
	ap->panel[aprow].state_flags &= ~(1 << appin);
	#endif
	return 0;

error_cell:
	setSysError(SYSTEM_LENGTH_ASSERT);
	return 1;

error:
	return 1;
}

int8_t a_panel_turn_blink_single(struct dev_descr_node * node, uint8_t cell_id)
{
	ASSERT_NULL_ER(node, SYSTEM_NULL_ARGUMENT, 1);
	struct anon_panel * ap = (struct anon_panel *)node->instance;

	uint8_t tot = ap->x_mask_len * ap->y_mask_len;
	ASSERT_LEN(tot, cell_id, error_cell);

	ap->panel[a_panel_get_row(ap->x_mask_len, cell_id)].blink_flags |= (1 << a_panel_get_pin(ap->x_mask_len, cell_id));
	//validate and set
	/*switch (flags)
	{
		case A_PAN_FLAG_BLINK_STAT_SLOW:
			ap->panel[a_panel_get_row(ap->x_mask_len, cell_id)].blink_flags |= (1 << a_panel_get_pin(ap->x_mask_len, cell_id));
		break;

		case A_PAN_FLAG_BLINK_STAT_FAST:
			//ap->panel[cell_id].status = A_PAN_FLAG_BLINK_STAT_FAST;
		break;

		default:
			setSysError(SYSTEM_UNKNOWN_OPERATION);
			return 1; //<-- leave
	}*/

	return 0;

error_cell:
	setSysError(SYSTEM_LENGTH_ASSERT);
	return 1;

error:
	return 1;
}

inline uint8_t a_panel_get_row(uint8_t mask_len, uint8_t cell_id)
{
	return (uint8_t) cell_id / mask_len;
}

inline uint8_t a_panel_get_pin(uint8_t mask_len, uint8_t cell_id)
{
	return (uint8_t) cell_id % mask_len;
}

#endif // BUILD_WITH_ANON_PANEL
