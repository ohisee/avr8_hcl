/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef A_PANEL_H_
#define A_PANEL_H_

#define ANON_PANEL_REVISION 2

#define A_PAN_FLAG_POWER_STAT 0b10000000
#define A_PAN_FLAG_BLINK_STAT_SLOW 0b01000000
#define A_PAN_FLAG_BLINK_STAT_FAST 0b11000000

#define A_PAN_BLINK_SECONDS_SLOW 2
#define A_PAN_BLINK_SECONDS_FAST 1

#define FOREACH_A_PAN_ACT(DV)\
	DV(A_PAN_ACT_ON, (uint8_t) 0)\
	DV(A_PAN_ACT_OFF, (uint8_t) 1)\
	DV(A_PAN_ACT_BLINK, (uint8_t) 2)

#include "../../../os/kernel/kernel_process.h"

enum a_pan_acts
{
	FOREACH_A_PAN_ACT(GENERATE_ENUM_VAL)
};

struct a_panel_row
{
    uint32_t ticks;         //ticks left
	uint8_t blink_flags;    //turn on off blink
	uint8_t state_flags;    //on off
	uint8_t y_mask;
};

struct anon_panel
{
	struct dev_descr_node * ddn;
	struct io_null_dev * io_x;
	struct io_null_dev * io_y;
	uint8_t x_mask_len;
	uint8_t y_mask_len;
	struct a_panel_row * panel; //pointer to the array
	k_pid pid;
};

struct anon_panel_setup
{
    t_dev_id io_x_id;
    t_dev_id io_y_id;
};

int8_t anon_panel_hal_message(struct dev_descr_node * ddn,
                              enum dd_event e_event,
                              void * data0,
                              size_t * dsize0);
int8_t anon_panel_kstream_init(struct dev_descr_node * ddn, struct kstream * ks);
int8_t anon_panel_stream_init(struct dev_descr_node * ddn, FILE * file);


int8_t a_panel_turn_on_single(struct dev_descr_node * node, uint8_t cell_id);
int8_t a_panel_turn_off_single(struct dev_descr_node * node, uint8_t cell_id);
int8_t a_panel_turn_blink_single(struct dev_descr_node * node, uint8_t cell_id);

uint8_t a_panel_get_row(uint8_t mask_len, uint8_t cell_id);
uint8_t a_panel_get_pin(uint8_t mask_len, uint8_t cell_id);
#endif /* A_PANEL_H_ */
