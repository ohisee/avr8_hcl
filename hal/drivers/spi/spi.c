/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD.ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"
#include <avr/io.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay_basic.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include "../../hal.h"
#include "../../lib/stream/stream.h"
#include "spi.h"

#include "../../../os/kernel/kernel_process.h"
#include "../../../os/kernel/kernel_mutex.h"
#include "../../../os/kernel/kernel_isr.h"

#if BUILD_WITH_SPI == YES
static int8_t spi_attach(struct dev_descr_node * ddn, struct spi_setup * setup);
static int8_t spi_detach(struct dev_descr_node * ddn);
static int8_t spi_query(struct dev_descr_node * ddn, struct hal_query_info * data0);
static int8_t spi_update(struct dev_descr_node * ddn, struct spi_setup * setup);
static int8_t spi_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static int8_t spi_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0);

static struct spi_chip_select_pins * spi_get_cs_by_stream(struct kstream * stream);
static struct spi_chip_select_pins * spi_get_cs(struct spi_dev * s_spi, cs_id csid);
static void spi_force_drop_session(struct spi_dev * s_spi);
static int8_t spi_static_write(struct kstream *stream, uint8_t var);
static int16_t spi_static_read(struct kstream *stream);
static int16_t spi_static_readlen(struct kstream *stream);
static int16_t spi_static_writelen(struct kstream *stream);
static void driver_spi_setBitOrder(struct spi_dev * s_spi, enum spi_bit_order bitOrder);
static void driver_spi_setDataMode(struct spi_dev * s_spi, enum spi_data_mode mode);
static void driver_spi_setClockDivider(struct spi_dev * s_spi, enum spi_clock rate);

struct spi_dev * volatile spi_devs[MAX_SPI_PORTS] = {[ 0 ... MAX_SPI_PORTS-1 ] = NULL};

int8_t spi_hal_message(struct dev_descr_node * ddn,
                        enum dd_event e_event,
                        void * data0,
                        size_t * dsize0)
{
    int8_t ret = 0;

    switch (e_event)
    {
        case DD_EVENT_ATTACH:
            ret = spi_attach(ddn, (struct spi_setup *) data0);
        break;

        case DD_EVENT_DETACH:
            ret = spi_detach(ddn);
        break;

        case DD_EVENT_UPDATE:
            ret = spi_update(ddn, (struct spi_setup *) data0);
        break;

        case DD_DUMP:
            ret = spi_dump(ddn, data0, dsize0);
        break;

        case DD_RESTORE:
            ret = spi_restore(ddn, data0, dsize0);
        break;

        case DD_QUERY:
            ret = spi_query(ddn, (struct hal_query_info *) data0);
        break;

        case DD_ISRSIGNAL:
            ret = K_MSG_ERROR;
        break;

        case DD_RESET:
            ret = K_MSG_ERROR;
        break;

        case DD_DROP:
            ret = K_MSG_ERROR;
        break;

        case DD_FLUSH:
            ret = K_MSG_ERROR;
        break;
    }

    return ret;
}

int8_t spi_kstream_init(struct dev_descr_node * ddn, struct kstream * ks)
{
    /*
    ks->fid_locked = K_NO_FD;

    ks->KSWRITE = spi_static_write;
    ks->KSWINIT = NULL;
    ks->KSWACK  = NULL;
    ks->KSWLEN  = spi_static_writelen;

    ks->KSREAD = spi_static_read;
    ks->KSRLEN = spi_static_readlen;

    ks->KSDROP = NULL;
    ks->KSFLUSH = NULL;

    ks->flags &= SSYNC;

	return 0;
    */
    return K_FAIL_STREAM;
}

int8_t
spi_stream_init(struct dev_descr_node * ddn, FILE * file)
{
    return K_FAIL_STREAM;
}


static int8_t
spi_attach(struct dev_descr_node * ddn, struct spi_setup * setup)
{
	ASSERT_NULL_ER(ddn, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    ASSERT_NULL_ER(setup, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    if (spi_devs[setup->e_dev] != NULL)
    {
        setSysError(SYSTEM_DEVICE_ALREADY_INITED);
        return K_NO_DD;
    }

	//check if pins available
	enum ports e_port = pgm_read_byte(&(spi_pins[(uint8_t) setup->e_dev].e_dev_port));
	uint8_t pins = pgm_read_byte(&(spi_pins[(uint8_t) setup->e_dev].pins));

	uint8_t ret = hal_hw_bind_pin(ddn->dev_id, e_port, pins);
	ASSERT_DATA(ret == 1, goto pin_busy);

	//create spi struct
	struct spi_dev * s_spi = (struct spi_dev *) kernel_malloc(sizeof(struct spi_dev));
	ASSERT_NULL(s_spi, malloc_failed);

    memset(s_spi, 0, sizeof(struct spi_dev));

	//setting spi_struct
	s_spi->e_bit_order = setup->e_bit_order;
	s_spi->e_clock = setup->e_clock;
	s_spi->e_data_mode = setup->e_data_mode;
	s_spi->e_mode = setup->e_mode;
	s_spi->cs_list = NULL;
	s_spi->e_dev = setup->e_dev;
	s_spi->cs_locked = NULL;
	s_spi->ddn = ddn;
	s_spi->mtx_spi = kernel_mutex_init();

	uint16_t p_ddr = hal_hw_get_addr_ddr(e_port);
	uint16_t p_port = hal_hw_get_addr_port(e_port);

	const struct spi_pins_nums * pgm_dprec = &s_spi_pins[(uint8_t) setup->e_dev];
        uint8_t sck = pgm_read_word( &(pgm_dprec->sck));
        uint8_t miso = pgm_read_word( &(pgm_dprec->miso));
        uint8_t mosi = pgm_read_word( &(pgm_dprec->mosi));
        uint8_t ss = pgm_read_word( &(pgm_dprec->ss));

	const struct spi_hwaddr * dev_hw_ptr = &s_spi_addr[(uint8_t) setup->e_dev];

	//initialize hardware
	if (s_spi->e_mode == SPI_MASTER)
	{
		//_SFR_IO8(p_ddr) |= (1 << SPI_PORT_SCK) | (1 << SPI_PORT_MOSI);
		//_SFR_IO8(p_ddr) &= ~(1 << SPI_PORT_MISO);
		//_SFR_IO8(p_port) &= ~(1 << SPI_PORT_SCK) & ~(1 << SPI_PORT_MOSI) & ~(1 << SPI_PORT_MISO);
        kernel_mutex_lock(mtx_giant_io);
		_HAL_WREG8(p_ddr, (_HAL_RREG8(p_ddr) | (1 << sck) | (1 << mosi) | (1 << ss)));
		_HAL_WREG8(p_ddr, (_HAL_RREG8(p_ddr) & ~(1 << miso)));
		_HAL_WREG8(p_port, (_HAL_RREG8(p_port) | (1 << sck) | (1 << mosi)));
		_HAL_WREG8(p_port, (_HAL_RREG8(p_port) & ~(1 << miso)));
        kernel_mutex_unlock(mtx_giant_io);

		driver_spi_setBitOrder(s_spi, s_spi->e_bit_order);
		driver_spi_setDataMode(s_spi, s_spi->e_data_mode);
		driver_spi_setClockDivider(s_spi, s_spi->e_clock);

		// Enable SPI Master
        _HAL_WREG8( pgm_read_word( &(dev_hw_ptr->spcr)), ( (1 << MSTR) | (1 << SPE) ) );
		//SPCR |= (1 << MSTR) | (1 << SPE);
#if DEBUG == YES
		fprintf_P(stdout, PSTR("DEBUG: master SPI p:%d SPCR:%d SPSR:%d\r\n"), e_port, SPCR, SPSR);
#endif // DEBUG

	}
	else
	{
		//_SFR_IO8(p_ddr) &= /*~(1 << SPI_PORT_SS) &*/ ~(1 << SPI_PORT_SCK) & ~(1 << SPI_PORT_MOSI);
		//_SFR_IO8(p_ddr) |= (1 << SPI_PORT_MISO);
		//_SFR_IO8(p_port) &= ~(1 << SPI_PORT_SCK) & ~(1 << SPI_PORT_MOSI) & ~(1 << SPI_PORT_MISO);
        kernel_mutex_lock(mtx_giant_io);
        _HAL_WREG8(p_ddr, _HAL_RREG8(p_ddr) & ~(1 << sck) & ~(1 << mosi));
        _HAL_WREG8(p_ddr, _HAL_RREG8(p_ddr) | (1 << miso));
        _HAL_WREG8(p_port, _HAL_RREG8(p_port) & ~(1 << sck) & ~(1 << mosi) & ~(1 << miso));
        kernel_mutex_unlock(mtx_giant_io);

		driver_spi_setBitOrder(s_spi, s_spi->e_bit_order);
		driver_spi_setDataMode(s_spi, s_spi->e_data_mode);
		// clock rate is ignored in slave mode

		// Enable SPI Slave
        _HAL_WREG8( pgm_read_word( &(dev_hw_ptr->spcr)),  (1 << SPE) );
		//SPCR |= (1 << SPE);
	}

	ddn->instance = (void *) s_spi;

	return 0;

malloc_failed:
	//unbind pins
	hal_hw_unbind_pin(ddn->dev_id, e_port, pins);
	setSysError(SYSTEM_MALLOC_FAILED);
	return 1;

pin_busy:
	setSysError(SYSTEM_HW_PIN_BUSY);
	return 1;
}

static int8_t
spi_detach(struct dev_descr_node * ddn)
{
    struct spi_dev * s_spi = (struct spi_dev *) ddn->instance;

    ASSERT_NULL_ER(s_spi, SYSTEM_NODE_INSTANCE_NULL, K_NO_DD);

    if (s_spi->cs_locked != NULL)
    {
        //device is busy, for now detaching anyway
    }

    ITERATE_BLOCK_DIRECT_MAN(struct spi_chip_select_pins, s_spi->cs_list, __it)
    {
        struct spi_chip_select_pins * cso = __it;

        _HAL_WREG8(hal_hw_get_addr_port(cso->e_port), _HAL_RREG8(hal_hw_get_addr_port(cso->e_port)) & ~(cso->bit_mask));
        _HAL_WREG8(hal_hw_get_addr_ddr(cso->e_port), _HAL_RREG8(hal_hw_get_addr_ddr(cso->e_port)) & ~(cso->bit_mask));

        //removed from list, free pin
        hal_hw_unbind_pin(ddn->dev_id, cso->e_port, cso->bit_mask);

        ITERATE_NEXT(__it);
        cso->next = NULL;

        kernel_free((void *) cso);
    }

    //turn off spi and set io back
    const struct spi_hwaddr * dev_hw_ptr = &s_spi_addr[(uint8_t) s_spi->e_dev];
    _HAL_WREG8( pgm_read_word( &(dev_hw_ptr->spcr)),  0 );
    _HAL_WREG8( pgm_read_word( &(dev_hw_ptr->spsr)),  0 );

    //remove self info
    enum ports e_port = pgm_read_byte(&(spi_pins[(uint8_t) s_spi->e_dev].e_dev_port));
    uint8_t pins = pgm_read_byte(&(spi_pins[(uint8_t) s_spi->e_dev].pins));

    uint16_t p_ddr = hal_hw_get_addr_ddr(e_port);
    uint16_t p_port = hal_hw_get_addr_port(e_port);
    //_SFR_IO8(p_ddr) &= ~(1 << SPI_PORT_SCK) & ~(1 << SPI_PORT_MOSI);
    //_SFR_IO8(p_ddr) &= ~(1 << SPI_PORT_MISO);
    //_SFR_IO8(p_port) &= ~(1 << SPI_PORT_SCK) & ~(1 << SPI_PORT_MOSI) & ~(1 << SPI_PORT_MISO);

    /*_KRN_WREG8(p_ddr, _KRN_RREG8(p_ddr) & ~(1 << SPI_PORT_SCK) & ~(1 << SPI_PORT_MOSI));
    _KRN_WREG8(p_ddr, _KRN_RREG8(p_ddr) & ~(1 << SPI_PORT_MISO));
    _KRN_WREG8(p_port, _KRN_RREG8(p_port) & ~(1 << SPI_PORT_SCK) & ~(1 << SPI_PORT_MOSI) & ~(1 << SPI_PORT_MISO));
    */
    _HAL_WREG8(p_ddr, _HAL_RREG8(p_ddr) & ~(pins));
    _HAL_WREG8(p_port, _HAL_RREG8(p_port) & ~(pins));

    //free pins
    hal_hw_unbind_dd_all_pin(ddn->dev_id);

    //free struct
    s_spi->cs_list = NULL;
    spi_devs[s_spi->e_dev] = NULL;
    kernel_free((void *) s_spi);

    return 0;
}

static int8_t
spi_query(struct dev_descr_node * ddn, struct hal_query_info * data0)
{
    FILE * strm = (FILE *)data0->data0;
#if SPI_BUILD_WITH_TEXT_QUERY == YES
    fprintf_P(strm, PSTR("SPI REVISION: %u\r\n"), SPI_REVISION);
    if (ddn->instance == NULL)
    {
        fprintf_P(strm, PSTR("SPI :/ BUS NOT INITIALIZED!"));
        return 0;
    }
    struct spi_dev * s_spi = (struct spi_dev *) ddn->instance;
    fprintf_P(strm, PSTR("MODE %i CLOCK_DIV: %i BIT_ORD: %i DATA_MODE: %i\r\n"), s_spi->e_mode, s_spi->e_clock, s_spi->e_bit_order, s_spi->e_data_mode);
    fprintf_P(strm, PSTR("SLAVE DEVICES: \r\n"));
    if (s_spi->cs_list == NULL)
    {
        fprintf_P(strm, PSTR("SPI :/ NO SLAVE DEVS ADDED YET!\r\n"));
        return 0;
    }

    ITERATE_BLOCK_DIRECT_MAN(struct spi_chip_select_pins, s_spi->cs_list, __it_s)
    {

        fprintf_P(strm, PSTR("ID: %d PORT: %d BIT_MASK: %d\r\n"), __it_s->csid, __it_s->e_port, __it_s->bit_mask);

        ITERATE_NEXT(__it_s);
    }

    if (s_spi->cs_locked)
    {
        fprintf_P(strm, PSTR("ID: %d PORT: %d BIT_MASK: %d\r\n"), s_spi->cs_locked->csid, s_spi->cs_locked->e_port, s_spi->cs_locked->bit_mask);
    }
    else
    {
        fprintf_P(strm, PSTR("SPI no chip selected\r\n"));
    }

#else
    fprintf_P(strm, PSTR("SERIAL was built without query! \r\n"));
#endif // SPI_BUILD_WITH_TEXT_QUERY
    return 0;
}

static int8_t
spi_update(struct dev_descr_node * ddn, struct spi_setup * setup)
{
    ASSERT_NULL_ER(ddn, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    ASSERT_NULL_ER(setup, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    ASSERT_DATA(setup->e_dev >= MAX_SPI_PORTS, setSysError(SYSTEM_UNKNOWN_ID_OF_IO_HW); goto error_leave);

    if (spi_devs[setup->e_dev] != NULL)
    {
        setSysError(SYSTEM_DEVICE_NOT_INITED);
		return K_NO_DD;
    }

    struct spi_dev * s_spi = (struct spi_dev *) ddn->instance;

    //reset hw
	s_spi->e_bit_order = setup->e_bit_order;
	s_spi->e_clock = setup->e_clock;
	s_spi->e_data_mode = setup->e_data_mode;
	s_spi->e_mode = setup->e_mode;
	s_spi->e_dev = setup->e_dev;

    enum ports e_port = pgm_read_byte(&(spi_pins[(uint8_t) s_spi->e_dev].e_dev_port));
    uint16_t p_ddr = hal_hw_get_addr_ddr(e_port);
	uint16_t p_port = hal_hw_get_addr_port(e_port);

	const struct spi_hwaddr * dev_hw_ptr = &s_spi_addr[(uint8_t) setup->e_dev];

	//initialize hardware
    if (setup->e_mode == SPI_MASTER)
    {
        driver_spi_setBitOrder(s_spi, setup->e_bit_order);
        driver_spi_setDataMode(s_spi, setup->e_data_mode);
        driver_spi_setClockDivider(s_spi, setup->e_clock);

        // Enable SPI Master
        _HAL_WREG8( pgm_read_word( &(dev_hw_ptr->spcr)), 0 );
        _HAL_WREG8( pgm_read_word( &(dev_hw_ptr->spcr)), ( (1 << MSTR) | (1 << SPE) ) );
    }
    else
    {
        driver_spi_setBitOrder(s_spi, setup->e_bit_order);
        driver_spi_setDataMode(s_spi, setup->e_data_mode);
        // clock rate is ignored in slave mode

        // Enable SPI Slave
        _HAL_WREG8( pgm_read_word( &(dev_hw_ptr->spcr)),  (1 << SPE) );
    }

return 0;

error_leave:
	return K_NO_DD;
}

static int8_t
spi_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    struct spi_dev_envelope * envlp = (struct spi_dev_envelope*) data0;
    struct spi_dev * spi = (struct spi_dev*) ddn->instance;

    envlp->e_dev = spi->e_dev;
    envlp->e_mode = spi->e_mode;
    envlp->e_clock = spi->e_clock;
    envlp->e_bit_order = spi->e_bit_order;
    envlp->e_data_mode = spi->e_data_mode;

    return 0;
}

static int8_t
spi_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    return spi_attach(ddn, (struct spi_setup *) data0);
}

cs_id
spi_add_slave(struct spi_dev * s_spi,
              enum ports e_port,
              uint8_t bit_mask,
              uint8_t flags)
{
	ASSERT_NULL_ER(s_spi, SYSTEM_NULL_ARGUMENT, -1);

	//try to alocate pin
	uint8_t ret = hal_hw_bind_pin(s_spi->ddn->dev_id, e_port, bit_mask );
	ASSERT_DATA(ret == 1, goto pin_busy);

	struct spi_chip_select_pins * csr = (struct spi_chip_select_pins *) kernel_malloc(sizeof(struct spi_chip_select_pins));
	ASSERT_NULL(csr, malloc_failed);

    memset(csr, 0, sizeof(struct spi_chip_select_pins));

	csr->bit_mask = bit_mask;
	csr->e_port = e_port;
	csr->next = NULL;

	//add to list
	uint8_t sel_spi_id = 0;
	if (s_spi->cs_list == NULL)
	{
		s_spi->cs_list = csr;
	}
	else
	{
		ADD_TO_LIST_MANUAL(struct spi_chip_select_pins, s_spi->cs_list)
		{

			IF_LAST_ELEMENT
			{
				sel_spi_id = PREV_IT->csid + 1;
				INJECT_ELEMENT_AFTER_PREV_IT(csr);
				FINISH_SORTING;
			}

			COMPARE_DIFF_DIR(csid, 2)
			{
				sel_spi_id = CURT_IT->csid - (CURT_IT->csid - PREV_IT->csid);
				INJECT_ELEMENT_BEFORE_IT(csr, s_spi->cs_list);
				FINISH_SORTING;
			}

			NEXT_ELEMENT;

		}
	}
	csr->csid = sel_spi_id;

	//modify hardware state
	//_SFR_IO8(kernel_hw_get_addr_ddr(csr->e_port)) |= csr->bit_mask; //set ddr output
	//_SFR_IO8(kernel_hw_get_addr_port(csr->e_port)) |= csr->bit_mask; //set high

    kernel_mutex_lock(mtx_giant_io);
    _HAL_WREG8(hal_hw_get_addr_ddr(csr->e_port), _HAL_RREG8(hal_hw_get_addr_ddr(csr->e_port)) | csr->bit_mask);
    _HAL_WREG8(hal_hw_get_addr_port(csr->e_port), _HAL_RREG8(hal_hw_get_addr_port(csr->e_port)) | csr->bit_mask);
    kernel_mutex_unlock(mtx_giant_io);

	return csr->csid;

pin_busy:
	return -1;

malloc_failed:
	setSysError(SYSTEM_MALLOC_FAILED);
	return -1;
}

int8_t
spi_remove_slave(struct spi_dev * s_spi, cs_id csid)
{
	ASSERT_NULL_ER(s_spi, SYSTEM_NULL_ARGUMENT, 1);
	ASSERT_DATA(csid < 0, goto slave_bad_id);

	REMOVE_FROM_LIST_2(struct spi_chip_select_pins, s_spi->cs_list, cso, cso->csid == csid, slave_not_found);

	//modify hardware state
	//_SFR_IO8(kernel_hw_get_addr_port(cso->e_port)) &= ~(cso->bit_mask); //set high
	//_SFR_IO8(kernel_hw_get_addr_ddr(cso->e_port)) &= ~(cso->bit_mask); //set ddr output

    kernel_mutex_lock(mtx_giant_io);

    _HAL_WREG8(hal_hw_get_addr_port(cso->e_port), _HAL_RREG8(hal_hw_get_addr_port(cso->e_port)) & ~(cso->bit_mask));
    _HAL_WREG8(hal_hw_get_addr_ddr(cso->e_port), _HAL_RREG8(hal_hw_get_addr_ddr(cso->e_port)) & ~(cso->bit_mask));

    kernel_mutex_unlock(mtx_giant_io);

	//removed from list, free pin
	hal_hw_unbind_pin(s_spi->ddn->dev_id, cso->e_port, cso->bit_mask);

	kernel_free((void *) cso);

	return 0;

slave_bad_id:
	setSysError(SYSTEM_ID_INCORRECT);
	return 1;

slave_not_found:
	setSysError(SYSTEM_ITEM_NOT_FOUND);
	return 1;
}

static struct
spi_chip_select_pins * spi_get_cs_by_stream(struct kstream * stream)
{
	ASSERT_NULL(stream, spi_get_cs_by_stream_null_arg);
	//ASSERT_NULL(s_spi, spi_get_cs_by_stream_dev_not_init);

	//SEARCH_IN_LIST_2(struct spi_chip_select_pins, s_spi->cs_list, csr, csr->stream == stream, spi_get_cs_by_stream_not_found);

	return NULL;

spi_get_cs_by_stream_not_found:
	setSysError(SYSTEM_ITEM_NOT_FOUND);
	return NULL;

spi_get_cs_by_stream_dev_not_init:
	setSysError(SYSTEM_NULL_DEV_NOT_INITED);
	return NULL;

spi_get_cs_by_stream_null_arg:
	setSysError(SYSTEM_NULL_ARGUMENT);
	return NULL;
}

static struct
spi_chip_select_pins * spi_get_cs(struct spi_dev * s_spi, cs_id csid)
{

	SEARCH_IN_LIST_2(struct spi_chip_select_pins, s_spi->cs_list, csr, csr->csid == csid, spi_get_cs_not_found);

	return csr;

spi_get_cs_not_found:
	setSysError(SYSTEM_ITEM_NOT_FOUND);
	return NULL;
}

// default is MASTER
static void driver_spi_begin_default()
{
	//driver_spi_begin(SPI_MASTER, MSBFIRST, SPI_MODE3, SPI_CLOCK_DIV4);
}


static void driver_spi_end() {
	//SPCR &= ~(1 << SPE);
}

uint8_t spi_run_session(struct spi_dev * s_spi, cs_id csid)
{
	ASSERT_NULL_ER(s_spi, SYSTEM_NULL_ARGUMENT, 1);
   // printf_P(PSTR("sess locking own:%d cw:%u %p\r\n"), kernel_mutex_get_cnt(s_spi->mtx_spi), kernel_mutex_get_cwating(s_spi->mtx_spi), s_spi->mtx_spi);
	kernel_mutex_lock(s_spi->mtx_spi);
//printf_P(PSTR("locked locking\r\n"));
	if (s_spi->cs_locked != NULL)
	{
	    //kernel_mutex_unlock(s_spi->mtx_spi);
		setSysError(SYSTEM_SPI_SESSION_BUSY); //debug
		while (s_spi->cs_locked != NULL);
		//kernel_mutex_lock(s_spi->mtx_spi);
	}

	s_spi->cs_locked = spi_get_cs(s_spi, csid);
	ASSERT_NULL_E(s_spi->cs_locked, SYSTEM_ID_INCORRECT, error);
	//set pin(s) low level
	//_SFR_IO8(kernel_hw_get_addr_port(spi_session_lock->e_port)) &= ~(spi_session_lock->bit_mask); //pull down chip select (slave select)

    kernel_mutex_lock(mtx_giant_io);
    _HAL_WREG8(hal_hw_get_addr_port(s_spi->cs_locked->e_port),
               _HAL_RREG8(hal_hw_get_addr_port(s_spi->cs_locked->e_port)) & ~(s_spi->cs_locked->bit_mask));
    kernel_mutex_unlock(mtx_giant_io);

   // kernel_mutex_unlock(s_spi->mtx_spi);
    //printf_P(PSTR("SESSION LOCKED\r\n"));
	return 0;
error:
    kernel_mutex_unlock(s_spi->mtx_spi);
    return 1;
}

static void
spi_force_drop_session(struct spi_dev * s_spi)
{
	//_SFR_IO8(kernel_hw_get_addr_port(spi_session_lock->e_port)) |= (spi_session_lock->bit_mask); //pull up chip select (slave select)
	/*kernel_mutex_lock(mtx_giant_io);
	_HAL_WREG8(hal_hw_get_addr_port(s_spi->cs_locked->e_port), _HAL_RREG8(hal_hw_get_addr_port(s_spi->cs_locked->e_port)) | (s_spi->cs_locked->bit_mask));
	kernel_mutex_unlock(mtx_giant_io);

    kernel_mutex_lock(s_spi->mtx_spi);
	s_spi->cs_locked = NULL;
    kernel_mutex_unlock(s_spi->mtx_spi);*/
    //this fuction can't unlock session because mutex was locked by other process.
    //ToDo add function kernel_mutex_forceunlock(mux) to OS

	return;
}

int8_t
spi_finish_session(struct spi_dev * s_spi, cs_id csid)
{
	ASSERT_NULL_ER(s_spi, SYSTEM_NULL_ARGUMENT, 1);

	//kernel_mutex_lock(s_spi->mtx_spi);

	ASSERT_NULL_E(s_spi->cs_locked, SYSTEM_DEV_HW_NOT_LOCKED, unlock_failed);
	ASSERT_DATA(csid != s_spi->cs_locked->csid, goto unlock_failed);

	//_SFR_IO8(kernel_hw_get_addr_port(spi_session_lock->e_port)) |= (spi_session_lock->bit_mask); //pull up chip select (slave select)
    kernel_mutex_lock(mtx_giant_io);
    _HAL_WREG8(hal_hw_get_addr_port(s_spi->cs_locked->e_port),
               _HAL_RREG8(hal_hw_get_addr_port(s_spi->cs_locked->e_port)) | (s_spi->cs_locked->bit_mask));

    kernel_mutex_unlock(mtx_giant_io);

	s_spi->cs_locked = NULL;

    kernel_mutex_unlock(s_spi->mtx_spi);
    //printf_P(PSTR("SESSION UNLOCKED\r\n"));
	return 0;

unlock_failed:
    //kernel_mutex_unlock(s_spi->mtx_spi);
	setSysError(SYSTEM_SPI_UNLOCK_FAILED);
	return 1;
}

uint8_t
spi_transfer(struct spi_dev * s_spi, uint8_t _data)
{
#if SPI_ASSERT_MY_FS_IN_TRASMIT_FUNC == YES
    ASSERT_NULL_ER(s_spi, SYSTEM_NULL_ARGUMENT, 1);
	ASSERT_NULL_ER(s_spi->cs_locked, SYSTEM_DEV_HW_NOT_LOCKED, 1);
#endif

    const struct spi_hwaddr * dev_hw_ptr = &s_spi_addr[(uint8_t) s_spi->e_dev];

    uint16_t spdr = pgm_read_word( &(dev_hw_ptr->spdr));
    uint16_t spsr = pgm_read_word( &(dev_hw_ptr->spsr));

    _HAL_WREG8( spdr,  _data );
	//SPDR = _data;
	while (!(_HAL_RREG8(spsr) & (1 << SPIF)));
	//while (!(SPSR & (1 << SPIF)));
	return _HAL_RREG8(spdr);
	//return SPDR;
}

/*---STATIC FUNCTIONS---*/

static int8_t spi_static_write(struct kstream *stream, uint8_t var)
{
	uint8_t ret = 0;
	/*struct spi_chip_select_pins * e_cs = spi_get_cs_by_stream(stream);
	ASSERT_NULL(e_cs, spi_static_write_null);

	//_SFR_IO8(kernel_hw_get_addr_port(e_cs->e_port)) &= ~(e_cs->bit_mask); //pull down chip select (slave select)
	_KRN_WREG8(kernel_hw_get_addr_port(e_cs->e_port), _KRN_RREG8(kernel_hw_get_addr_port(e_cs->e_port)) & ~(e_cs->bit_mask));
	ret = spi_transfer((uint8_t) var);
	//_SFR_IO8(kernel_hw_get_addr_port(e_cs->e_port)) |= (e_cs->bit_mask); //pull up chip select (slave select)
    _KRN_WREG8(kernel_hw_get_addr_port(e_cs->e_port), _KRN_RREG8(kernel_hw_get_addr_port(e_cs->e_port)) | (e_cs->bit_mask));

spi_static_write_null:*/
	return ret;
}

static int16_t spi_static_read(struct kstream * stream)
{
	uint8_t ret = 0;
/*	struct spi_chip_select_pins * e_cs = spi_get_cs_by_stream(stream);
	ASSERT_NULL(e_cs, spi_static_read_null);

	//_SFR_IO8(kernel_hw_get_addr_port(e_cs->e_port)) &= ~(e_cs->bit_mask); //pull down chip select (slave select)
	_KRN_WREG8(kernel_hw_get_addr_port(e_cs->e_port), _KRN_RREG8(kernel_hw_get_addr_port(e_cs->e_port)) & ~(e_cs->bit_mask));
	ret = spi_transfer(0);
	//_SFR_IO8(kernel_hw_get_addr_port(e_cs->e_port)) |= (e_cs->bit_mask); //pull up chip select (slave select)
    _KRN_WREG8(kernel_hw_get_addr_port(e_cs->e_port), _KRN_RREG8(kernel_hw_get_addr_port(e_cs->e_port)) | (e_cs->bit_mask));

spi_static_read_null:*/
	return ret;
}

static int16_t spi_static_readlen(struct kstream *stream)
{
	return 0;
}

static int16_t spi_static_writelen(struct kstream *stream)
{
	return 0;
}

void
driver_spi_attachInterrupt(struct spi_dev * s_spi, void (*ISRHANDLER)(), uint8_t delayed)
{
    const struct spi_hwaddr * dev_hw_ptr = &s_spi_addr[(uint8_t) s_spi->e_dev];
    uint16_t spcr = pgm_read_word( &(dev_hw_ptr->spcr));
	_HAL_WREG8(spcr, _HAL_RREG8(spcr) | (1 << SPIE));

    enum isr_type e_isrt = (delayed == 1) ? ISR_DELAYED : ISR_NORMAL;

    kernel_isr_grab(SPI_STC_vect_num, ISRHANDLER, e_isrt);
	//SPCR |= (1 << SPIE);
}

void
driver_spi_detachInterrupt(struct spi_dev * s_spi)
{
    const struct spi_hwaddr * dev_hw_ptr = &s_spi_addr[(uint8_t) s_spi->e_dev];
    uint16_t spcr = pgm_read_word( &(dev_hw_ptr->spcr));
	_HAL_WREG8(spcr, _HAL_RREG8(spcr) & ~(1 << SPIE));

	kernel_isr_release(SPI_STC_vect_num);
	//SPCR &= ~(1 << SPIE);
}

static void
driver_spi_setBitOrder(struct spi_dev * s_spi, enum spi_bit_order bitOrder)
{
    const struct spi_hwaddr * dev_hw_ptr = &s_spi_addr[(uint8_t) s_spi->e_dev];
    uint16_t spcr = pgm_read_word( &(dev_hw_ptr->spcr));

	if(bitOrder == LSBFIRST)
	{
	    _HAL_WREG8(spcr, _HAL_RREG8(spcr) | (1 << DORD));
		//SPCR |= (1 << DORD);
	}
	else
	{
	    _HAL_WREG8(spcr, _HAL_RREG8(spcr) & ~(1 << DORD));
		//SPCR &= ~(1 << DORD);
	}
}

static void
driver_spi_setDataMode(struct spi_dev * s_spi, enum spi_data_mode mode)
{
    const struct spi_hwaddr * dev_hw_ptr = &s_spi_addr[(uint8_t) s_spi->e_dev];
    uint16_t spcr = pgm_read_word( &(dev_hw_ptr->spcr));

    _HAL_WREG8(spcr, (_HAL_RREG8(spcr) & ~SPI_MODE_MASK) | mode);
	//SPCR = (SPCR & ~SPI_MODE_MASK) | mode;
}

static void
driver_spi_setClockDivider(struct spi_dev * s_spi, enum spi_clock rate)
{
    const struct spi_hwaddr * dev_hw_ptr = &s_spi_addr[(uint8_t) s_spi->e_dev];
    uint16_t spcr = pgm_read_word( &(dev_hw_ptr->spcr));
    uint16_t spsr = pgm_read_word( &(dev_hw_ptr->spsr));

    _HAL_WREG8(spcr, (_HAL_RREG8(spcr) & ~SPI_CLOCK_MASK) | (rate & SPI_CLOCK_MASK));
	//SPCR = (SPCR & ~SPI_CLOCK_MASK) | (rate & SPI_CLOCK_MASK);
	_HAL_WREG8(spsr, (_HAL_RREG8(spsr) & ~SPI_2XCLOCK_MASK) | ((rate >> 2) & SPI_2XCLOCK_MASK));
	//SPSR = (SPSR & ~SPI_2XCLOCK_MASK) | ((rate >> 2) & SPI_2XCLOCK_MASK);
}
#endif // BUILD_WITH_SPI
