/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */
#include "build_defs.h"
#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <avr/eeprom.h>
#include "hal_sectioning.h"
#include "hal_private.h"

#include "../os/kernel/kernel_cs.h"

#if WITH_DHCP == YES
    #include "../processes/dhclient/libnetwork.h"
    t_dev_id if_did[DHCP_SLOTS] EEMEM;                          /*run dhcp for the following dds*/
#endif // WITH_DHCP

int8_t
hal_external_config_dump()
{

#if WITH_DHCP == YES
    //write DHCP
    CRITICAL_SECTION()
    {
        for (uint8_t i = 0; i<DHCP_SLOTS; i++)
        {
            t_dev_id did = dhcp_get_did(i);
            eeprom_update_block((const void *)&did, (void*)&if_did[i], sizeof(t_dev_id));
        }
    }
#endif // WITH_DHCP

    return 0;
}

int8_t
hal_external_config_restore()
{
#if WITH_DHCP == YES
    eeprom_busy_wait();

    CRITICAL_SECTION()
    {
        for (uint8_t i = 0; i<DHCP_SLOTS; i++)
        {
            t_dev_id did = -1;
            eeprom_read_block((void*) &did, (const void *)&if_did[i], sizeof(t_dev_id));
            if (did != K_NO_DD)
            {
                dhclient(hal_get_knetev_by_did(did));
            }
        }
    }
#endif // WITH_DHCP

    return 0;
}
