/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "build_defs.h"
#include <stdint.h>
#include <stdbool.h>
#include "./error.h"

static uint8_t sys_error[MAX_ERROR_SIZE] = {[ 0 ... MAX_ERROR_SIZE-1 ] = 0};
static uint8_t sys_pos = 0;

static uint8_t sys_error_current = 0;

void sys_errors_reset()
{
	uint8_t i = 0;
	for (;i<MAX_ERROR_SIZE;i++) sys_error[i] = 0;
	sys_pos = 0;
	sys_error_current = 0;

	return;
}

uint8_t getSysEPos()
{
	return sys_pos;
}

uint8_t getSysErrorAt(uint8_t at)
{
	if (at >= MAX_ERROR_SIZE)
	{
		return 0;
	}

	return sys_error[at];
}

uint8_t getSysErrror(){
	return sys_error_current;
}

uint8_t getLastSysErrror(){
	uint8_t error_id = sys_pos;

	if (sys_pos == 0) error_id = MAX_ERROR_SIZE-1;
	else --error_id;

	return sys_error[error_id];
}

void setSysError(uint8_t err){
	sys_error_current = err;
	sys_error[sys_pos] = err;

	++sys_pos;
	if (sys_pos >= MAX_ERROR_SIZE) sys_pos = 0;

	return;
}

void clearSysError(){
	sys_error_current = 0;
}
