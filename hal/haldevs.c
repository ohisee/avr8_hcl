/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "haldevs.h"

/*fd_dev enum captions*/
FOREACH_DD_CODE(GENERATE_VARS_VAL);

const char * const dd_dev_captions[] PROGMEM =
{
	FOREACH_DD_CODE(GENERATE_VAR_NAMES_VAL)
	P_NULL
};

const uint8_t dd_dev_ids[] PROGMEM =
{
	FOREACH_DD_CODE(GENERATE_VARS_NUMERICAL)
};


const uint8_t dd_dev_cnt PROGMEM = FOREACH_DD_CODE(COUNT_TOTAL_ELEM) + 0;

//----------------------------------
FOREACH_D_HANDLERS(GENERATE_VARS_VAL)

const char * const d_handlers_captions[] PROGMEM = {
	FOREACH_D_HANDLERS(GENERATE_VAR_NAMES_VAL)
	P_NULL
};

//--------------------------------

FOREACH_SPI_HNDLRS(GENERATE_VARS_VAL)

const char * const spi_handlers_captions[] PROGMEM = {
	FOREACH_SPI_HNDLRS(GENERATE_VAR_NAMES_VAL)
	P_NULL
};

//--------------------------------

FOREACH_OWI_HNDLRS(GENERATE_VARS_VAL)

const char * const owi_handlers_captions[] PROGMEM = {
	FOREACH_OWI_HNDLRS(GENERATE_VAR_NAMES_VAL)
	P_NULL
};
//--------------------------------

FOREACH_IO_HNDLRS(GENERATE_VARS_VAL)

const char * const io_handlers_captions[] PROGMEM = {
	FOREACH_IO_HNDLRS(GENERATE_VAR_NAMES_VAL)
	P_NULL
};
//--------------------------------

FOREACH_VIRTUAL_HNDLRS(GENERATE_VARS_VAL)

const char * const virtual_handlers_captions[] PROGMEM = {
	FOREACH_VIRTUAL_HNDLRS(GENERATE_VAR_NAMES_VAL)
	P_NULL
};
//--------------------------------

const uint8_t d_handlers_ids[] PROGMEM = {
	FOREACH_D_HANDLERS(GENERATE_VARS_NUMERICAL)
};

const uint8_t spi_handlers_ids[] PROGMEM = {
	FOREACH_SPI_HNDLRS(GENERATE_VARS_NUMERICAL)
};

const uint8_t owi_handlers_ids[] PROGMEM = {
	FOREACH_OWI_HNDLRS(GENERATE_VARS_NUMERICAL)
};

const uint8_t io_handlers_ids[] PROGMEM = {
	FOREACH_IO_HNDLRS(GENERATE_VARS_NUMERICAL)
};

const uint8_t virtual_handlers_ids[] PROGMEM = {
	FOREACH_VIRTUAL_HNDLRS(GENERATE_VARS_NUMERICAL)
};

const uint8_t d_handlers_cnt PROGMEM = FOREACH_D_HANDLERS(COUNT_TOTAL_ELEM) + 0;
const uint8_t spi_handlers_cnt PROGMEM = FOREACH_SPI_HNDLRS(COUNT_TOTAL_ELEM) + 0;
const uint8_t owi_handlers_cnt PROGMEM = FOREACH_OWI_HNDLRS(COUNT_TOTAL_ELEM) + 0;
const uint8_t io_handlers_cnt PROGMEM = FOREACH_IO_HNDLRS(COUNT_TOTAL_ELEM) + 0;
const uint8_t virtual_handlers_cnt PROGMEM = FOREACH_VIRTUAL_HNDLRS(COUNT_TOTAL_ELEM) + 0;
