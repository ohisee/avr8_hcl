/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include "./device/dev.h"
#include "hal_sectioning.h"
#include "hal.h"
#include "./handlers.h"

#include "drivers/serial/usart.h"
#include "drivers/io/io.h"
#include "drivers/spi/spi.h"
#include "drivers/timer/timer.h"
#include "drivers/adc/adc.h"

#include "drivers/enc28j60/enc28.h"
#include "drivers/ds18x20/ds18.h"
#include "drivers/annunciator_panel/a_panel.h"
#include "drivers/usart2eth/u2eth.h"
#include "drivers/input_scanner/inpscan.h"

#define FOREACH_DD_HANDLERS(CV) \
    CV(DEV_DESCR_USART,     ROOT_DRIVER_DEV,        usart_hal_message,      usart_stream_init,      usart_kstream_init) \
    CV(DEV_DESCR_IO,        ROOT_DRIVER_DEV,        io_hal_message,         io_stream_init,         io_kstream_init) \
    CV(DEV_DESCR_SPI,       ROOT_DRIVER_DEV,        spi_hal_message,        spi_stream_init,        spi_kstream_init) \
    CV(DEV_DESCR_SPI,       SPI_ENC28J60,           enc28_hal_message,      enc28_stream_init,      enc28_kstream_init) \
    CV(DEV_DESCR_TIMER,     ROOT_DRIVER_DEV,        timer_hal_message,      timer_stream_init,      timer_kstream_init) \
    CV(DEV_DESCR_ADC,       ROOT_DRIVER_DEV,        adc_hal_message,        adc_stream_init,         adc_kstream_init) \
    CV(DEV_DESCR_OWI,       SOFTWARE_OWI_DS18X20,   ds18x20_hal_message,    ds18x20_stream_init,    ds18x20_kstream_init) \
    CV(DEV_DESCR_IO,        IO_ANNUNCIATOR_PANEL,   anon_panel_hal_message, anon_panel_stream_init, anon_panel_kstream_init)\
    CV(DEV_DESCR_VIRTUAL,   U2ETH_ROUTER,           u2eth_hal_message,      u2eth_stream_init,      u2eth_kstream_init) \
    CV(DEV_DESCR_VIRTUAL,   IO_DIG_INP_MON,         inpscan_hal_message,    inpscan_stream_init,    inpscan_kstream_init) \
    CV(DEV_DESCR_NULL,      ROOT_DRIVER_DEV,        NULL,                   NULL,                   NULL)

const struct dd_handlers dd_hndl_list[] PROGMEM =
{
    FOREACH_DD_HANDLERS(GENERATE_ARRAY_5ARGS)
};

ATTRIBUTE_HAL_SECTION
const struct dd_handlers *
handler_search(enum dev_descr_code e_dev_code, enum dev_desc_handlers e_dev_hndl)
{
    HNDL_CNT i = 0;
    //for (HNDL_CNT i = 0; i < DD_HNDLR_COUNT; i++)
    while (1)
    {
        uint8_t code = pgm_read_byte(&(dd_hndl_list[i].e_dd_code));
        #if DD_HNDLR_COUNT > 255
            uint16_t code2 = pgm_read_word(&(dd_hndl_list[i].e_dh_code));
        #else
            uint8_t code2 = pgm_read_byte(&(dd_hndl_list[i].e_dh_code));
        #endif // DD_HNDLR_COUNT

        if ((code == DEV_DESCR_NULL) && (code2 == ROOT_DRIVER_DEV) )
        {
            return NULL;
        }
        else if ( (e_dev_code == code) && (e_dev_hndl == code2) )
        {
            return &dd_hndl_list[i];
        }
        i++;
    }

    return NULL;
}
