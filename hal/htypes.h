#ifndef HTYPES_H_INCLUDED
#define HTYPES_H_INCLUDED

#include <stdint.h>

#define K_NO_DD -1      //return when error or no dev present
#define K_NO_TID -1     //no transaction
#define K_NO_TASK -1    //no task was created
#define K_NO_NETID -1   //no IF created or etc
#define K_NO_LISID -1   //no listen ID
#define K_NO_STATE -1
#define K_FAIL_STREAM 1
#define K_MSG_ERROR -1
#define K_HW_BUSY 1

typedef int8_t t_dev_id;
typedef uint8_t t_dev_count;
typedef int8_t t_stream_id;
typedef int16_t t_trans_id;
typedef int8_t t_netdev_id;
typedef int8_t t_state_id;
typedef int8_t t_listen_id;
typedef int8_t t_client_id;
typedef uint32_t t_net_addr;
typedef uint16_t t_net_port;

#endif // HTYPES_H_INCLUDED
