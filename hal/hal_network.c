/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include "hal_sectioning.h"
#include "hal_private.h"

//os
#include "../os/kernel/kernel_memory.h"
#include "../os/kernel/kernel_process.h"
#include "../os/kernel/kernel_mutex.h"
#include "../os/kernel/kernel_cs.h"

#if NETWORKING_HIGH_MEM == YES
#define NET_PROC_STACK_SIZE 100  //150
#define HNDLR_PROC_STACK_SIZE 300   //400
#else
#define NET_PROC_STACK_SIZE 300  //400
#endif

#define NET_HAL_POLL_NIC_US 10000

static t_netdev_id net_dev_amount = 0;
static k_pid poll_task_id = K_NO_PID;
#if NETWORKING_HIGH_MEM == YES
static k_pid handler_task_id = K_NO_PID;
volatile struct kproc_wait_condition * cond_hndlr = NULL;
volatile int8_t cnd_var = 0;
#endif // NETWORKING_HIGH_MEM
volatile struct knetdevice_list * volatile ll_netdevice = NULL;
volatile struct knetstate_list * volatile ll_states = NULL;
volatile struct knetlisten_list * volatile ll_netlisten = NULL;

static void nif_handler(void * arg);
static void nif_iterator(void * arg);
static int8_t __check_bindport_isfree(uint32_t bindip, uint16_t bindport, enum knet_proto listen_proto);


ATTRIBUTE_HAL_SECTION
static int8_t
__check_bindport_isfree(uint32_t bindip,
                        uint16_t bindport,
                        enum knet_proto listen_proto)
{
    if (listen_proto == KNET_TCP)
    {
        SEARCH_IN_LIST_2F(struct knetstate_list, ll_states, sit,
                          (sit->node->laddr == bindip) &&
                          (sit->node->lport == bindport) &&
                          (sit->node->e_knet_proto == KNET_TCP),
                          error_found);
    }

    SEARCH_IN_LIST_2F(struct knetlisten_list, ll_netlisten, lit,
                      (lit->node->e_knet_proto == listen_proto) &&
                      ((lit->node->laddr == bindip) || (lit->node->laddr == 0)) &&
                      (lit->node->lport == bindport),
                      error_found);

    return 0;

error_found:
    return 1;
}

#if NETWORKING_HIGH_MEM == YES

ATTRIBUTE_HAL_SECTION
static void
nif_handler(void * arg)
{
    while(1)
    {
        kernel_cond_wait(cond_hndlr);
        CRITICAL_SECTION()
        {
            --cnd_var;
        }
        //printf_P(PSTR("WAKEUP\r\n"));

        ITERATE_BLOCK_DIRECT_MAN(volatile struct knetdevice_list, ll_netdevice, __it)
        {
            if (kernel_mutex_try_lock(__it->node->m_rx) == 0)
            {
                //printf_P(PSTR("locked\r\n"));
                if ( (b_get_read_len(__it->node->b_rx_buf) != 0) &&
                     (__it->node->rx_state == NULL) )
                {
                   // printf_P(PSTR("eth\r\n"));
                    eth_filter(__it->node);
                }
                kernel_mutex_unlock(__it->node->m_rx);
            }

            //next
            ITERATE_NEXT(__it);
        }
    }

}
#endif // NETWORKING_HIGH_MEM

ATTRIBUTE_HAL_SECTION
static void
nif_iterator(void * arg)
{
    int8_t ret = 0;

    while (1)
    {
        ITERATE_BLOCK_DIRECT_MAN(volatile struct knetdevice_list, ll_netdevice, __it)
        {
            if (kernel_mutex_try_lock(__it->node->m_rx) == 0)
            {
                if (__it->node->rx_state == NULL)
                {
                    ret = __it->node->hndls.POLL_DEVICE(__it->node);
                    if (ret > 0)
                    {
                        //data received processing!
                        //fprintf_P(stdout, PSTR("PKT RECEIVED\r\n"));
                        #if NETWORKING_HIGH_MEM == YES
                        CRITICAL_SECTION()
                        {
                            ++cnd_var;
                        }
                        #else
                            eth_filter(__it->node);
                        #endif

                    }
                    else
                    {
                        if (ret == -1)
                        {
                            //#if DEBUG_MESSAGES == YES
                            printf_P(PSTR("Netboard poll failed!\r\n"));
                            //#endif // DEBUG_MESSAGES
                        }

                    }
                }

                kernel_mutex_unlock(__it->node->m_rx);
            }
            //next
            ITERATE_NEXT(__it);


        } //ITERATE_BLOCK_DIRECT_MAN

        kernel_process_usleep(NET_HAL_POLL_NIC_US, 0);
    }
    return 0;
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_init_network_stack()
{
#if NETWORKING_HIGH_MEM == YES
    cnd_var = 0;
    cond_hndlr = kernel_cond_wait_init(&cnd_var, SC_WAKEUP_LARGER_ZERO);
    if (cond_hndlr == NULL)
    {
        #if DEBUG_MESSAGES == YES
        printf_P(PSTR("[NIF_ITERATOR] CREATE FAILED cnd_var!\r\n"));
        #endif // DEBUG_MESSAGES
        return 1;
    }

    //create NIC poll
    poll_task_id = kernel_create_process(PSTR("[NIF_ITERATOR]"), KPROC_TYPE_SYSTEM, NET_PROC_STACK_SIZE, nif_iterator, NULL);
    if (poll_task_id == K_NO_TASK)
    {
        //task create fail!
        #if DEBUG_MESSAGES == YES
        printf_P(PSTR("[NIF_ITERATOR] CREATE FAILED poll_task_id!\r\n"));
        #endif // DEBUG_MESSAGES
        return 1;
    }

    handler_task_id = kernel_create_process(PSTR("[IP_PACK_PARCER]"), KPROC_TYPE_SYSTEM, HNDLR_PROC_STACK_SIZE, nif_handler, NULL);
    if (handler_task_id == K_NO_TASK)
    {
        //task create fail!
        #if DEBUG_MESSAGES == YES
        printf_P(PSTR("[NIF_ITERATOR] CREATE FAILED handler_task_id!\r\n"));
        #endif // DEBUG_MESSAGES
        return 1;
    }
#else
    //create NIC poll
    poll_task_id = kernel_create_process(PSTR("[NIF_ITERATOR]"), KPROC_TYPE_SYSTEM, NET_PROC_STACK_SIZE, nif_iterator, NULL);
    if (poll_task_id == K_NO_TASK)
    {
        //task create fail!
        #if DEBUG_MESSAGES == YES
        printf_P(PSTR("[NIF_ITERATOR] CREATE FAILED poll_task_id!\r\n"));
        #endif // DEBUG_MESSAGES
        return 1;
    }
#endif

    return 0;
}

ATTRIBUTE_HAL_SECTION
struct knetdevice_list *
hal_net_mem_knetdev()
{
    return ll_netdevice;
}

ATTRIBUTE_HAL_SECTION
struct knetlisten_list *
hal_net_mem_knetlisten()
{
    return ll_netlisten;
}

ATTRIBUTE_HAL_SECTION
struct knetstate_list *
hal_net_mem_knetstate()
{
    return ll_states;
}

ATTRIBUTE_HAL_SECTION
struct knetdevice *
hal_net_add_dd(struct dev_descr_node * dd,
               void * driver_nic_set,
               uint8_t * mac_addr,
               struct knetdev_handlers * hndlr)
{
    ASSERT_NULL_ER(dd, SYSTEM_NULL_ARGUMENT, NULL);
    ASSERT_NULL_ER(driver_nic_set, SYSTEM_NULL_ARGUMENT, NULL);
    ASSERT_NULL_ER(mac_addr, SYSTEM_NULL_ARGUMENT, NULL);

    if (poll_task_id == K_NO_PID)
    {
        if (hal_init_network_stack() != 0)
        {
            setSysError(SYSTEM_KERNEL_PROCESS_CREATE_FAILED);
            return NULL;
        }
    }

    struct knetdevice * nd = (struct knetdevice *) kernel_malloc(sizeof(struct knetdevice));
    ASSERT_NULL_ER(nd, SYSTEM_MALLOC_FAILED, NULL);

    //setting struct fileds
    nd->ddn = dd;
    nd->net_dev_id = dd->dev_id;
    nd->driver_nic_stuct = driver_nic_set;
    nd->mac = mac_addr;
    nd->hndls.MOD_CONNECTION = hndlr->MOD_CONNECTION;
    nd->hndls.POLL_DEVICE = hndlr->POLL_DEVICE;
    nd->hndls.TRANSMIT = hndlr->TRANSMIT;
    nd->b_rx_buf = b_create(NULL, BUF_TYPE_LINEAR, nd->rx_buf, NETWORK_MAX_FRAMELEN);
    nd->b_tx_buf = b_create(NULL, BUF_TYPE_LINEAR, nd->tx_buf, NETWORK_MAX_FRAMELEN);
    b_reset(nd->b_rx_buf);
    b_reset(nd->b_tx_buf);
    nd->m_tx = kernel_mutex_init();
    nd->m_rx = kernel_mutex_init();
    nd->rx_state = NULL;
    memset((void*) &nd->nic_set, 0, sizeof(struct kern_nic_setup));

    //initializing id selector
    t_netdev_id sel_netdev_id = 0;

    //creating new list container
    NEW_LIST_ITEM(struct knetdevice_list, litem, list_fail);
    litem->node = nd;

    //pushing it to list with sorking also looking for the nearest id

    kernel_mutex_lock(hal_mux); //<--- LOCKING MUTEX
    PUSH_TO_LIST(ll_netdevice, litem);
  /*  if (ll_netdevice == NULL)
	{
		ll_netdevice = litem;
		sel_netdev_id = 0;
	}
	else
	{
		ADD_TO_LIST_MANUAL(struct knetdevice_list, ll_netdevice)
		{

			IF_LAST_ELEMENT
			{
				sel_netdev_id = PREV_IT->node->net_dev_id + 1;
				INJECT_ELEMENT_AFTER_PREV_IT(litem);
				FINISH_SORTING;
			}

			COMPARE_DIFF(net_dev_id, 2)
			{
				sel_netdev_id = CURT_IT->node->net_dev_id - (CURT_IT->node->net_dev_id - PREV_IT->node->net_dev_id);
				INJECT_ELEMENT_BEFORE_IT(litem, ll_netdevice);
				FINISH_SORTING;
			}

			NEXT_ELEMENT;

		}

	}
	//setting for id tot he instance
	//nd->net_dev_id = sel_netdev_id;
	#if DEBUG_MESSAGES == YES
    printf_P(PSTR("Created new network interface:#%d\r\n"), sel_netdev_id);
    #endif // DEBUG_MESSAGES
*/
    //increasing the amount
    ++net_dev_amount;

    kernel_mutex_unlock(hal_mux); //<--- LOCKING MUTEX

    return nd;  //return the

list_fail:
    setSysError(SYSTEM_MALLOC_FAILED);
    b_free(nd->b_rx_buf);
    b_free(nd->b_tx_buf);
    kernel_free((void*)nd->m_tx);
    kernel_free((void*)nd->m_rx);
    kernel_free((void *) nd);
    return NULL;
}

ATTRIBUTE_HAL_SECTION
struct knetdevice *
hal_get_knetdev(t_netdev_id knid)
{
    ASSERT_NEGATIVE_ER(knid, SYSTEM_ID_INCORRECT, NULL);

    SEARCH_IN_LIST_2(LIST knetdevice_list, ll_netdevice, ll, ll->node->net_dev_id == knid, error_not_found);

    return ll->node;

error_not_found:
    setSysError(SYSTEM_ITEM_NOT_FOUND);
    return NULL;
}

ATTRIBUTE_HAL_SECTION
struct knetdevice *
hal_get_knetev_by_did(t_dev_id did)
{
    ASSERT_NEGATIVE_ER(did, SYSTEM_FD_INVALID, NULL);

    SEARCH_IN_LIST_2(LIST knetdevice_list, ll_netdevice, ll, ll->node->ddn->dev_id == did, error_not_found);

    return ll->node;

error_not_found:
    setSysError(SYSTEM_ITEM_NOT_FOUND);
    return NULL;
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_net_remove_dd(struct knetdevice * net_dev)
{
    ASSERT_NULL_ER(net_dev, SYSTEM_NULL_ARGUMENT, 1);



    //also lock net if buffer mutexes
    kernel_mutex_lock(net_dev->m_rx);
    kernel_mutex_lock(net_dev->m_tx);

    kernel_mutex_lock(hal_mux); //<--- LOCKING MUTEX
    //removing all listen
    REMOVE_FROM_LIST_ALL_MATCH(LIST knetlisten_list,
                               ll_netlisten,
                               lln,
                               lln->node->laddr == net_dev->nic_set.ipv4,
                               kernel_free((void*)lln->node); kernel_free((void*) lln); );

    //removing all states
    REMOVE_FROM_LIST_ALL_MATCH(LIST knetstate_list,
                               ll_states,
                               lls,
                               lls->node->laddr == net_dev->nic_set.ipv4,
                               kernel_free((void*)lls->node); kernel_free((void*) lls); );

    REMOVE_FROM_LIST_2(LIST knetdevice_list, ll_netdevice, ll, ll->node->net_dev_id == net_dev->net_dev_id, error_not_found);

    kernel_free((void*) ll); //remove list item

    kernel_free((void*)net_dev->b_rx_buf);
    kernel_free((void*)net_dev->b_tx_buf);
    kernel_mutex_unlock(net_dev->m_rx);
    kernel_free((void*)net_dev->m_rx);
    kernel_mutex_unlock(net_dev->m_tx);
    kernel_free((void*)net_dev->m_tx);
    kernel_free((void*) net_dev);

    kernel_mutex_unlock(hal_mux); //<--- LOCKING MUTEX

    return 0;

error_not_found:
    setSysError(SYSTEM_ITEM_NOT_FOUND);
    return 1;
}

/*ATTRIBUTE_HAL_SECTION
t_listen_id
hal_open_server_socket(t_net_addr listen_ip,
                       t_net_port listen_port,
                       enum knet_proto listen_proto,
                       struct knetlisten_handlers * hndlr)
{
    int8_t state = __check_bindport_isfree(listen_ip, listen_port, listen_proto);
    if (state != 0)
    {
        setSysError(SYSTEM_NET_PORT_IS_BUSY);
        return K_NO_LISID;
    }

    struct knetlisten * listen = hal_net_listen_socket(listen_proto,
                                                           listen_ip,
                                                           listen_port,
                                                           hndlr);
    ASSERT_NULL(listen, error);

    return listen->k_listen;

error:
    return K_NO_LISID;
}*/

/*ATTRIBUTE_HAL_SECTION
t_listen_id
hal_open_client_socket(t_net_addr remote_ip, t_net_port remote_port, enum knet_proto e_proto)
{
    return K_NO_LISID;
}*/

/*ATTRIBUTE_HAL_SECTION
t_state_id
hal_get_state_by_destip(t_net_addr remote_ip)
{
    return -1;
}
*/

ATTRIBUTE_HAL_SECTION
const struct knetlisten *
hal_get_listen(t_net_addr local_ip, t_net_port loc_net_port, enum knet_proto e_proto)
{
    SEARCH_IN_LIST_2(struct knetlisten_list, ll_netlisten, ditem,
                 ((ditem->node->laddr == local_ip) ||
                  (ditem->node->laddr == 0)
                 ) &&
                 (ditem->node->lport == loc_net_port) &&
                 (ditem->node->e_knet_proto == e_proto),
                 not_found
                );

    return (const struct knetlisten *) ditem->node;

not_found:
    return NULL;
}

ATTRIBUTE_HAL_SECTION
const struct knetlisten *
hal_get_listen_by_state(struct knetstate * state)
{
    ASSERT_NULL_ER(state, SYSTEM_NULL_ARGUMENT, NULL);

    return hal_get_listen(state->laddr, state->lport, state->e_knet_proto);
}

/*ATTRIBUTE_HAL_SECTION
const struct knetlisten *
hal_get_listen_for_udp(t_net_addr local_ip, t_net_port local_port)
{
    return hal_get_listen(local_ip, local_port, KNET_UDP);
}*/

ATTRIBUTE_HAL_SECTION
struct knetlisten *
hal_net_listen_socket(enum knet_proto e_knet_proto,
                       t_net_addr laddr,
                       t_net_port lport,
                        struct knetlisten_handlers * hndls)
{
    //lookup if port already bind

    int8_t state = __check_bindport_isfree(laddr, lport, e_knet_proto);
    if (state != 0)
    {
        setSysError(SYSTEM_NET_PORT_IS_BUSY);
        return NULL;
    }

    struct knetlisten * listen = (struct knetlisten *) kernel_malloc(sizeof(struct knetlisten));
    ASSERT_NULL_ER(listen, SYSTEM_MALLOC_FAILED, NULL);

    NEW_LIST_ITEM(struct knetlisten_list, litem, error_list);
    litem->node = listen;

    //initializing id selector
    t_listen_id sel_listen_id = 0;

    kernel_mutex_lock(hal_mux); //<--- LOCKING MUTEX

    //pushing it to list with sorking also looking for the nearest id
    if (ll_netlisten == NULL)
	{
		ll_netlisten = litem;
		sel_listen_id = 0;
	}
	else
	{
		ADD_TO_LIST_MANUAL(struct knetlisten_list, ll_netlisten)
		{

			IF_LAST_ELEMENT
			{
				sel_listen_id = PREV_IT->node->k_listen + 1;
				INJECT_ELEMENT_AFTER_PREV_IT(litem);
				FINISH_SORTING;
			}

			COMPARE_DIFF(k_listen, 2)
			{
				sel_listen_id = CURT_IT->node->k_listen - (CURT_IT->node->k_listen - PREV_IT->node->k_listen);
				INJECT_ELEMENT_BEFORE_IT(litem, ll_netlisten);
				FINISH_SORTING;
			}

			NEXT_ELEMENT;

		}

	}
	//setting for id tot he instance
	listen->k_listen = sel_listen_id;
    listen->e_knet_proto = e_knet_proto;
    listen->laddr = laddr;
    listen->lport = lport;
    listen->hndls.CLOSED = hndls->CLOSED;
    listen->hndls.ONOPEN = hndls->ONOPEN;
    listen->hndls.ONRCV = hndls->ONRCV;

    kernel_mutex_unlock(hal_mux); //<--- UNLOCKING MUTEX

    return listen;


error_list:
    kernel_free((void *) listen);
error:
    setSysError(SYSTEM_MALLOC_FAILED);
    return NULL;
}

ATTRIBUTE_HAL_SECTION
struct knetstate *
hal_request_new_state()
{
    struct knetstate * state = (struct knetstate *) kernel_malloc(sizeof(struct knetstate));
    ASSERT_NULL_ER(state, SYSTEM_MALLOC_FAILED, NULL);

    NEW_LIST_ITEM(struct knetstate_list, litem, error_list);
    litem->node = state;

    //initializing id selector
    t_state_id sel_state_id = 0;

    kernel_mutex_lock(hal_mux); //<--- LOCKING MUTEX

    //pushing it to list with sorking also looking for the nearest id
    if (ll_states == NULL)
	{
		ll_states = litem;
		sel_state_id = 0;
	}
	else
	{
		ADD_TO_LIST_MANUAL(struct knetstate_list, ll_states)
		{

			IF_LAST_ELEMENT
			{
				sel_state_id = PREV_IT->node->kstate_id + 1;
				INJECT_ELEMENT_AFTER_PREV_IT(litem);
				FINISH_SORTING;
			}

			COMPARE_DIFF(kstate_id, 2)
			{
				sel_state_id = CURT_IT->node->kstate_id - (CURT_IT->node->kstate_id - PREV_IT->node->kstate_id);
				INJECT_ELEMENT_BEFORE_IT(litem, ll_states);
				FINISH_SORTING;
			}

			NEXT_ELEMENT;

		}

	}
	//setting for id tot he instance
	state->kstate_id = sel_state_id;

    kernel_mutex_unlock(hal_mux); //<--- LOCKING MUTEX


    return state;

error_list:
    free((void *) state);
error:
    setSysError(SYSTEM_MALLOC_FAILED);
    return NULL;
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_net_remove_state(struct knetstate * state)
{
    ASSERT_NULL_ER(state, SYSTEM_NULL_ARGUMENT, 1);

    kernel_mutex_lock(hal_mux); //<--- LOCKING MUTEX

    REMOVE_FROM_LIST_S(struct knetstate_list, ll_states, item, item->node->kstate_id == state->kstate_id);
    if (item != NULL)
    {
        if (item->node->sock_opts != NULL)
        {
            kernel_free((void *) item->node->sock_opts); //prevent memory leak
        }
        kernel_free((void*) item);
    }

    kernel_mutex_unlock(hal_mux); //<--- LOCKING MUTEX

    return 0;
}

ATTRIBUTE_HAL_SECTION
const struct knetstate *
hal_get_state(t_net_addr raddr, t_net_port rport, t_net_port lport)
{
    SEARCH_IN_LIST_2(struct knetstate_list, ll_states, ditem,
                      (ditem->node->raddr == raddr) &&
                      (ditem->node->rport == rport) &&
                      (ditem->node->lport == lport) &&
                      (ditem->node->e_knet_proto == KNET_TCP),
                      not_found);

    return (const struct knetstate *) ditem->node;

not_found:
    setSysError(SYSTEM_ITEM_NOT_FOUND);
    return NULL;
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_net_closeport(t_listen_id sock_id)
{
    return 1; //todo complete this section
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_send_dgram(struct knetdevice * k_net_dev,
               uint32_t destIP,
               uint16_t destPORT,
               uint16_t sourcePORT,
               enum knet_proto e_proto,
               uint8_t * buff,
               uint16_t len)
{
    struct knetstate state;
    state.kstate_id = K_NO_STATE;
    state.laddr = k_net_dev->nic_set.ipv4;
    state.lport = sourcePORT;
    state.raddr = destIP;
    state.rport = destPORT;
    state.sock_opts = NULL;
    state.e_knet_proto = e_proto;

    return hal_net_send(k_net_dev, &state, buff, len);
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_net_send(struct knetdevice * knetd, struct knetstate * state, uint8_t * buffer, uint16_t len)
{
    int32_t written = 0;
    eth_frame_t *frame = (eth_frame_t *) buffer;

    switch (state->e_knet_proto)
    {
        case KNET_IP:
            written = ip_send(knetd, frame, len);

#if DEBUG_MESSAGES == YES
            printf_P(PSTR("IP: sent: %li bytes"), written);
#endif // DEBUG_MESSAGES

        break;

        case KNET_ICMP:
        case KNET_UDP:
#if WITH_TCP == YES
            written = udp_send(knetd, state->raddr, state->rport, state->lport, frame, len);
#else
            return 1;
#endif // WITH_TCP

#if DEBUG_MESSAGES == YES
            printf_P(PSTR("UDP sent: %li bytes\r\n"), written);
#endif // DEBUG_MESSAGES

        break;

        case KNET_TCP:
#if WITH_TCP == YES
            written = tcp_send(knetd, state, frame, len, TCP_OPTION_PUSH);
#else
            return 1;
#endif
        break;

        default:
            setSysError(SYSTEM_NET_PROTO_NOT_SUP);
            return 1;
        break;
    }

#if NETWORK_COUNT_TRAFFIC == YES
            if (written > 0)
            {
                knetd->out_bytes += written;
                if (knetd->out_bytes >= 0xFFFFFBFF)
                {
                    knetd->out_bytes = 0;
                }
            }
#endif // NETWORK_COUNT_TRAFFIC

    return 0;
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_net_receive(struct knetdevice * knetd,
                struct knetlisten * knetl,
                uint8_t ** data_out,
                uint16_t * data_out_len,
                enum hal_net_rcv e_net_rcv )
{
    ASSERT_NULL_ER(knetd, SYSTEM_NULL_ARGUMENT, -1);
    ASSERT_NULL_ER(knetl, SYSTEM_NULL_ARGUMENT, -1);
    /*ASSERT_NULL_ER(data_out, SYSTEM_NULL_ARGUMENT, -1);
    ASSERT_NULL_ER(data_out_len, SYSTEM_NULL_ARGUMENT, -1);*/


    if (e_net_rcv == HAL_NRCV_NOBLOCK)
    {
        if ((knetd->rx_state->laddr != knetl->laddr) ||
            (knetd->rx_state->lport != knetl->lport) ||
            (knetd->rx_state->e_knet_proto != knetl->e_knet_proto))
        {
            return 1;
        }
    }
    else
    {
        while ( (knetd->rx_state->laddr != knetl->laddr) &&
                (knetd->rx_state->lport != knetl->lport) &&
                (knetd->rx_state->e_knet_proto != knetl->e_knet_proto))
        {
            //wait for data;
            kernel_process_usleep(5000, 0);
        }
    }
    kernel_mutex_lock(knetd->m_rx);

    if (data_out != NULL)
    {
        *data_out = hal_net_buffer_payload(knetd->rx_buf, knetl->e_knet_proto);
        *data_out_len = b_get_read_len(knetd->b_rx_buf);
    }

    return 0;
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_net_release_rx(struct knetdevice * knetd)
{
    if (knetd->rx_state != NULL)
    {
        if (knetd->rx_state->kstate_id == -1)
        {
            kernel_free((void*)knetd->rx_state);
        }

        knetd->rx_state = NULL;
        kernel_mutex_unlock(knetd->m_rx);
    }

    return 0;
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_net_onrcv_release_rx(struct knetdevice * knetd)
{
    if (knetd->rx_state != NULL)
    {
        if (knetd->rx_state->kstate_id == -1)
        {
            kernel_free((void*)knetd->rx_state);
        }

        knetd->rx_state = NULL;
    }

    return 0;
}

ATTRIBUTE_HAL_SECTION
uint8_t *
hal_net_grab_tx_buffer(struct knetdevice * knetd, enum knet_proto proto)
{
    kernel_mutex_lock(knetd->m_tx);
    return hal_net_buffer_payload(knetd->tx_buf, proto);
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_net_relese_tx_buffer(struct knetdevice * knetd)
{
    b_reset(knetd->b_tx_buf);
    kernel_mutex_unlock(knetd->m_tx);
    return 0;
}

ATTRIBUTE_HAL_SECTION
uint8_t *
hal_net_buffer_payload(uint8_t * buff, enum knet_proto proto)
{
    ASSERT_NULL_E(buff, SYSTEM_NULL_ARGUMENT, error);

    eth_frame_t *frame = (void*)buff;
	ip_packet_t *ip = (void*)(frame->data);
	udp_packet_t *udp = (void*)(ip->data);

    switch (proto)
    {
        case KNET_ROOT:
            return buff;
        case KNET_ETH:
            return frame->data;
            //return enc28->net_buf_out + sizeof(struct eth_frame);
        case KNET_IP:
            return ip->data;
            //return enc28->net_buf_out + (sizeof(struct ip_packet) + sizeof(struct eth_frame));
        case KNET_ICMP:
            /*{
                struct icmp_echo_packet * icmp = (void*)(ip->data);
                return icmp->data;
            }*/
            return buff + (sizeof(struct icmp_echo_packet) + sizeof(struct ip_packet) + sizeof(struct eth_frame));
        case KNET_UDP:
            return udp->data; //enc28->net_buf_out + (sizeof(struct udp_packet) + sizeof(struct ip_packet) + sizeof(struct eth_frame));
#if (WITH_TCP == YES)
        case KNET_TCP:
            return buff + (sizeof(struct tcp_packet) + sizeof(struct ip_packet) + sizeof(struct eth_frame));
#endif
        default:
            return buff;
    }

error:
    return 0;
}
