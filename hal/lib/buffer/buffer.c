/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <util/atomic.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include "../../device/dev.h"

#include "../../error.h"
#include "../../shared.h"
#include "buffer.h"

static void __unsafe_TBf8(struct buffer * buf, uint8_t inp);
static uint8_t __RBf_unsafe(struct buffer * buf);

struct buffer *
b_create(struct buffer * buf, enum buffer_type e_buf_type, uint8_t * ex_buf, uint32_t cbf_size)
{
    if (buf == NULL)
    {
        buf = (struct buffer *) kernel_malloc(sizeof(struct buffer));
    }
    ASSERT_NULL_E(buf, SYSTEM_MALLOC_FAILED, error);

    buf->flags = 0;

    if (ex_buf == NULL)
    {
        buf->buf_start = (uint8_t *) kernel_malloc(cbf_size);
        ASSERT_NULL_E(buf->buf_start, SYSTEM_MALLOC_FAILED, error_2);
        buf->buf_end = buf->buf_start + cbf_size - 1;
        buf->flags = BUF_FLAG_HEAP;
    }
    else
    {
        buf->buf_start = ex_buf;
        buf->buf_end = buf->buf_start + cbf_size - 1;
    }

    buf->read_ptr = buf->buf_start;
    buf->write_ptr = buf->buf_start;

    switch (e_buf_type)
    {
        case BUF_TYPE_LINEAR:
            buf->flags |= BUF_FLAG_LINEAR;
        break;

        case BUF_TYPE_CIRCULAR:
            buf->flags |= BUF_FLAG_CIRCULAR;
        break;
    }

    return buf;

error_2:
    kernel_free((void *) buf);
error:
    return NULL;
}

void
b_free(struct buffer * buf)
{
    if (buf == NULL)
    {
        return;
    }

    if (buf->flags & BUF_FLAG_HEAP)
    {
        kernel_free((void *) buf->buf_start);
        memset((void *) buf->buf_start, 0, buf->buf_end - buf->buf_start + 1);
    }

    kernel_free((void *) buf);
    memset((void *) buf, 0, sizeof(struct buffer));

    return;
}

void
b_free_static(struct buffer * buf)
{
    if (buf == NULL)
    {
        return;
    }

    kernel_free((void *) buf);
    memset((void *) buf, 0, sizeof(struct buffer));

    return;
}

void b_reset(struct buffer * buf)
{
    buf->read_ptr = buf->buf_start;
    buf->write_ptr = buf->buf_start;

    return;
}

void
b_reset_for_reading(struct buffer * buf)
{
    if (buf->flags & BUF_FLAG_LINEAR)
    {
        buf->read_ptr = buf->buf_start;
    }

    return;
}

void
b_set_new_data(struct buffer * buf, size_t offset)
{
    if (buf->flags & BUF_FLAG_LINEAR)
    {
        buf->write_ptr = buf->write_ptr + offset;
        buf->read_ptr = buf->buf_start;
    }

    return;
}

uint32_t
b_get_buf_len(struct buffer * buf)
{
    return (buf->buf_end - buf->buf_start + 1);
}

uint32_t
b_get_free_space(struct buffer * buf)
{
    if (buf->flags & BUF_FLAG_CIRCULAR)
    {
        if (buf->write_ptr < buf->read_ptr)
        {
            //R - W - 1
            return (uint32_t) (buf->read_ptr - buf->write_ptr);// - 1;
        }
        else if (buf->write_ptr == buf->read_ptr)
        {
            return (uint32_t) (buf->buf_end - buf->buf_start + 1);
        }
        else
        {
            //T-W+(T-(T-R)
            uint8_t * buf_len = buf->buf_end - buf->buf_start + 1;
            return (uint32_t) (buf_len - buf->write_ptr) + ( buf_len - ( buf_len - buf->read_ptr ));
        }
    }
    else if (buf->flags & BUF_FLAG_LINEAR)
    {
        return (uint32_t) (((buf->buf_end - buf->buf_start) + (uint8_t *)1) - buf->write_ptr);
    }
    else
    {
        return 0;
    }
}

size_t
b_get_read_len(struct buffer * buf)
{
    if (buf->flags & BUF_FLAG_CIRCULAR)
    {
        return b_get_buf_len(buf) - b_get_free_space(buf);
    }
    else if (buf->flags & BUF_FLAG_LINEAR)
    {
        return (uint32_t) (buf->write_ptr - buf->read_ptr + 1);
    }
    else
    {
        return 0;
    }
}

int8_t
b_has_more(struct buffer * buf)
{
    return (buf->write_ptr == buf->read_ptr) ? B_NO_MORE : B_MORE;
}

static void
__unsafe_TBf8(struct buffer * buf, uint8_t inp)
{
    if (buf->flags & BUF_FLAG_CIRCULAR)
    {
        *buf->write_ptr = inp;
        ++(buf->write_ptr);

        if (buf->write_ptr > buf->buf_end)
        {
             buf->write_ptr = buf->buf_start;
        }
    }
    else if (buf->flags & BUF_FLAG_LINEAR)
    {
        if (buf->write_ptr <= buf->buf_end)
        {
            *buf->write_ptr = inp;
            ++(buf->write_ptr);
            //++(buf->read_ptr);
        }
    }

	return;
}

int16_t
bTBf8(struct buffer * buf, uint8_t inp)
{
    ASSERT_NULL(buf, error);

    if (b_get_free_space(buf) <= sizeof(inp))
    {
        return -1;
    }

    __unsafe_TBf8(buf, inp);


    return sizeof(inp);

error:
    setSysError(SYSTEM_NULL_ARGUMENT);
    return -1;
}

/*void usart_TBfCRC16(struct buffer * buf, int16_t crc)
{
    ASSERT_NULL(s_usart_ptr, error);

    uint8_t * ptr = &crc;
    if (__check_txe_space(s_usart_ptr, sizeof(crc)) == 1)
    {
        return;
    }

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        __unsafe_TBf8(s_usart_ptr, *ptr++);
        __unsafe_TBf8(s_usart_ptr, *ptr);

    }

	return;

error:
    setSysError(SYSTEM_NULL_ARGUMENT);
    return;
}*/

int16_t
bTBf16(struct buffer * buf, int16_t inp)
{
    ASSERT_NULL(buf, error);

    if (b_get_free_space(buf) <= sizeof(inp))
    {
        return -1;
    }

    uint8_t * ptr = &inp;
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr);


	return sizeof(inp);

error:
    setSysError(SYSTEM_NULL_ARGUMENT);
    return -1;
}

int16_t
bTBf16_t(struct buffer * buf, uint16_t inp)
{
    ASSERT_NULL(buf, error);

    if (b_get_free_space(buf) <= sizeof(inp))
    {
        return -1;
    }

    uint8_t * ptr = &inp;
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr);

	return sizeof(inp);

error:
    setSysError(SYSTEM_NULL_ARGUMENT);
    return -1;
}

int16_t
bTBf32(struct buffer * buf, int32_t inp)
{
    ASSERT_NULL(buf, error);

    if (b_get_free_space(buf) <= sizeof(inp))
    {
        return -1;
    }

    uint8_t * ptr = &inp;
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr);


	return sizeof(inp);

error:
    setSysError(SYSTEM_NULL_ARGUMENT);
    return -1;
}

int16_t
bTBf32_t(struct buffer * buf, uint32_t inp)
{
    ASSERT_NULL(buf, error);

    if (b_get_free_space(buf) <= sizeof(inp))
    {
        return -1;
    }

    uint8_t * ptr = &inp;
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr);

	return sizeof(inp);

error:
    setSysError(SYSTEM_NULL_ARGUMENT);
    return -1;
}

int16_t
bTBf64(struct buffer * buf, int64_t inp)
{
    ASSERT_NULL(buf, error);

    if (b_get_free_space(buf) <= sizeof(inp))
    {
        return -1;
    }

    uint8_t * ptr = &inp;
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr);



	return sizeof(inp);

error:
    setSysError(SYSTEM_NULL_ARGUMENT);
    return -1;
}

int16_t
bTBf64_t(struct buffer * buf, uint64_t inp)
{
    ASSERT_NULL(buf, error);

    if (b_get_free_space(buf) <= sizeof(inp))
    {
        return -1;
    }

    uint8_t * ptr = &inp;
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr);


	return sizeof(inp);

error:
    setSysError(SYSTEM_NULL_ARGUMENT);
    return -1;
}

int16_t
bTBfBuf(struct buffer * buf, uint8_t * inp, uint16_t len)
{
    ASSERT_NULL_E(buf, SYSTEM_NULL_ARGUMENT, error);
    ASSERT_NULL_E(inp, SYSTEM_NULL_ARGUMENT, error);

	uint16_t i = 0;

    if (b_get_free_space(buf) <= len)
    {
        return -1;
    }

    for (i=0; i < len; i++)
    {
        __unsafe_TBf8(buf, inp[i]);
    }

    return 0;



error:
	return -1;
}

int16_t
bTBfDouble(struct buffer * buf, double inp)
{
    ASSERT_NULL(buf, error);

    if (b_get_free_space(buf) <= sizeof(inp))
    {
        return -1;
    }

    uint8_t * ptr = &inp;
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr++);
    __unsafe_TBf8(buf, *ptr);


    return sizeof(inp);

error:
    setSysError(SYSTEM_NULL_ARGUMENT);
    return -1;
}

static uint8_t
__RBf_unsafe(struct buffer * buf)
{
    if (buf->flags & BUF_FLAG_CIRCULAR)
    {
        uint8_t val = *buf->read_ptr;

        ++buf->read_ptr;

        if (buf->read_ptr > buf->buf_end)
        {
            buf->read_ptr = buf->buf_start;
        }

        return val;
    }
    else if (buf->flags & BUF_FLAG_LINEAR)
    {
        uint8_t val = *buf->read_ptr;

        ++buf->read_ptr;

        if (buf->read_ptr == buf->write_ptr)
        {
            buf->write_ptr = buf->buf_start;
            buf->read_ptr = buf->buf_start;
            buf->flags |= BUF_FLAG_EOF;
        }

        /*if (buf->write_ptr > buf->buf_end)
        {

        }*/

        return val;
    }

    return 0;
}

/*static uint8_t __RBf_no_offset(struct usart_io * s_usart_ptr, uint8_t offs)
{
    uint8_t pos = s_usart_ptr->rcv_buf.read_pos + offs;
    if (pos >= USART_RCV_BUFFER)
    {
        pos = pos - USART_RCV_BUFFER;
    }

	return s_usart_ptr->rcv_buf.buf[pos];//s_usart_ptr->rcv_buf.read_pos + offs];
}*/

int16_t
bRBf8(struct buffer * buf)
{

    if (b_has_more(buf) == 0)
    {
        return -1;
    }

	return __RBf_unsafe(buf);
}

int32_t
bRBfArr(struct buffer * buf, uint8_t * arr_ptr, uint16_t rlen)
{
    ASSERT_NULL_E(arr_ptr, SYSTEM_NULL_ARGUMENT, error);
    ASSERT_DATA(rlen > USART_RCV_BUFFER, goto error);

    if (buf->flags & BUF_FLAG_CIRCULAR)
    {
        uint16_t toRead = b_get_read_len(buf); //s_usart_ptr->rcv_buf.to_read_left;
        if (rlen < toRead)
        {
            toRead = rlen;
        }

        if ( (buf->read_ptr + toRead) > buf->buf_end)
        {
            uint16_t tor = buf->buf_end - buf->read_ptr + 1;
            memcpy(arr_ptr, buf->read_ptr, tor); //&s_usart_ptr->rcv_buf.buf[s_usart_ptr->rcv_buf.read_pos], tor);
            buf->read_ptr = buf->buf_start;
            tor = toRead - tor;
            memcpy(arr_ptr, buf->read_ptr, tor);
            buf->read_ptr += tor;
        }
        else
        {
            memcpy(arr_ptr, buf->read_ptr, toRead);
            buf->read_ptr += toRead;
        }
        return toRead;
    }
    else if (buf->flags & BUF_FLAG_LINEAR)
    {
        uint32_t left = b_get_read_len(buf); //s_usart_ptr->rcv_buf.to_read_left;
        if (rlen > left)
        {
            setSysError(SYSTEM_OUT_OF_RANGE);
            goto error;
        }

        memcpy(arr_ptr, buf->read_ptr, left);
        buf->read_ptr += left;

        return left;
    }
    //s_usart_ptr->rcv_buf.to_read_left -= toRead;


error:
    return -1;
}

int32_t
cbRBfArr_no_offset(struct buffer * buf, uint8_t * arr_ptr, uint16_t rlen)
{
    ASSERT_NULL_E(arr_ptr, SYSTEM_NULL_ARGUMENT, error);
    ASSERT_DATA(rlen > USART_RCV_BUFFER, goto error);

    if (buf->flags & BUF_FLAG_CIRCULAR)
    {
        uint16_t toRead = b_get_read_len(buf); //s_usart_ptr->rcv_buf.to_read_left;
        if (rlen < toRead)
        {
            toRead = rlen;
        }

        if ( (buf->read_ptr + toRead) > buf->buf_end)
        {
            uint16_t tor = buf->buf_end - buf->read_ptr + 1;
            memcpy(arr_ptr, buf->read_ptr, tor);
            tor = toRead - tor;
            memcpy(arr_ptr, buf->buf_start, tor);
        }
        else
        {
            memcpy(arr_ptr, buf->read_ptr, toRead);
        }

        //s_usart_ptr->rcv_buf.to_read_left -= toRead;

        return toRead;
    }
    else if (buf->flags & BUF_FLAG_LINEAR)
    {
        uint32_t left = b_get_read_len(buf); //s_usart_ptr->rcv_buf.to_read_left;
        if (rlen > left)
        {
            setSysError(SYSTEM_OUT_OF_RANGE);
            goto error;
        }

        memcpy(arr_ptr, buf->read_ptr, left);

        return left;
    }

error:
    return -1;
}
