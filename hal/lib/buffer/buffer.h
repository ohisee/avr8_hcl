#ifndef BUFFER_H_INCLUDED
#define BUFFER_H_INCLUDED

#include "../../build_defs.h"

enum buffer_type
{
	BUF_TYPE_LINEAR = (uint8_t) 0x01,
	BUF_TYPE_CIRCULAR = (uint8_t) 0x02
};

#define BUF_FLAG_LINEAR 	0x01
#define BUF_FLAG_CIRCULAR 	0x02
#define BUF_FLAG_EOF		0x04
#define BUF_FLAG_HEAP		0x80

#define B_MORE 1
#define B_NO_MORE 0
/*********CIRCULAR BUFFER***************/
struct buffer
{
	uint8_t flags;			//flags
	uint8_t * buf_start;    //buf start
	uint8_t * buf_end;      //buf stop
	uint8_t * read_ptr;     //read pos
	uint8_t * write_ptr;    //write pos
};

struct buffer * b_create(struct buffer * buf, enum buffer_type e_buf_type, uint8_t * ex_buf, uint32_t cbf_size);
void b_free(struct buffer * buf);
void b_free_static(struct buffer * buf);
void b_reset(struct buffer * buf);
void b_reset_for_reading(struct buffer * buf);
uint32_t b_get_buf_len(struct buffer * buf);
uint32_t b_get_free_space(struct buffer * buf);
size_t b_get_read_len(struct buffer * buf);
void b_set_new_data(struct buffer * buf, size_t offset);
int8_t b_has_more(struct buffer * buf);
int16_t bTBf8(struct buffer * buf, uint8_t inp);
int16_t bTBf16(struct buffer * buf, int16_t inp);
int16_t bTBf16_t(struct buffer * buf, uint16_t inp);
int16_t bTBf32(struct buffer * buf, int32_t inp);
int16_t bTBf32_t(struct buffer * buf, uint32_t inp);
int16_t bTBf64(struct buffer * buf, int64_t inp);
int16_t bTBf64_t(struct buffer * buf, uint64_t inp);
int16_t bTBfBuf(struct buffer * buf, uint8_t * inp, uint16_t len);
int16_t bTBfDouble(struct buffer * buf, double inp);
int16_t bRBf8(struct buffer * buf);
int32_t bRBfArr(struct buffer * buf, uint8_t * arr_ptr, uint16_t rlen);
int32_t bRBfArr_no_offset(struct buffer * buf, uint8_t * arr_ptr, uint16_t rlen);

#endif // BUFFER_H_INCLUDED
