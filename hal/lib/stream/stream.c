/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <util/atomic.h>
#include "../../hal.h"
#include "./stream.h"

#include "../../../os/kernel/kernel_memory.h"

struct kstream * ksopen(struct dev_descr_node * ddn,
                        FILE * file,
                        uint8_t flags)
{
    ASSERT_NULL_ER(ddn, SYSTEM_NULL_ARGUMENT, NULL);

    //check if fd supports streams
    t_hal_kstream fkstream = (t_hal_kstream) pgm_read_word(&ddn->prgm_handlers->FSTREAM_INIT); //<---- FIX
    ASSERT_NULL_E(fkstream, SYSTEM_OPERATION_NOT_SUPPORTED, leave_ksopen);

    struct kstream * ksr = (struct kstream *) kernel_malloc(sizeof(struct kstream));
    ASSERT_NULL_E(ksr, SYSTEM_MALLOC_FAILED, leave_ksopen);

    //reset memory
    memset((void *) ksr, 0, sizeof(struct kstream));

    //setting values
    ksr->ddn = ddn;
    ksr->flags = flags;

    if (fkstream(ddn, ksr) != 0)
    {
        //fstream open failed, FD returned fail
        goto free_kstream;
    }

     //attach kstream to file
    file->udata = (void *) ksr;

    return ksr;

free_kstream:
    kernel_free((void *) ksr);
    goto leave_ksopen;

arg_error:
    setSysError(SYSTEM_FD_INVALID);
leave_ksopen:
    return NULL;
}


void ksclose(struct kstream * ks)
{
    ASSERT_NULL_E(ks, SYSTEM_NULL_ARGUMENT, leave_ksclose);

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        //kernel_unregister_stream(ks);
        kernel_free((void *) ks);
    }

leave_ksclose:
    return;
}

void kssetdata(struct kstream * ks, void * data)
{
    ks->loc_data = data;
}

/*void kslock(struct kstream * stream, t_dev_id did)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, error_exit);
	ASSERT_NULL_E(did, SYSTEM_FD_INVALID, error_exit);

stream_locked:
	//wait when stream will be unlocked
	while (stream->fid_locked != K_NO_FD); //wait

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
	    //check once again
		if (stream->fid_locked == K_NO_FD)
		{
			//locking
			stream->fid_locked = did;
			return;
		}
	}

    //somehow the stream was locked by other source befor program entered ATOMIC_BLOCK
	goto stream_locked;

error_exit:
	return;
}*/

/*void ksunlock(struct kstream * stream, t_dev_id did)
{
	ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, error_exit);
	ASSERT_NULL_E(did, SYSTEM_FD_INVALID, error_exit);

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
        if (stream->fid_locked == did)
        {
            stream->fid_locked = K_NO_FD;
            return;
        }
	}

	//some fd is trying to unlock stream
	setSysError(SYSTEM_STREAM_ILLEGAL_UNLOCK_ATTEMPT);

error_exit:
	return;
}*/

void ksinitack(struct kstream * stream)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, error_exit);
    ASSERT_NULL_E(stream->KSWINIT, SYSTEM_OPERATION_NOT_SUPPORTED, error_exit);

    stream->KSWINIT(stream);

error_exit:
	return;
}

void ksfinishack(struct kstream * stream)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, error_exit);
    ASSERT_NULL_E(stream->KSWACK, SYSTEM_OPERATION_NOT_SUPPORTED, error_exit);

    stream->KSWACK(stream);

error_exit:
	return;
}

/*void ksdrop(struct kstream * stream, enum drop_data_act e_drop)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, leave);

    ASSERT_NULL_E(stream->KSDROP, SYSTEM_BUFFER_DROP_SEGMENT_NOT_SUPP, leave);

    stream->KSDROP(stream, e_drop);

leave:
    return;
}*/

void ksflush(struct kstream * stream)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, leave);

//    ASSERT_NULL_E(stream->KSFLUSH, SYSTEM_BUFFER_DROP_SEGMENT_NOT_SUPP, leave);

   // stream->KSFLUSH(stream);

leave:
    return;
}

uint8_t ksrcvlen(struct kstream * stream)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, error);
    ASSERT_NULL_E(stream->KSRLEN, SYSTEM_OPERATION_NOT_SUPPORTED, error);

    return stream->KSRLEN(stream);

error:
    return 0;
}

uint8_t kstxelen(struct kstream * stream)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, error);
    ASSERT_NULL_E(stream->KSWLEN, SYSTEM_OPERATION_NOT_SUPPORTED, error);

    return stream->KSWLEN(stream);

error:
    return 0;
}

uint8_t * ksdirectrbuf(struct kstream * stream)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, error);

    return NULL;//stream->KSRBUF(stream);

error:
    return NULL;
}

void kswrite8(struct kstream * stream, uint8_t inp)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, null_arg);

    stream->KSWRITE(stream, inp, 1);

null_arg:
	return;
}

void kswrite16(struct kstream * stream, int16_t inp)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, null_arg);

    uint8_t * ptr = &inp;
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr, 1);

null_arg:
	return;
}

void kswrite16_u(struct kstream * stream, uint16_t inp)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, null_arg);

    uint8_t * ptr = &inp;
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr, 1);

null_arg:
	return;
}

void kswrite32(struct kstream * stream, int32_t inp)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, null_arg);

    uint8_t * ptr = &inp;
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr, 1);

null_arg:
	return;
}

void kswrite32_u(struct kstream * stream, uint32_t inp)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, null_arg);

    uint8_t * ptr = &inp;
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr, 1);

null_arg:
	return;
}

void kswrite64(struct kstream * stream, int64_t inp)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, null_arg);

    uint8_t * ptr = &inp;
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr, 1);

null_arg:
	return;
}

void kswrite64_u(struct kstream * stream, uint64_t inp)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, null_arg);

    uint8_t * ptr = &inp;
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr, 1);

null_arg:
	return;
}

void kswrite(struct kstream * stream, uint8_t * inp, uint16_t len)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, null_arg);
    ASSERT_NULL_E(inp, SYSTEM_NULL_ARGUMENT, null_arg);


    stream->KSWRITE(stream, inp, len);


null_arg:
	return;
}

void kswritef(struct kstream * stream, double inp)
{
    ASSERT_NULL_E(stream, SYSTEM_NULL_ARGUMENT, null_arg);

    uint8_t * ptr = &inp;

    if (sizeof(double) == 8)
    {
        stream->KSWRITE(stream, *ptr++, 1);
        stream->KSWRITE(stream, *ptr++, 1);
        stream->KSWRITE(stream, *ptr++, 1);
        stream->KSWRITE(stream, *ptr++, 1);
    }

    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr++, 1);
    stream->KSWRITE(stream, *ptr, 1);

null_arg:
	return;
}

int16_t ksread8(struct kstream * stream)
{
    if (!(stream->flags & SREAD))
    {
        //read not allowed
        stream->flags |= SERR;
        return -1;
    }

    return 0;//stream->KSREAD(stream);
}

int16_t ksread16(struct kstream * stream)
{
    if (!(stream->flags & SREAD))
    {
        //read not allowed
        stream->flags |= SERR;
        return -1;
    }

    uint16_t tmp = 0;
    //((uint8_t *)&tmp)[0] = (uint8_t) stream->KSREAD(stream);
    //((uint8_t *)&tmp)[1] = (uint8_t) stream->KSREAD(stream);

    return tmp;
}

int32_t
ksread32(struct kstream * stream)
{
    if (!(stream->flags & SREAD))
    {
        //read not allowed
        stream->flags |= SERR;
        return -1;
    }

    int32_t tmp = 0;
    //((uint8_t *)&tmp)[0] = (uint8_t) stream->KSREAD(stream);
    //((uint8_t *)&tmp)[1] = (uint8_t) stream->KSREAD(stream);
   // ((uint8_t *)&tmp)[2] = (uint8_t) stream->KSREAD(stream);
   // ((uint8_t *)&tmp)[3] = (uint8_t) stream->KSREAD(stream);

    return tmp;
}

int32_t ksreadbuf(struct kstream * stream, uint8_t * rbuf, uint16_t rlen)
{
    if (!(stream->flags & SREAD))
    {
        //read not allowed
        stream->flags |= SERR;
        return -1;
    }

    return 0;//stream->KSREADARR(stream, rbuf, rlen);
    return 0;//stream->KSREADARR(stream, rbuf, rlen);
}
