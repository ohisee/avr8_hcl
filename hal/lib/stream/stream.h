#ifndef STREAM_H_INCLUDED
#define STREAM_H_INCLUDED

typedef int8_t k_stream_id;

//stream
#define SASYNC 1                          /*asynchronous stream (user-read-only)*/
#define SSYNC ~1                          /*synchronous stream (inversion of the previous def) (user-read-only)*/
#define SREAD 2                           /*can read (user RW) via init function*/
#define SWRITE 4                          /*can write (user RW) via init function*/
#define SRW SREAD | SWRITE                /*both RW*/
#define SCRC 32                           /*RISED (crc calculation in progress) user-read-only*/
#define SEOF 64                           /*end of file*/
#define SERR 128                          /*error (any)*/

struct kstream
{
    k_stream_id id;                     //automatic set
    struct dev_descr_node * ddn;        //opened to
	uint8_t flags;                      //flags
    void * loc_instance;                //pointer to the record
    void * loc_data;                    //pointer to the data

    int8_t (*KSWINIT)(struct kstream * ks);     //initialize write
    int8_t (*KSWACK) (struct kstream * ks);     //finalize write
    int8_t (*KSRBUF)(struct kstream * ks,
                     uint8_t * rbuf,
                     uint8_t * wbuf
                    );                          //direct access to R buf
    int8_t (*KSWRITE)(struct kstream * ks,
                      uint8_t * buf_ptr,
                      uint16_t bwsize
                     );                         //write to stream
    int32_t (*KSREAD)(struct kstream * ks,
                      uint8_t * lbuf,
                      uint16_t rsize
                     );                         //read from stream to local buf
    int16_t (*KSRLEN)(struct kstream * ks);     //read (to read left) length
    int16_t (*KSWLEN)(struct kstream * ks);     //write (left) length
};


struct kstream * ksopen(struct dev_descr_node * ddn, FILE * file, uint8_t flags/*, enum kstream_type e_stype*/);
void ksclose(struct kstream * ks);
void kssetdata(struct kstream * ks, void * data);

void kslock(struct kstream * stream, t_dev_id did);
void ksunlock(struct kstream * stream, t_dev_id did);
void ksfinishack(struct kstream * stream);
void ksdrop(struct kstream * stream);
void ksflush(struct kstream * stream);
uint8_t ksrcvlen(struct kstream * stream);
uint8_t kstxelen(struct kstream * stream);
uint8_t * ksdirectrbuf(struct kstream * stream);
void kswrite8(struct kstream * stream, uint8_t inp);
void kswrite16(struct kstream * stream, int16_t inp);
void kswrite16_u(struct kstream * stream, uint16_t inp);
void kswrite32(struct kstream * stream, int32_t inp);
void kswrite32_u(struct kstream * stream, uint32_t inp);
void kswrite64(struct kstream * stream, int64_t inp);
void kswrite64_u(struct kstream * stream, uint64_t inp);
void kswrite(struct kstream * stream, uint8_t * inp, uint16_t len);
void kswritef(struct kstream * stream, double inp);
int16_t ksread8(struct kstream * stream);
int16_t ksread16(struct kstream * stream);
int32_t ksread32(struct kstream * stream);
int32_t ksreadbuf(struct kstream * stream, uint8_t * rbuf, uint16_t rlen);

#endif // STREAM_H_INCLUDED
