/*-
 *  ATTENTION!: the initial version of the DHCP logic was taken from:
 *		link:http://easyelectronics.ru/files/WE/redsh/eth_232.zip
 *		link2:http://we.easyelectronics.ru/electro-and-pc/podklyuchenie-mikrokontrollera-k-lokalnoy-seti-udp-server.html
 *		License: WTFPL
 *
 *	--------APPLICABLE ONLY FOR DSC code, not ENC28J60 logic/routnies------
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"
#include <avr/pgmspace.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "../../hal.h"
#include "./hal_net_stack.h"
#include "./hal_net_stack_func.h"

uint8_t * arp_search_cache(struct arp_cache_entry * arp_cache, uint32_t node_ip_addr);
uint8_t __arp_cache_add(struct arp_cache_entry * arp_cache, uint32_t ip_adr, uint8_t * mac_adr);

uint8_t arp_cache_add(struct arp_cache_entry * arp_cache, uint32_t ip_adr, uint8_t * mac_adr)
{
	if (arp_search_cache(arp_cache, ip_adr) != NULL)
	{
		return 1;
	}

	return __arp_cache_add(arp_cache, ip_adr, mac_adr);
}

uint8_t __arp_cache_add(struct arp_cache_entry * arp_cache, uint32_t ip_adr, uint8_t * mac_adr)
{
	ASSERT_NULL_E(arp_cache, SYSTEM_NULL_ARGUMENT, error);

	uint8_t i = 0;
	for (; i < ARP_CACHE_SIZE; i++)
	{
		if ((arp_cache[i].flags == 0) ||
			(arp_cache[i].flags & ARP_FLAG_CAN_BE_REMOVED) )
		{
			break;
		}
	}

	ASSERT_DATA(i == ARP_CACHE_SIZE, goto error_out_of_space);

	arp_cache[i].ip_addr = ip_adr;
	memcpy(arp_cache[i].mac_addr, mac_adr, 6);
	arp_cache[i].flags |= ARP_FLAG_USED;

#if DEBUG_MESSAGES == YES
	printf_P(PSTR("ARP cache added: %d ip:%lu\r\n"), i, ip_adr);
#endif // DEBUG_MESSAGES

	return 0;

error_out_of_space:
	setSysError(SYSTEM_LENGTH_ASSERT);
error:
	return 1;
}

// search ARP cache
uint8_t * arp_search_cache(struct arp_cache_entry * arp_cache, uint32_t node_ip_addr)
{
	ASSERT_NULL_E(arp_cache, SYSTEM_NULL_ARGUMENT, error);

	uint8_t i;
	for(i = 0; i < ARP_CACHE_SIZE; ++i)
	{
		if (arp_cache[i].flags & ARP_FLAG_USED)
		{
			if(arp_cache[i].ip_addr == node_ip_addr)
			{
				return arp_cache[i].mac_addr;
			}
		}
	}

error:
	return NULL;
}

// resolve MAC address
// returns 0 if still resolving
// (invalidates net_buffer if not resolved)
uint8_t *arp_resolve(struct knetdevice * knd, uint32_t node_ip_addr)
{
	ASSERT_NULL_ER(knd, SYSTEM_NULL_ARGUMENT, NULL);

	uint8_t *mac;

	// search arp cache
	if((mac = arp_search_cache(knd->arp, node_ip_addr)))
	{
		return mac;
	}

	uint8_t buf[sizeof(struct eth_frame)+sizeof(struct arp_message)];

	eth_frame_t *frame = (void*)buf;
	arp_message_t *msg = (void*)(frame->data);

	// send request
	memset(frame->to_addr, 0xff, 6);
	frame->type = ETH_TYPE_ARP;

	msg->hw_type = ARP_HW_TYPE_ETH;
	msg->proto_type = ARP_PROTO_TYPE_IP;
	msg->hw_addr_len = 6;
	msg->proto_addr_len = 4;
	msg->type = ARP_TYPE_REQUEST;
	memcpy(msg->mac_addr_from, knd->mac, 6);
	msg->ip_addr_from = knd->nic_set.ipv4;
	memset(msg->mac_addr_to, 0x00, 6);
	msg->ip_addr_to = node_ip_addr;

	eth_send(knd, frame, sizeof(arp_message_t));

	return NULL;
}

// process arp packet
void arp_filter(struct knetdevice * knd, uint16_t len)
{
	eth_frame_t *frame = (void*)knd->rx_buf;
	arp_message_t *msg = (void*)(frame->data);

	if(len >= sizeof(arp_message_t))
	{
		if( (msg->hw_type == ARP_HW_TYPE_ETH) &&
			(msg->proto_type == ARP_PROTO_TYPE_IP) &&
			(msg->ip_addr_to == knd->nic_set.ipv4 )
		  )
		{
			switch(msg->type)
			{
				case ARP_TYPE_REQUEST:
					#if DEBUG_MESSAGES == YES
                        printf_P(PSTR("rcv ARP_TYPE_REQUEST\r\n"));
					#endif // DEBUG_MESSAGES

					msg->type = ARP_TYPE_RESPONSE;
					memcpy(msg->mac_addr_to, msg->mac_addr_from, 6);
					memcpy(msg->mac_addr_from, knd->mac, 6);
					msg->ip_addr_to = msg->ip_addr_from;
					msg->ip_addr_from = knd->nic_set.ipv4;
					eth_reply(knd, frame, sizeof(arp_message_t));
				break;

				case ARP_TYPE_RESPONSE:
					if(arp_search_cache(knd->arp, msg->ip_addr_from) == NULL)
					{
						#if DEBUG_MESSAGES == YES
                            printf_P(PSTR("rcv ARP_TYPE_RESPONSE\r\n"));
						#endif // DEBUG_MESSAGES

						/*arp_cache[arp_cache_wr].ip_addr = msg->ip_addr_from;
						memcpy(arp_cache[arp_cache_wr].mac_addr, msg->mac_addr_from, 6);
						arp_cache_wr++;*/

						//lookup for the socket which could request the IP
						//if it was not found then set ksock as -1 for single usage untill next time
						if (__arp_cache_add(knd->arp, msg->ip_addr_from, msg->mac_addr_from) == 1)
						{
							//trap, no space in buffer left ToDo: handle this situation
							//this should be handled by the periodic task which will check if cache entries are actual
						}

						/*if(arp_cache_wr == ARP_CACHE_SIZE)
							arp_cache_wr = 0;*/
					}
				break;
			}
		}
	}

	return;
}
