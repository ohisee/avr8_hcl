#ifndef HAL_NET_STACK_H_
#define HAL_NET_STACK_H_



#define ip_broadcast(_ip_addr_, _ip_mask_) (_ip_addr_ | ~_ip_mask_)


/*
 * Ethernet
 */

#define ETH_TYPE_ARP		htons(0x0806)
#define ETH_TYPE_IP			htons(0x0800)

typedef struct eth_frame {
	uint8_t to_addr[6];
	uint8_t from_addr[6];
	uint16_t type;
	uint8_t data[];
} eth_frame_t;

/*
 * ARP
 */

#define ARP_HW_TYPE_ETH		htons(0x0001)
#define ARP_PROTO_TYPE_IP	htons(0x0800)

#define ARP_TYPE_REQUEST	htons(1)
#define ARP_TYPE_RESPONSE	htons(2)

typedef struct arp_message {
	uint16_t hw_type;
	uint16_t proto_type;
	uint8_t hw_addr_len;
	uint8_t proto_addr_len;
	uint16_t type;
	uint8_t mac_addr_from[6];
	uint32_t ip_addr_from;
	uint8_t mac_addr_to[6];
	uint32_t ip_addr_to;
} arp_message_t;

#define ARP_FLAG_USED 1				//indicates that this record is still in use
//#define ARP_FLAG_RENEW 2			//indicates that timeout happened and renew is required
#define ARP_FLAG_CAN_BE_REMOVED 4	//indicates that this record can be substituted anytime

typedef struct arp_cache_entry {
	uint32_t ip_addr;
	uint8_t mac_addr[6];
	uint8_t flags;
} arp_cache_entry_t;

struct udp_checksum_pseudo
{
	uint32_t from_addr;
	uint32_t to_addr;
	uint8_t reserved;
	uint8_t protocol;
	uint16_t len;
};

/*
 * IP
 */

#define IP_PROTOCOL_ICMP	1
#define IP_PROTOCOL_TCP		6
#define IP_PROTOCOL_UDP		17

typedef struct ip_packet {
	uint8_t ver_head_len;
	uint8_t tos;
	uint16_t total_len;
	uint16_t fragment_id;
	uint16_t flags_framgent_offset;
	uint8_t ttl;
	uint8_t protocol;
	uint16_t cksum;
	uint32_t from_addr;
	uint32_t to_addr;
	uint8_t data[];
} ip_packet_t;


/*
 * ICMP
 */

#define ICMP_TYPE_ECHO_RQ	8
#define ICMP_TYPE_ECHO_RPLY	0

typedef struct icmp_echo_packet {
	uint8_t type;
	uint8_t code;
	uint16_t cksum;
	uint16_t id;
	uint16_t seq;
	uint8_t data[];
} icmp_echo_packet_t;


/*
 * UDP
 */

typedef struct udp_packet {
	uint16_t from_port;
	uint16_t to_port;
	uint16_t len;
	uint16_t cksum;
	uint8_t data[];
} udp_packet_t;

/*
 * TCP
 */

#if (WITH_TCP == YES)
#define TCP_FLAG_URG		0x20
#define TCP_FLAG_ACK		0x10
#define TCP_FLAG_PSH		0x08
#define TCP_FLAG_RST		0x04
#define TCP_FLAG_SYN		0x02
#define TCP_FLAG_FIN		0x01

typedef struct tcp_packet {
	uint16_t from_port;
	uint16_t to_port;
	uint32_t seq_num;
	uint32_t ack_num;
	uint8_t data_offset;
	uint8_t flags;
	uint16_t window;
	uint16_t cksum;
	uint16_t urgent_ptr;
	uint8_t data[];
} tcp_packet_t;

#define tcp_head_size(tcp)	(((tcp)->data_offset & 0xf0) >> 2)
#define tcp_get_data(tcp)	((uint8_t*)(tcp) + tcp_head_size(tcp))

typedef enum tcp_status_code {
	TCP_CLOSED,
	TCP_SYN_SENT,
	TCP_SYN_RECEIVED,
	TCP_ESTABLISHED,
	TCP_FIN_WAIT
} tcp_status_code_t;

typedef enum tcp_sending_mode {
	TCP_SENDING_SEND,
	TCP_SENDING_REPLY,
	TCP_SENDING_RESEND
} tcp_sending_mode_t;

typedef struct tcp_state {
	enum tcp_sending_mode tcp_send_mode;
	enum tcp_status_code status;
	uint64_t event_time;
	uint32_t seq_num;
	uint32_t ack_num;
	/*uint32_t remote_addr;
	uint16_t remote_port;
	uint16_t local_port;*///kernel keeps this information
	uint8_t tcp_ack_sent;
#if (WITH_TCP_REXMIT==YES)
	uint8_t is_closing;
	uint8_t rexmit_count;
	uint32_t seq_num_saved;
#endif
} tcp_state_t;

#define TCP_OPTION_PUSH			0x01
#define TCP_OPTION_CLOSE		0x02
#define TCP_OPTION_ABORT        0x04
#endif


#endif
