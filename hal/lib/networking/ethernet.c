/*-
 *  ATTENTION!: the initial version of the DHCP logic was taken from:
 *		link:http://easyelectronics.ru/files/WE/redsh/eth_232.zip
 *		link2:http://we.easyelectronics.ru/electro-and-pc/podklyuchenie-mikrokontrollera-k-lokalnoy-seti-udp-server.html
 *		License: WTFPL
 *
 *	--------APPLICABLE ONLY FOR DSC code, not ENC28J60 logic/routnies------
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "../../hal.h"
#include "./hal_net_stack.h"
#include "./hal_net_stack_func.h"

uint32_t generate_crc32 (uint32_t crc, unsigned char *buffer, int length) {
    int i, j;
    for (i=0; i<length; i++) {
        crc = crc ^ *(buffer++);
        for (j=0; j<8; j++) {
		    if (crc & 1)
		        crc = (crc>>1) ^ 0xEDB88320 ;
		    else
		        crc = crc >>1 ;
        }
    }
    return crc;
}

// send Ethernet frame
// fields must be set:
//	- frame.dst
//	- frame.type
int32_t eth_send(struct knetdevice * knd, eth_frame_t *frame, uint16_t len)
{
	memcpy(frame->from_addr, knd->mac, 6);
	return knd->hndls.TRANSMIT(knd, (void*)frame, len + sizeof(eth_frame_t));
	//return enc28j60_send_packet(enc28, (void*)frame, len + sizeof(eth_frame_t));
}

// send Ethernet frame back
int32_t eth_reply(struct knetdevice * knd, eth_frame_t *frame, uint16_t len)
{
	memcpy(frame->to_addr, frame->from_addr, 6);
	memcpy(frame->from_addr, knd->mac, 6);

	//int32_t wlen = enc28j60_send_packet(enc28, (void*)frame, len + sizeof(eth_frame_t));
	int32_t wlen = knd->hndls.TRANSMIT(knd, (void*)frame, len + sizeof(eth_frame_t));

	#if DEBUG_MESSAGES == YES
	printf_P(PSTR("ETH: reply! %i\r\n"), wlen);
	#endif // DEBUG_MESSAGES

	return wlen;
}

int32_t eth_resend(struct knetdevice * knd, eth_frame_t *frame, uint16_t len)
{
	//return enc28j60_send_packet(enc28, (void*)frame, len + sizeof(eth_frame_t));
	return knd->hndls.TRANSMIT(knd, (void*)frame, len + sizeof(eth_frame_t));
}

// process Ethernet frame
void eth_filter(struct knetdevice * knd)
{
    size_t len = b_get_read_len(knd->b_rx_buf);
	if(len >= sizeof(eth_frame_t))
	{
		eth_frame_t *frame = (void*)knd->rx_buf;
		switch(frame->type)
		{
			case ETH_TYPE_ARP:
			    #if DEBUG_MESSAGES == YES
				printf_P(PSTR("ENC28: received ARP!\r\n"));
				#endif // DEBUG_MESSAGES
				arp_filter(knd, len - sizeof(eth_frame_t));
			break;
			case ETH_TYPE_IP:
			    #if DEBUG_MESSAGES == YES
				printf_P(PSTR("ENC28: received IP!\r\n"));
				#endif // DEBUG_MESSAGES
				ip_filter(knd, len - sizeof(eth_frame_t));
			break;
		}
	}
	else
	{
		setSysError(SYSTEM_NET_DATA_DROPED);
	}

	return;
}
