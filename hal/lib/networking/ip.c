/*-
 *  ATTENTION!: the initial version of the DHCP logic was taken from:
 *		link:http://easyelectronics.ru/files/WE/redsh/eth_232.zip
 *		link2:http://we.easyelectronics.ru/electro-and-pc/podklyuchenie-mikrokontrollera-k-lokalnoy-seti-udp-server.html
 *		License: WTFPL
 *
 *	--------APPLICABLE ONLY FOR DSC code, not ENC28J60 logic/routnies------
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "../../hal.h"
#include "./hal_net_stack.h"
#include "./hal_net_stack_func.h"


// calculate IP checksum
/*uint16_t ip_cksum(uint32_t sum, uint8_t *buf, size_t len)
{
	while(len >= 2)
	{
		sum += ((uint16_t)*buf << 8) | *(buf+1);
		buf += 2;
		len -= 2;
	}

	if(len)
		sum += (uint16_t)*buf << 8;

	while(sum >> 16)
		sum = (sum & 0xffff) + (sum >> 16);

	return ~htons((uint16_t)sum);
}*/
uint16_t ip_cksum(uint32_t sum, uint8_t *buf, size_t len)
{
    // Рассчитываем сумму word'ов блока
    while(len >= 2)
    {
        sum += *(uint16_t *)buf;
        buf += 2;
        len -= 2;
    }

    if(len)
        sum += (uint16_t) ((*buf) << 8) & 0xFF00;

    // Складываем старший и младший word суммы
    // пока не получим число, влезающее в word
    while(sum >> 16)
        sum = (sum & 0xffff) + (sum >> 16);

    //Берём дополнение до единицы
    return ~((uint16_t)sum);
}

/*WORD software_checksum(BYTE *rxtx_buffer, WORD len, DWORD sum)
{
	// build the sum of 16bit words
	while(len>1)
	{
		sum += 0xFFFF & (*rxtx_buffer<<8|*(rxtx_buffer+1));
		rxtx_buffer+=2;
		len-=2;
	}
	// if there is a byte left then add it (padded with zero)
	if (len)
	{
		sum += (0xFF & *rxtx_buffer)<<8;
	}
	// now calculate the sum over the bytes in the sum
	// until the result is only 16bit long
	while (sum>>16)
	{
		sum = (sum & 0xFFFF)+(sum >> 16);
	}
	// build 1's complement:
	return( (WORD) sum ^ 0xFFFF);
}*/

// send IP packet
// fields must be set:
//	- ip.dst
//	- ip.proto
// len is IP packet payload length
int32_t ip_send(struct knetdevice * knd, eth_frame_t *frame, uint16_t len)
{
	ip_packet_t *ip = (void*)(frame->data);
	uint32_t route_ip;
	uint8_t * mac_addr_to;

	// set frame.dst
	if(ip->to_addr == ip_broadcast(knd->nic_set.ipv4, knd->nic_set.ipv4_mask))
	{
		// use broadcast MAC
		memset(frame->to_addr, 0xff, 6);
	}
	else
	{
		// apply route
		if( ((ip->to_addr ^ knd->nic_set.ipv4) & knd->nic_set.ipv4_mask) == 0 )
			route_ip = ip->to_addr;
		else
			route_ip = knd->nic_set.ipv4_gateway;

		// resolve mac address
		if((mac_addr_to = arp_resolve(knd, route_ip)) == NULL)
        {
			return 1;
        }

		memcpy(frame->to_addr, mac_addr_to, 6);
	}

	// set frame.type
	frame->type = ETH_TYPE_IP;

	// fill IP header
	len += sizeof(ip_packet_t);

	ip->ver_head_len = 0x45;
	ip->tos = 0;
	ip->total_len = htons(len);
	ip->fragment_id = 0;
	ip->flags_framgent_offset = 0;
	ip->ttl = IP_PACKET_TTL;
	ip->cksum = 0;
	ip->from_addr = knd->nic_set.ipv4;
	ip->cksum = ip_cksum(0, (void*)ip, sizeof(ip_packet_t));

	// send frame
	return eth_send(knd, frame, len);
}

// send IP packet back
// len is IP packet payload length
int32_t ip_reply(struct knetdevice * knd, eth_frame_t *frame, uint16_t len)
{
	ip_packet_t *packet = (void*)(frame->data);

	len += sizeof(ip_packet_t);

	packet->total_len = htons(len);
	packet->fragment_id = 0;
	packet->flags_framgent_offset = 0;
	packet->ttl = IP_PACKET_TTL;
	packet->cksum = 0;
	packet->to_addr = packet->from_addr;
	packet->from_addr = knd->nic_set.ipv4;
	packet->cksum = ip_cksum(0, (void*)packet, sizeof(ip_packet_t));

	return eth_reply(knd, frame, len);
}

int32_t ip_resend(struct knetdevice * knd, eth_frame_t *frame, uint16_t len)
{
	ip_packet_t *ip = (void*)(frame->data);

	len += sizeof(ip_packet_t);
	ip->total_len = htons(len);
	ip->cksum = 0;
	ip->cksum = ip_cksum(0, (void*)ip, sizeof(ip_packet_t));

	return eth_resend(knd, frame, len);
}

// process IP packet
void ip_filter(struct knetdevice * knd, uint16_t len)
{
	eth_frame_t *frame = (void*) knd->rx_buf;
	uint16_t hcs;
	ip_packet_t *packet = (void*)(frame->data);

	//if(len >= sizeof(ip_packet_t))
	//{
		hcs = packet->cksum;
		packet->cksum = 0;

		//lookup opened sockets

		if( (packet->ver_head_len == 0x45) &&
			(ip_cksum(0, (void*)packet, sizeof(ip_packet_t)) == hcs) &&
			((packet->to_addr == knd->nic_set.ipv4) || (packet->to_addr == ip_broadcast(knd->nic_set.ipv4, knd->nic_set.ipv4_mask)) ))
		{
			len = ntohs(packet->total_len) - sizeof(ip_packet_t);

			switch(packet->protocol)
			{
#if WITH_ICMP == YES
				case IP_PROTOCOL_ICMP:
					icmp_filter(knd, frame, len);
				break;
#endif

#if WITH_UDP == YES
				case IP_PROTOCOL_UDP:
				{
					udp_packet_t *udp = (void*)(packet->data);

					if(len >= sizeof(udp_packet_t))
					{
						len = ntohs(udp->len) - sizeof(udp_packet_t);

                        struct knetstate temp_state;
                        temp_state.kstate_id = -1;
						temp_state.raddr = packet->from_addr;
						temp_state.rport = ntohs(udp->from_port);
						temp_state.laddr = packet->to_addr;
						temp_state.lport = ntohs(udp->to_port);
						temp_state.e_knet_proto = KNET_UDP;

						//check if we are waiting for a incoming data
						const struct knetlisten * udpl = hal_get_listen(temp_state.laddr, temp_state.lport, KNET_UDP);
						if (udpl == NULL)
						{
							//the handler was not defied for the ip:port, return and set errror
							#if DEBUG_MESSAGES == YES
							printf_P(PSTR("ENC28: received on closed port! %d\r\n"), temp_state.lport);
							#endif
							setSysError(SYSTEM_NET_DATA_ON_CLOSED_PORT);
							return;
						}


						knd->rx_state = (struct knetstate *) kernel_malloc(sizeof(struct knetstate));
						if (knd->rx_state == NULL)
                        {
                            //fatal error
                            #if DEBUG_MESSAGES == YES
							printf_P(PSTR("ENC28: knd->rx_state malloc failed!\r\n"));
							#endif
							setSysError(SYSTEM_MALLOC_FAILED);
							knd->rx_state = &temp_state; //change this because it is crap
                        }
                        else
                        {
                            knd->rx_state->kstate_id = temp_state.kstate_id;
                            knd->rx_state->raddr = temp_state.raddr;
                            knd->rx_state->rport = temp_state.rport;
                            knd->rx_state->laddr = temp_state.laddr;
                            knd->rx_state->lport = temp_state.lport;
                            knd->rx_state->e_knet_proto = temp_state.e_knet_proto;
                        }


						/*enc28->buf_in_data_len = len;
						enc28->net_buf_payload = udp->data;
						enc28->buf_in_pos = 0;*/
						//knd->rxed_len = len;

						uint8_t mac_br[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

						//if not broadcast mac then store it
						//ToDo change this block later
						if (memcmp(frame->from_addr, mac_br, 6) != 0)
						{
							arp_cache_add(knd->arp, packet->from_addr, frame->from_addr);
						}

						//call socket handler
						#if DEBUG_MESSAGES == YES
						printf_P(PSTR("ENC28: UDP laar=%x lport=%d %d rport=%d\r\n"), packet->to_addr, udp->to_port, knd->rx_state->lport, knd->rx_state->rport);
						#endif // DEBUG_MESSAGES

						if (udpl->hndls.ONRCV == NULL)
						{
							//DEBUG
							#if DEBUG_MESSAGES == YES
							printf_P(PSTR("ENC28: ONRCV is null\r\n"));
							#endif // DEBUG_MESSAGES
							break;
						}
						udpl->hndls.ONRCV(knd, knd->rx_state, udp->data, len);
					}
				}
				break;
#endif
#if WITH_TCP == YES
				case IP_PROTOCOL_TCP:

					tcp_filter(knd, frame, len);
				break;
#endif
				default:
					setSysError(SYSTEM_NET_PROTO_NOT_SUP);
				break;
			}
		}

		return;
}
